<?php
// Text
$_['text_success']       = 'Sikeresen módosította a vásárlókat';

// Error
$_['error_permission']   = 'Figyelem: Nincs engedélye az API-hoz való hozzáféréshez!';
$_['error_firstname']    = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']     = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']        = 'E-Mail cím nem megfelelő!';
$_['error_telephone']    = 'A telefonszám minimum 3 maximum 32 karakter lehet!';
$_['error_custom_field'] = '%s kötelező!';