<?php
// Text
$_['text_success']     = 'Az ajándékutalvány sikeresen felhasználva!';
$_['text_cart']        = 'Kosár sikeresen frissítve!';
$_['text_for']         = '%s Gift Certificate for %s';

// Error
$_['error_permission'] = 'Figyelem: Nincs engedélye az API-hoz való hozzáféréshez!';
$_['error_voucher']    = 'Ez az ajándékutalvány érvénytelen!';
$_['error_to_name']    = 'A címzett nevének 1 és 64 karakter közé kell esnie!';
$_['error_from_name']  = 'A megadott név hosszának 1 és 64 karakter közé kell esnie!';
$_['error_email']      = 'E-Mail cím nem megfelelő!';
$_['error_theme']      = 'Kérjük, válasszon témát!';
$_['error_amount']     = 'Az értéknek %s és %s közé kell esnie!';
