<?php
// Text
$_['text_success'] = 'Az API munkafolyamat sikeresen elindítva!';

// Error
$_['error_key']  = 'Warning: Incorrect API Key!';
$_['error_ip']   = 'Warning: Your IP %s is not allowed to access this API!';
