<?php
// Text
$_['text_success']     = 'A hűségpontok sikeresen levonva!';

// Error
$_['error_permission'] = 'Figyelem: Nincs engedélye az API-hoz való hozzáféréshez!';
$_['error_reward']     = 'Kérjük, adja meg mennyi hűségpontot szeretne beváltani!';
$_['error_points']     = 'Figyelem: Ön nem rendelkezik %s hűségponttal!';
$_['error_maximum']    = 'Figyelem: A maximálisan felhasználható pontok száma: %s!';