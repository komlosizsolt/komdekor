<?php
// Text
$_['text_success']     = 'Kosár sikeresen frissítve!';

// Error
$_['error_permission'] = 'Figyelem: Nincs engedélye az API-hoz való hozzáféréshez!';
$_['error_stock']      = 'A *** jelölt termék(ek) nincsenek raktáron a kért mennyiségben!';
$_['error_minimum']    = 'Minimális rendelési összeg %s %s!';
$_['error_store']      = 'Az Ön által kiválasztott termék nem vásárolható meg az üzletből!';
$_['error_required']   = '%s kötelező!';