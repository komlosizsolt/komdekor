<?php
// Text
$_['text_address']       = 'Számlázási cím sikeresen beállítva!';
$_['text_method']        = 'Fizetési mód sikeresen beállítva!';

// Error
$_['error_permission']   = 'Figyelem: Nincs engedélye az API-hoz való hozzáféréshez!';
$_['error_firstname']    = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']     = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_address_1']    = 'A cím minimum 3 maximum 128 karakter lehet!';
$_['error_city']         = 'A település neve 3 és 128 karakter közé kell essen!';
$_['error_postcode']     = 'Az irányítószám hossza ebben az országban 2 és 10 karakter közé kell essen!';
$_['error_country']      = 'Kérjük, válasszon egy országot!';
$_['error_zone']         = 'Válasszon ki megyét!';
$_['error_custom_field'] = '%s kötelező!';
$_['error_address']      = 'Figyelem: Számlázási cím megadása kötelező!';
$_['error_method']       = 'Fizetési mód nincs kiválasztva!';
$_['error_no_payment']   = 'Figyelem: Nincsenek elérhető fizetési opciók!';