<?php
// Text
$_['text_title']				= 'Klarna Számla - 14 napos fizetési határidővel';
$_['text_terms_fee']			= '<span id="klarna_invoice_toc"></span> (+%s)<script type="text/javascript">var terms = new Klarna.Terms.Invoice({el: \'klarna_invoice_toc\', eid: \'%s\', country: \'%s\', charge: %s});</script>';
$_['text_terms_no_fee']			= '<span id="klarna_invoice_toc"></span><script type="text/javascript">var terms = new Klarna.Terms.Invoice({el: \'klarna_invoice_toc\', eid: \'%s\', country: \'%s\'});</script>';
$_['text_additional']			= 'A Klarna Számlának további információkra van szüksége, hogy megkezdhesse a rendelése feldolgozását.';
$_['text_male']					= 'Férfi';
$_['text_female']				= 'Nő';
$_['text_year']					= 'Év';
$_['text_month']				= 'Hónap';
$_['text_day']					= 'Nap';
$_['text_comment']				= 'Klarna számlaszám: %s
%s/%s: %.4f';

// Entry
$_['entry_gender']				= 'Nem (Férfi / Nő)';
$_['entry_pno']					= 'Személyi igazolvány száma';
$_['entry_dob']					= 'Születési dátum';
$_['entry_phone_no']			= 'Telefonszám';
$_['entry_street']				= 'Utca';
$_['entry_house_no']			= 'Házszám';
$_['entry_house_ext']			= 'Házszám kiegészítő adatok (A,B, 3. emelet, stb)';
$_['entry_company']				= 'Cégjegyzék szám';

// Help
$_['help_pno']					= 'Kérjük, adja meg társadalombiztosítási számát (TAJ)';
$_['help_phone_no']				= 'Kérjük, adja meg telefonszámát.';
$_['help_street']				= 'Felhívjuk figyelmét, hogy a szállításra csak akkor kerülhet sor, ha az a Klarna Számlában regisztrált címre történik.';
$_['help_house_no']				= 'Kérjük, adja meg házszámát.';
$_['help_house_ext']			= 'Kérjük adja meg kiegészítő adatait a házszámhoz (A, B, C,  3. emelet, stb.).';
$_['help_company']				= 'Kérjük, adja meg cégjegyzékszámát';

// Error
$_['error_deu_terms']			= 'El kell fogadnia a Klarna Számla adatvédelmi nyilatkozatát';
$_['error_address_match']		= 'Amennyiben a Klarna Számlát szeretné használni, a szállítási és számlázási címnek egyeznie kell';
$_['error_network']				= 'Hiba történt a Klarna Számlához való kapcsolódás során. Kérjük, próbálja meg később.';