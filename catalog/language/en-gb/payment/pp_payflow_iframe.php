<?php
// Text
$_['text_title']				= 'Hitel vagy bankkártya';
$_['text_secure_connection']	= 'Biztonságos kapcsolat felépítése...';

// Error
$_['error_connection']			= 'Nem sikerült csatlakozni a PayPal-hoz. Kérjük, vegye fel a kapcsolatot a oldal adminisztrátorával, vagy válasszon másik fizetési eljárást.';