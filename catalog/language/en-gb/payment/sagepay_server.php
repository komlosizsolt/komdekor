<?php
// Text
$_['text_title']				= 'Hitelkártya / Bankkártya (SagePay)';
$_['text_credit_card']			= 'Kártya adatai';
$_['text_description']			= 'Items on %s Order No: %s';
$_['text_card_type']			= 'Kártya típus: ';
$_['text_card_name']			= 'Kártya neve: ';
$_['text_card_digits']			= 'Utolsó számok: ';
$_['text_card_expiry']			= 'Lejárat: ';
$_['text_trial']				= '%s every %s %s for %s payments then ';
$_['text_recurring']			= '%s minden %s %s';
$_['text_length']				= ' for %s payments';
$_['text_success']				= 'Az Ön befizetését jóváhagytuk.';
$_['text_decline']				= 'Az Ön tranzakcióját visszautasítottuk.';
$_['text_bank_error']			= 'A feldolgozás során hiba történt a bank oldalán.';
$_['text_transaction_error']	= 'Hiba történt a tranzakció feldolgozása során.';
$_['text_generic_error']		= 'Hiba történt a kérés feldolgozása során.';
$_['text_hash_failed']			= 'A Hash ellenőrzés nem sikerült. Kérem, ne kíséreljen meg újbóli fizetést, amíg az előző tranzakció státusza ismeretlen, ezen kívül vegye fel a kapcsolatot a kereskedővel.';
$_['text_link']					= 'Kattintson <a href="%s">ide</a> a folytatáshoz';
$_['text_confirm_delete']		= 'Are you sure you want to delete the card?';

// Entry
$_['entry_card']				= 'Új vagy meglévő kártya: ';
$_['entry_card_existing']		= 'Meglévő';
$_['entry_card_new']			= 'Új';
$_['entry_card_save']			= 'Jegyezze meg a kártya adatokat, a későbbi felhasználáshoz';
$_['entry_cc_choice']			= 'Válasszon a meglévő kártyák közül';

// Button
$_['button_delete_card']		= 'Delete selected card';