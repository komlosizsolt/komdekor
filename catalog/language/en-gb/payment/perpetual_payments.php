<?php
// Text
$_['text_title']				= 'Hitel vagy bankkártya (Biztonságosan feldolgozva a Perpetual Payments által)';
$_['text_credit_card']			= 'Bankkártya adatai';
$_['text_transaction']			= 'Tranzakció azonosító:';
$_['text_avs']					= 'AVS/CVV:';
$_['text_avs_full_match']		= 'Teljes egyezés';
$_['text_avs_not_match']		= 'Nem egyező';
$_['text_authorisation']		= 'Jóváhagyó kód:';

// Entry
$_['entry_cc_number']			= 'Kártyaszám';
$_['entry_cc_start_date']		= 'A kártya aktiválásának ideje';
$_['entry_cc_expire_date']		= 'A kártya lejárati ideje';
$_['entry_cc_cvv2']				= 'Biztonsági kód (CVV2)';
$_['entry_cc_issue']			= 'Kártyát kibocsájtó bank neve';

// Help
$_['help_start_date']			= '(ha elérhető)';
$_['help_issue']				= '(csak Maestro és Solo kártyák esetén)';