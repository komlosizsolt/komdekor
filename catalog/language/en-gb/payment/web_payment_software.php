<?php
// Text
$_['text_title']				= 'Hitelkártya / Bankkártya (Web Payment Software)';
$_['text_credit_card']			= 'Bankkártya adatai';

// Entry
$_['entry_cc_owner']			= 'Kártyatulajdonos';
$_['entry_cc_number']			= 'Kártyaszám';
$_['entry_cc_expire_date']		= 'A kártya lejárati ideje';
$_['entry_cc_cvv2']				= 'Biztonsági kód (CVV2)';