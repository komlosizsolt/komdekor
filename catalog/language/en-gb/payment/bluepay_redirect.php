<?php
// Text
$_['text_title']				= 'Hitelkártya / Bankkártya (BluePay)';
$_['text_credit_card']			= 'Kártya adatai';
$_['text_description']			= 'Items on %s Order No: %s';
$_['text_card_type']			= 'Kártya típus: ';
$_['text_card_name']			= 'Kártya neve: ';
$_['text_card_digits']			= 'Utolsó számok: ';
$_['text_card_expiry']			= 'Lejárat: ';

// Returned text
$_['text_transaction_error']	= 'Hiba történt a tranzakció közben - ';

// Entry
$_['entry_card']				= 'Új vagy meglévő kártya: ';
$_['entry_card_existing']		= 'Meglévő';
$_['entry_card_new']			= 'Új';
$_['entry_card_save']			= 'Jegyezze meg a kártya adatait';
$_['entry_cc_owner']			= 'Kártyatulajdonos';
$_['entry_cc_number']			= 'Kártyaszám';
$_['entry_cc_start_date']		= 'A kártya aktiválásának ideje';
$_['entry_cc_expire_date']		= 'A kártya lejárati ideje';
$_['entry_cc_cvv2']				= 'Biztonsági kód (CVV2)';
$_['entry_cc_address']			= 'utca';
$_['entry_cc_city']				= 'Település';
$_['entry_cc_state']			= 'Megye';
$_['entry_cc_zipcode']			= 'Irányítószám';
$_['entry_cc_phone']			= 'Telefon';
$_['entry_cc_email']			= 'E-mail';
$_['entry_cc_choice']			= 'Válasszon a meglévő kártyák közül';