<?php
// Heading
$_['text_title']				= 'Hitel / Bank kártya (Realex)';

// Button
$_['button_confirm']			= 'Megerősít';

// Entry
$_['entry_cc_type']				= 'Kártya típusa';

// Text
$_['text_success']				= 'Az Ön befizetését jóváhagytuk.';
$_['text_decline']				= 'A tranzakció sikertelen volt';
$_['text_bank_error']			= 'A feldolgozás során hiba történt a bank oldalán.';
$_['text_generic_error']		= 'Hiba történt a kérés feldolgozása során.';
$_['text_hash_failed']			= 'A Hash ellenőrzés nem sikerült. Kérem, ne kíséreljen meg újbóli fizetést, amíg az előző tranzakció státusza ismeretlen, ezen kívül vegye fel a kapcsolatot a kereskedővel.';
$_['text_link']					= 'Kattintson <a href="%s">ide</a> a folytatáshoz';
$_['text_select_card']			= 'Kérem válassza ki a kártyája típusát';
$_['text_result']				= 'Hitelesítési eredmény';
$_['text_message']				= 'Üzenet';
$_['text_cvn_result']			= 'CVN eredmény';
$_['text_avs_postcode']			= 'AVS irányítószám';
$_['text_avs_address']			= 'AVS cím';
$_['text_eci']					= 'ECI (3D secure) eredmény';
$_['text_tss']					= 'TSS eredmény';
$_['text_order_ref']			= 'Rendelési szám';
$_['text_timestamp']			= 'Időbélyeg';
$_['text_card_type']			= 'Kártya típusa';
$_['text_card_digits']			= 'Kártyaszám';
$_['text_card_exp']				= 'A kártya lejárati dátuma';
$_['text_card_name']			= 'Kártya neve';
$_['text_3d_s1']				= 'Cardholder Not Enrolled, liability shift';
$_['text_3d_s2']				= 'Unable To Verify Enrolment, no liability shift';
$_['text_3d_s3']				= 'Invalid Response From Enrolment Server, no liability shift';
$_['text_3d_s4']				= 'Enrolled, But Invalid Response From ACS (Access Control Server), no liability shift';
$_['text_3d_s5']				= 'Successful Authentication, liability shift';
$_['text_3d_s6']				= 'Authentication Attempt Acknowledged, liability shift';
$_['text_3d_s7']				= 'Incorrect Password Entered, no liability shift';
$_['text_3d_s8']				= 'Authentication Unavailable, no liability shift';
$_['text_3d_s9']				= 'Invalid Response From ACS, no liability shift';
$_['text_3d_s10']				= 'RealMPI Fatal Error, no liability shift';
$_['text_3d_liability']     	= 'No Liability Shift';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';