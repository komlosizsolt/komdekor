<?php
// Text
$_['text_title']				= 'Hitel vagy bank kártya (PayPal)';
$_['text_wait']					= 'Kérem várjon!';
$_['text_credit_card']			= 'Bankkártya adatai';

// Entry
$_['entry_cc_type']				= 'Kártya típusa';
$_['entry_cc_number']			= 'Kártyaszám';
$_['entry_cc_start_date']		= 'A kártya aktiválásának ideje';
$_['entry_cc_expire_date']		= 'A kártya lejárati ideje';
$_['entry_cc_cvv2']				= 'Biztonsági kód (CVV2)';
$_['entry_cc_issue']			= 'Kártyát kibocsájtó bank neve';

// Help
$_['help_start_date']			= '(ha elérhető)';
$_['help_issue']				= '(csak Maestro és Solo kártyák esetén)';