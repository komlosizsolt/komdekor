<?php
// Text
$_['text_title']				= 'Hitel vagy bankkártya';
$_['text_credit_card']			= 'Bankkártya adatai';
$_['text_wait']					= 'Kérem várjon!';
$_['text_result']				= 'Eredmény';
$_['text_message']				= 'Üzenet';
$_['text_cvn_result']			= 'CVN eredmény';
$_['text_avs_postcode']			= 'AVS irányítószám';
$_['text_avs_address']			= 'AVS cím';
$_['text_eci']					= 'ECI (3D secure) eredmény';
$_['text_tss']					= 'TSS eredmény';
$_['text_card_bank']			= 'A kártyát kibocsájtó bank neve';
$_['text_card_country']			= 'Kibocsájtó ország';
$_['text_card_region']			= 'A kártya régiója';
$_['text_last_digits']			= 'Utolsó 4 számjegy';
$_['text_order_ref']			= 'Rendelési szám';
$_['text_timestamp']			= 'Időbélyeg';
$_['text_card_visa']			= 'Visa';
$_['text_card_mc']				= 'Mastercard';
$_['text_card_amex']			= 'American Express';
$_['text_card_switch']			= 'Switch';
$_['text_card_laser']			= 'Laser';
$_['text_card_diners']			= 'Diners';
$_['text_auth_code']			= 'Hitelesítő kód';
$_['text_3d_s1']				= 'Cardholder Not Enrolled, liability shift';
$_['text_3d_s2']				= 'Unable To Verify Enrolment, no liability shift';
$_['text_3d_s3']				= 'Invalid Response From Enrolment Server, no liability shift';
$_['text_3d_s4']				= 'Enrolled, But Invalid Response From ACS (Access Control Server), no liability shift';
$_['text_3d_s5']				= 'Successful Authentication, liability shift';
$_['text_3d_s6']				= 'Authentication Attempt Acknowledged, liability shift';
$_['text_3d_s7']				= 'Incorrect Password Entered, no liability shift';
$_['text_3d_s8']				= 'Authentication Unavailable, no liability shift';
$_['text_3d_s9']				= 'Invalid Response From ACS, no liability shift';
$_['text_3d_s10']				= 'RealMPI Fatal Error, no liability shift';

// Entry
$_['entry_cc_type']				= 'Kártya típusa';
$_['entry_cc_number']			= 'Kártyaszám';
$_['entry_cc_name']				= 'Kártyatulajdonos neve';
$_['entry_cc_expire_date']		= 'A kártya lejárati ideje';
$_['entry_cc_cvv2']				= 'Biztonsági kód (CVV2)';
$_['entry_cc_issue']			= 'Kártyát kibocsájtó bank neve';

// Help
$_['help_start_date']			= '(ha elérhető)';
$_['help_issue']				= '(csak Maestro és Solo kártyák esetén)';

// Error
$_['error_card_number']			= 'Kérem ellenőrizze, hogy valós kártyaszámot adott-e meg';
$_['error_card_name']			= 'Kérem, ellenőrizze a kártyatulajdonos nevének helyességét';
$_['error_card_cvv']			= 'Kérem, ellenőrizze a CVV2 kód helyességét';
$_['error_3d_unable']			= 'A kereskedő megközveteli a 3D Secure használhatát, de nem tudta azonosítani az Ön bankját. Kérjük, próbálja meg később.';
$_['error_3d_500_response_no_payment'] = 'An invalid response was received from the card processor, no payment has been taken';
$_['error_3d_unsuccessful']		= '3D secure azonosítás nem sikerült';