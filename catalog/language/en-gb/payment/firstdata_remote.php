<?php
// Text
$_['text_title']				= 'Hitel vagy bankkártya';
$_['text_credit_card']			= 'Bankkártya adatai';
$_['text_wait']					= 'Kérem várjon!';

// Entry
$_['entry_cc_number']			= 'Kártyaszám';
$_['entry_cc_name']				= 'Kártyatulajdonos neve';
$_['entry_cc_expire_date']		= 'A kártya lejárati ideje';
$_['entry_cc_cvv2']				= 'Biztonsági kód (CVV2)';

// Help
$_['help_start_date']			= '(ha elérhető)';
$_['help_issue']				= '(csak Maestro és Solo kártyák esetén)';

// Text
$_['text_result']				= 'Eredmény: ';
$_['text_approval_code']		= 'Jóváhagyási kód: ';
$_['text_reference_number']		= 'Hivatkozás: ';
$_['text_card_number_ref']		= 'A kártya utolsó 4 számjegye: xxxx ';
$_['text_card_brand']			= 'Kártya típusa: ';
$_['text_response_code']		= 'Válasz kód: ';
$_['text_fault']				= 'Hiba üzenet: ';
$_['text_error']				= 'Hibaüzenet: ';
$_['text_avs']					= 'Cím megőrsítése: ';
$_['text_address_ppx']			= 'Nincs cím információ, vagy a kártya kibocsátója nem ellenőrizte azokat.';
$_['text_address_yyy']			= 'A kártya kibocsátója megerősítette, hogy az irányítószám és az utcanév megegyezik az általa tárolt adatokkal.';
$_['text_address_yna']			= 'A kártya kibocsátója megerősítette, hogy az utcanév megegyezik az általa tárolttal, de az irányítószám nem.';
$_['text_address_nyz']			= 'A kártya kibocsátója megerősítette, hogy az irányítószám megegyezik az általa tárolttal, de az utcanév nem';
$_['text_address_nnn']			= 'Sem az utca név, sem az irányítószám nem egyezik a kártya kibocsátójánál tárolt adatokkal.';
$_['text_address_ypx']			= 'A kártya kibocsátója megerősítette, hogy az utcanév egyezik az általa tárolttal. A kibocsátó nem ellenőrzi az irányítószámot.';
$_['text_address_pyx']			= 'A kártya kibocsátója megerősítette, hogy az irányítószám egyezik az általa tárolttal. A kibocsátó nem ellenőrzi az utcanevet.';
$_['text_address_xxu']			= 'A kártya kibocsátója nem ellenőrzi az AVS információkat';
$_['text_card_code_verify']		= 'Biztonsági kód: ';
$_['text_card_code_m']			= 'A biztonsági kód megegyezik';
$_['text_card_code_n']			= 'A biztonsági kód nem egyezik meg';
$_['text_card_code_p']			= 'Nincs feldolgozva';
$_['text_card_code_s']			= 'A kereskedő jelezte, hogy a kártyán nincs feltüntetve a biztonsági kód';
$_['text_card_code_u']			= 'A kibocsájtó nem hitelesített és / vagy nem továbbította a feloldó kulcsokat ';
$_['text_card_code_x']			= 'No response from the credit card association was received';
$_['text_card_code_blank']		= 'A blank response should indicate that no code was sent and that there was no indication that the code was not present on the card.';
$_['text_card_accepted']		= 'Elfogadott kártyák: ';
$_['text_card_type_m']			= 'Mastercard';
$_['text_card_type_v']			= 'Visa (Hitel/Bank/Electron/Delta)';
$_['text_card_type_c']			= 'Diners';
$_['text_card_type_a']			= 'American Express';
$_['text_card_type_ma']			= 'Maestro';
$_['text_card_new']				= 'Új kártya';
$_['text_response_proc_code']	= 'Feldolgozás kódja: ';
$_['text_response_ref']			= 'Referencia szám: ';

// Error
$_['error_card_number']			= 'Kérem ellenőrizze, hogy valós kártyaszámot adott-e meg';
$_['error_card_name']			= 'Kérem, ellenőrizze a kártyatulajdonos nevének helyességét';
$_['error_card_cvv']			= 'Kérem, ellenőrizze a CVV2 kód helyességét';
$_['error_failed']				= 'A tranzakciót nem sikerült végrehajtani, kérem lépjen kapcsolatba a kereskedővel';