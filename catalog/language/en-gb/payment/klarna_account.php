<?php
// Text
$_['text_title']				= 'Klarna Account - Pay from %s/month';
$_['text_terms']				= '<span id="klarna_account_toc"></span><script type="text/javascript">var terms = new Klarna.Terms.Account({el: \'klarna_account_toc\', eid: \'%s\', country: \'%s\'});</script>';
$_['text_information']			= 'Klarna fiók információk';
$_['text_additional']			= 'A Klarna-nak további információkra van szüksége, mielőtt megkezdhetné megrendelése feldolgozását.';
$_['text_male']					= 'Férfi';
$_['text_female']				= 'Nő';
$_['text_year']					= 'Év';
$_['text_month']				= 'Hónap';
$_['text_day']					= 'Nap';
$_['text_payment_option']		= 'Fizetési beállítások';
$_['text_single_payment']		= 'Egyszeri befizetés';
$_['text_monthly_payment']		= '%s - %s havonta';
$_['text_comment']				= 'Klarna számlaszám: %s
%s/%s: %.4f';

// Entry
$_['entry_gender']				= 'Nem (Férfi / Nő)';
$_['entry_pno']					= 'Személyi igazolvány száma';
$_['entry_dob']					= 'Születési dátum';
$_['entry_phone_no']			= 'Telefonszám';
$_['entry_street']				= 'Utca';
$_['entry_house_no']			= 'Házszám';
$_['entry_house_ext']			= 'Házszám kiegészítő adatok (A,B, 3. emelet, stb)';
$_['entry_company']				= 'Cégjegyzék szám';

// Help
$_['help_pno']					= 'Kérjük, adja meg társadalombiztosítási számát (TAJ)';
$_['help_phone_no']				= 'Kérjük, adja meg telefonszámát.';
$_['help_street']				= 'Felhívjuk figyelmét, hogy a szállításra csak akkor kerülhet sor, ha az a Klarna Számlában regisztrált címre történik.';
$_['help_house_no']				= 'Kérjük, adja meg házszámát.';
$_['help_house_ext']			= 'Kérjük adja meg kiegészítő adatait a házszámhoz (A, B, C,  3. emelet, stb.).';
$_['help_company']				= 'Kérjük, adja meg cégjegyzékszámát';

// Error
$_['error_deu_terms']			= 'El kell fogadnia a Klarna Számla adatvédelmi nyilatkozatát';
$_['error_address_match']		= 'Amennyiben a Klarna rendszerével szeretne fizetni, a szállítási és számlázási címnek egyeznie kell.';
$_['error_network']				= 'Hiba történt a Klarna Számlához való kapcsolódás során. Kérjük, próbálja meg később.';