<?php
// Heading
$_['text_title']				= 'Hitelkártya / Bankkártya (First Data)';

// Button
$_['button_confirm']			= 'Folytatás';

// Text
$_['text_new_card']				= 'Új kártya';
$_['text_store_card']			= 'Jegyezze meg a kártyám adatait';
$_['text_address_response']		= 'Cím megőrsítése: ';
$_['text_address_ppx']			= 'Nincs cím információ, vagy a kártya kibocsátója nem ellenőrizte azokat.';
$_['text_address_yyy']			= 'A kártya kibocsátója megerősítette, hogy az irányítószám és az utcanév megegyezik az általa tárolt adatokkal.';
$_['text_address_yna']			= 'A kártya kibocsátója megerősítette, hogy az utcanév megegyezik az általa tárolttal, de az irányítószám nem.';
$_['text_address_nyz']			= 'A kártya kibocsátója megerősítette, hogy az irányítószám megegyezik az általa tárolttal, de az utcanév nem';
$_['text_address_nnn']			= 'Sem az utca név, sem az irányítószám nem egyezik a kártya kibocsátójánál tárolt adatokkal.';
$_['text_address_ypx']			= 'A kártya kibocsátója megerősítette, hogy az utcanév egyezik az általa tárolttal. A kibocsátó nem ellenőrzi az irányítószámot.';
$_['text_address_pyx']			= 'A kártya kibocsátója megerősítette, hogy az irányítószám egyezik az általa tárolttal. A kibocsátó nem ellenőrzi az utcanevet.';
$_['text_address_xxu']			= 'A kártya kibocsátója nem ellenőrzi az AVS információkat';
$_['text_card_code_verify']		= 'Biztonsági kód: ';
$_['text_card_code_m']			= 'A biztonsági kód megegyezik';
$_['text_card_code_n']			= 'A biztonsági kód nem egyezik meg';
$_['text_card_code_p']			= 'Nincs feldolgozva';
$_['text_card_code_s']			= 'A kereskedő jelezte, hogy a kártyán nincs feltüntetve a biztonsági kód';
$_['text_card_code_u']			= 'A kibocsájtó nem hitelesített és / vagy nem továbbította a feloldó kulcsokat ';
$_['text_card_code_x']			= 'No response from the credit card association was received';
$_['text_card_code_blank']		= 'A blank response should indicate that no code was sent and that there was no indication that the code was not present on the card.';
$_['text_card_type_m']			= 'Mastercard';
$_['text_card_type_v']			= 'Visa (Hitel/Bank/Electron/Delta)';
$_['text_card_type_c']			= 'Diners';
$_['text_card_type_a']			= 'American Express';
$_['text_card_type_ma']			= 'Maestro';
$_['text_card_type_mauk']		= 'Maestro UK/Solo';
$_['text_response_code_full']	= 'Jóváhagyási kód: ';
$_['text_response_code']		= 'Teljes válasz kód:';
$_['text_response_card']		= 'Card used: ';
$_['text_response_card_type']	= 'Kártya típus: ';
$_['text_response_proc_code']	= 'Feldolgozás kódja: ';
$_['text_response_ref']			= 'Referencia szám: ';

// Error
$_['error_failed']				= 'Nem sikerült a tranzakció, kérjük próbálja újra';