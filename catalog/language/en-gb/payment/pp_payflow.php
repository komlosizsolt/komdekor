<?php
// Text
$_['text_title']				= 'Hitel vagy bank kártya (PayPal)';
$_['text_credit_card']			= 'Bankkártya adatai';
$_['text_start_date']			= '(ha elérhető)';
$_['text_issue']				= '(csak Maestro és Solo kártyák esetén)';
$_['text_wait']					= 'Kérem várjon!';

// Entry
$_['entry_cc_owner']			= 'Kártyatulajdonos neve:';
$_['entry_cc_type']				= 'Kártya típus: ';
$_['entry_cc_number']			= 'Kártya száma:';
$_['entry_cc_start_date']		= 'Kártya érvényességének kezdete:';
$_['entry_cc_expire_date']		= 'A kártya lejárati dátuma:';
$_['entry_cc_cvv2']				= 'Biztonsági kód (CVV2):';
$_['entry_cc_issue']			= 'A kártyát kibocsájtó bank neve:';

// Error
$_['error_required']			= 'Figyelem: minden, a fizetéssel kapcsolatos mező kitöltése kötelező.';
$_['error_general']				= 'Figyelem: a tranzakció feldolgozása közben központi hiba történt, kérjük, próbálja újra.';
$_['error_config']				= 'Figyelem: A fizetési modul beállításai hibásak. Kérjük, erősítse meg belépési adatait.';
$_['error_address']				= 'Figyelem: A fizetésnél megadott település név, megye név és irányítószám hibás. Kérjük, próbálja újra.';
$_['error_declined']			= 'Figyelem: A tranzakció nem sikerült. Kérjük, próbálja újra.';
$_['error_invalid']				= 'Figyelem: A megadott kártya adatai hibásak. Kérjük, próbálja újra.';