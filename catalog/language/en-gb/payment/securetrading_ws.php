<?php
$_['text_title'] = 'Hitelkártya / Bankkártya';
$_['text_card_details'] = 'Kártya adatai';
$_['text_wait'] = 'A tranzakció feldolgozás alatt';
$_['text_auth_code'] = 'Hitelesítő kód: %s';
$_['text_postcode_check'] = 'Irányítószám ellenőrzés: %s';
$_['text_security_code_check'] = 'CVV2 ellenőrzés: %s';
$_['text_address_check'] = 'Cím ellenőrzés: %s';
$_['text_3d_secure_check'] = '3D Secure ellenőrzés: %s';
$_['text_not_given'] = 'Nem adott';
$_['text_not_checked'] = 'Nem elleőrzött';
$_['text_match'] = 'Egyező';
$_['text_not_match'] = 'Nem egyező';
$_['text_authenticated'] = 'Azonosított';
$_['text_not_authenticated'] = 'Nem azonosított';
$_['text_authentication_not_completed'] = 'Attempted but not completed';
$_['text_unable_to_perform'] = 'Nem teljesíthető';
$_['text_transaction_declined'] = 'A tranzakciót az Ön bankja visszautasította, kérem válasszon másik fizetési módot.';
$_['text_transaction_failed'] = 'A tranzakció feldolgozását nem tudjuk megkezdeni, kérjük ellenőrizze a megadott adatok helyességét.';
$_['text_connection_error'] = 'Kérjük, próbálja meg később vagy válasszon másik fizetési módot';

$_['entry_type'] = 'Kártya típusa';
$_['entry_number'] = 'Kártyaszám';
$_['entry_expire_date'] = 'Lejárati dátum';
$_['entry_cvv2'] = 'Biztonsági kód (CVV2)';

$_['button_confirm'] = 'Megerősít';

$_['error_failure'] = 'Nem tudjuk befejezni a tranzakciót. Kérjük, próbálja meg később, vagy válasszon másik fizetési módot.';