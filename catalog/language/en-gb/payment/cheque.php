<?php
// Text
$_['text_title']				= 'Csekk / Előreutalás';
$_['text_instruction']			= 'Csekk / Előreutalásos rendelés részletei';
$_['text_payable']				= 'Számlázási cím:';
$_['text_address']				= 'Címzett:';
$_['text_payment']				= 'Az Ön rendelését addig nem szállítjuk, amíg a fizetés meg nem történik.';