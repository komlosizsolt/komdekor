<?php
// Heading
$_['heading_title']				= 'Köszönjük vásárlását a(z) %s keresztül...';

// Text
$_['text_title']				= 'Hitelkártya / Bankkártya (PayPoint)';
$_['text_response']				= 'Válasszon a PayPoint-tól:';
$_['text_success']				= 'Az Ön vételára sikeresen megérkezett.';
$_['text_success_wait']			= '<b><span style="color: #FF0000">Kérjük várjon,</span></b> mialatt befejezzük rendelésének feldolgozását.<br>Ha nem kerül átirányításra 10 mp-en belül, kérjük kattintson <a href="%s">ide</a>.';
$_['text_failure']				= '... Az Ön tranzakciója törölve lett!';
$_['text_failure_wait']			= '<b><span style="color: #FF0000">Kérjük várjon...</span></b><br>Ha nem kerül átirányításra 10 mp-en belül, kérjük kattintson <a href="%s">ide</a>.';