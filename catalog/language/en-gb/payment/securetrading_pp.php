<?php

$_['text_title'] = 'Hitelkártya / Bankkártya';
$_['button_confirm'] = 'Megerősít';

$_['text_postcode_check'] = 'Irányítószám ellenőrzés: %s';
$_['text_security_code_check'] = 'CVV2 ellenőrzés: %s';
$_['text_address_check'] = 'Cím ellenőrzés: %s';
$_['text_not_given'] = 'Nem adott';
$_['text_not_checked'] = 'Nem elleőrzött';
$_['text_match'] = 'Egyező';
$_['text_not_match'] = 'Nem egyező';
$_['text_payment_details'] = 'Fizetési adatok';

$_['entry_card_type'] = 'Kártya típusa';