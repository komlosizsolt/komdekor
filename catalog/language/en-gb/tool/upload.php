<?php
// Text
$_['text_upload']    = 'A fájl sikeresen feltöltve!';

// Error
$_['error_filename'] = 'A fájlnév minimum 3 maximum 64 karakter lehet!';
$_['error_filetype'] = 'Érvénytelen fájltípus!';
$_['error_upload']   = 'Feltöltés szükséges!';
