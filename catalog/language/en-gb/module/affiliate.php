<?php
// Heading
$_['heading_title']    = 'Affiliate';

// Text
$_['text_register']    = 'Regisztráció';
$_['text_login']       = 'Belépés';
$_['text_logout']      = 'Kilépés';
$_['text_forgotten']   = 'Elfelejtett jelszó';
$_['text_account']     = 'Fiókom';
$_['text_edit']        = 'Profilom szerkesztése';
$_['text_password']    = 'Jelszó';
$_['text_payment']     = 'Kifizetési módok';
$_['text_tracking']    = 'Affiliate követés';
$_['text_transaction'] = 'Tranzakcióim';
