<?php
// Heading
$_['heading_title']    = 'Fiók';

// Text
$_['text_register']    = 'Regisztráció';
$_['text_login']       = 'Belépés';
$_['text_logout']      = 'Kilépés';
$_['text_forgotten']   = 'Elfelejtett jelszó';
$_['text_account']     = 'Fiókom';
$_['text_edit']        = 'Profilom szerkesztése';
$_['text_password']    = 'Jelszó';
$_['text_address']     = 'Címjegyzékem';
$_['text_wishlist']    = 'Kívánságlistám';
$_['text_order']       = 'Rendeléseim';
$_['text_download']    = 'Letöltéseim';
$_['text_reward']      = 'Hűségpontjaim';
$_['text_return']      = 'Termék(ek) visszaküldése';
$_['text_transaction'] = 'Tranzakcióim';
$_['text_newsletter']  = 'Hírlevél beállításaim';
$_['text_recurring']   = 'Előfizetéseim';