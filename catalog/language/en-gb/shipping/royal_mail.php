<?php
// Text
$_['text_title']                        = 'Nemzeti posta';
$_['text_weight']                       = 'Súly:';
$_['text_insurance']                    = 'Összeg biztosítva:';
$_['text_special_delivery']             = 'Special Delivery Next Day';
$_['text_1st_class_signed']             = 'First Class Signed Post';
$_['text_2nd_class_signed']             = 'Second Class Signed Post';
$_['text_1st_class_standard']           = 'Elsőosztályú Standard küldés';
$_['text_2nd_class_standard']           = 'Másodosztályú Standard küldés';
$_['text_international_standard']       = 'International Standard';
$_['text_international_tracked_signed'] = 'International Tracked & Signed';
$_['text_international_tracked']        = 'International Tracked';
$_['text_international_signed']         = 'Nemzetközileg aláírt';
$_['text_international_economy']        = 'International Economy';