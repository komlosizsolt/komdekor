<?php
// Text
$_['text_title']                               = 'Fedex';
$_['text_weight']                              = 'Súly:';
$_['text_eta']                                 = 'Várható idő:';
$_['text_europe_first_international_priority'] = 'Europe First International Priority';
$_['text_fedex_1_day_freight']                 = 'Fedex 1 Day Freight';
$_['text_fedex_2_day']                         = 'Fedex 2 napos';
$_['text_fedex_2_day_am']                      = 'Fedex 2 napos délelőtt';
$_['text_fedex_2_day_freight']                 = 'Fedex 2 Day Freight';
$_['text_fedex_3_day_freight']                 = 'Fedex 3 Day Freight';
$_['text_fedex_express_saver']                 = 'Fedex Express Saver';
$_['text_fedex_first_freight']                 = 'Fedex First Fright';
$_['text_fedex_freight_economy']               = 'Fedex Fright Economy';
$_['text_fedex_freight_priority']              = 'Fedex Fright Priority';
$_['text_fedex_ground']                        = 'Fedex Ground';
$_['text_first_overnight']                     = 'Egynapos éjszakai';
$_['text_ground_home_delivery']                = 'Ground Home Delivery';
$_['text_international_economy']               = 'Nemzetközi gazdaságos';
$_['text_international_economy_freight']       = 'Nemzetközi gazdaságos Freight';
$_['text_international_first']                 = 'Nemzetközi First';
$_['text_international_priority']              = 'Nemzetközi elsőbbségi';
$_['text_international_priority_freight']      = 'Nemzetközi Priority Freight';
$_['text_priority_overnight']                  = 'Éjszakai elsőbbségi';
$_['text_smart_post']                          = 'Smart Post';
$_['text_standard_overnight']                  = 'Standard éjszakai';