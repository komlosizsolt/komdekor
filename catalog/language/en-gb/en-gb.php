<?php
// Locale
$_['code']                  = 'hu';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'Y-m-d';
$_['date_format_long']      = 'Y. F dS, l';
$_['time_format']           = 'H:i:s';
$_['datetime_format']       = 'Y. m. d. H:i:s';
$_['decimal_point']         = ',';
$_['thousand_point']        = ' ';

// Text
$_['text_home']             = 'Főoldal';
$_['text_yes']              = 'Igen';
$_['text_no']               = 'Nem';
$_['text_none']             = ' --- Nincs --- ';
$_['text_select']           = ' --- Válasszon --- ';
$_['text_all_zones']        = 'Összes zóna';
$_['text_pagination']       = 'Tételek: %d - %d / %d (%d oldal)';
$_['text_loading']          = 'Betöltés...';
$_['text_no_results']       = 'No results!';

// Buttons
$_['button_address_add']    = 'Cím hozzáadása';
$_['button_back']           = 'Vissza';
$_['button_continue']       = 'Tovább';
$_['button_cart']           = 'Kosárba';
$_['button_cancel']         = 'Mégsem';
$_['button_compare']        = 'Összehasonlítás';
$_['button_wishlist']       = 'Kívánságlistára';
$_['button_checkout']       = 'Fizetés';
$_['button_confirm']        = 'Rendelés megerősítése';
$_['button_coupon']         = 'Kupon beváltása';
$_['button_delete']         = 'Törlés';
$_['button_download']       = 'Letöltés';
$_['button_edit']           = 'Szerkesztés';
$_['button_filter']         = 'Keresés pontosítása';
$_['button_new_address']    = 'Új cím';
$_['button_change_address'] = 'Cím változtatás';
$_['button_reviews']        = 'Vélemények';
$_['button_write']          = 'Vélemény írása';
$_['button_login']          = 'Belépés';
$_['button_update']         = 'Frissítés';
$_['button_remove']         = 'Törlés';
$_['button_reorder']        = 'Újrarendelés';
$_['button_return']         = 'Visszaküldés';
$_['button_shopping']       = 'A vásárlás folytatása';
$_['button_search']         = 'Keresés';
$_['button_shipping']       = 'Szállítás engedélyezése';
$_['button_submit']         = 'Küldés';
$_['button_guest']          = 'Vásárlás vendégként';
$_['button_view']           = 'Megtekintés';
$_['button_voucher']        = 'Ajándékutalvány beváltása';
$_['button_upload']         = 'Fájl feltöltése';
$_['button_reward']         = 'Pontok beváltása';
$_['button_quote']          = 'Árajánlat kérése';
$_['button_list']           = 'Lista';
$_['button_grid']           = 'Rács';
$_['button_map']            = 'Google térkép';

// Error
$_['error_exception']       = 'HIBA: kód(%s): %s  %s sor: %s';
$_['error_upload_1']        = 'A feltölteni kívánt fájl mérete meghaladja a php.ini fájlban beállított upload_max_filesize értékét!';
$_['error_upload_2']        = 'A feltölteni kívánt fájl mérete meghaladja a MAX_FILE_SIZE értékét, amit a HTML űrlapon lett megadva!';
$_['error_upload_3']        = 'A feltöltött fájl csak részben lett feltöltve!';
$_['error_upload_4']        = 'Nincs fájl ami feltölthető!';
$_['error_upload_6']        = 'Az ideiglenes könyvtár hiányzik!';
$_['error_upload_7']        = 'Nem írható a fájl a tárhelyen!';
$_['error_upload_8']        = 'A fájl feltöltést megszakította a kiegészítő!';
$_['error_upload_999']      = 'Nem létező hibakód!';
$_['error_curl']            = 'CURL: Error Code(%s): %s';
