<?php
// Text
$_['text_search']              = 'Keresés';
$_['text_brand']               = 'Márka';
$_['text_manufacturer']        = 'Gyártó:';
$_['text_model']               = 'Cikkszám:';
$_['text_reward']              = 'Hűségpont:';
$_['text_points']              = 'Ár hűségpontokban pontokban:';
$_['text_stock']               = 'Készlet információ:';
$_['text_instock']             = 'Készleten';
$_['text_tax']                 = 'Nettó:';
$_['text_discount']            = '%s vagy több %s';
$_['text_option']              = 'Lehetséges opciók';
$_['text_minimum']             = 'Ennek a terméknek a minimális rendelési mennyisége %s db';
$_['text_reviews']             = '%s vélemény';
$_['text_write']               = 'Írjon véleményt a termékről';
$_['text_login']               = 'A megtekintéshez <a href="%s">belépés</a> vagy <a href="%s">regisztráció</a> szükséges';
$_['text_no_reviews']          = 'Nincsenek vélemények erről a termékről.';
$_['text_note']                = '<span style="color: #FF0000;">Megjegyzés:</span> HTML kódok nem engedélyezettek!';
$_['text_success']             = 'Köszönjük, hogy megírta véleményét, továbbítottuk azt jóváhagyásra.';
$_['text_related']             = 'Kapcsolódó termékek';
$_['text_tags']                = 'Címkék:';
$_['text_error']               = 'Termék nem található!';
$_['text_payment_recurring']   = 'Fizetési profil';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s minden %d %s(s) amíg törlik';
$_['text_day']                 = 'nap';
$_['text_week']                = 'hét';
$_['text_semi_month']          = 'fél hónap';
$_['text_month']               = 'hónap';
$_['text_year']                = 'év';

// Entry
$_['entry_qty']                = 'Menyniség';
$_['entry_name']               = 'Név';
$_['entry_review']             = 'Az Ön véleménye:';
$_['entry_rating']             = 'Értékelés:';
$_['entry_good']               = 'Jó';
$_['entry_bad']                = 'Rossz';

// Tabs
$_['tab_description']          = 'Leírás';
$_['tab_attribute']            = 'Jellemzők';
$_['tab_review']               = 'Vélemények (%s)';

// Error
$_['error_name']               = 'A vélemény nevének 3 és 25 karakter között kell lennie!';
$_['error_text']               = 'A vélemény szövegének 25 és 1000 karakter között kell lennie!';
$_['error_rating']             = 'Kérjük, válasszon értékelési besorolást!';
