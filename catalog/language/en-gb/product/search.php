<?php
// Heading
$_['heading_title']     = 'Keresés';
$_['heading_tag']		= 'Cimke - ';

// Text
$_['text_search']       = 'A keresési feltételeknek megfelelő termék(ek)';
$_['text_keyword']      = 'Kulcsszavak';
$_['text_category']     = 'Összes kategória';
$_['text_sub_category'] = 'Keresés az alkategóriákban';
$_['text_empty']        = 'Nincs a keresési feltételeknek megfelelő termék(ek).';
$_['text_quantity']     = 'Darab:';
$_['text_manufacturer'] = 'Gyártó:';
$_['text_model']        = 'Cikkszám:';
$_['text_points']       = 'Hűségpontok:';
$_['text_price']        = 'Ár:';
$_['text_tax']          = 'Nettó:';
$_['text_reviews']      = '%s véleményen alapján.';
$_['text_compare']      = 'Termékek összehasonlítása (%s)';
$_['text_sort']         = 'Rendezés:';
$_['text_default']      = 'Alapértelmezett';
$_['text_name_asc']     = 'Név (A - Z)';
$_['text_name_desc']    = 'Név (Z - A)';
$_['text_price_asc']    = 'Ár (alacsony > magas)';
$_['text_price_desc']   = 'Ár (magas > alcsony)';
$_['text_rating_asc']   = 'Értékelés (legalacsonyabb)';
$_['text_rating_desc']  = 'Értékelés (legmagasabb)';
$_['text_model_asc']    = 'Cikkszám (A - Z)';
$_['text_model_desc']   = 'Cikkszám (Z - A)';
$_['text_limit']        = 'Listázás:';

// Entry
$_['entry_search']      = 'Keresés:';
$_['entry_description'] = 'Keresés a termék leírásokban';