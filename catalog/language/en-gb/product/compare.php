<?php
// Heading
$_['heading_title']     = 'Termékek összehasonlítása';

// Text
$_['text_product']      = 'Termék adatok';
$_['text_name']         = 'Megnevezés';
$_['text_image']        = 'Kép';
$_['text_price']        = 'Ár';
$_['text_model']        = 'Cikkszám';
$_['text_manufacturer'] = 'Gyártó';
$_['text_availability'] = 'Készlet információ';
$_['text_instock']      = 'Készleten';
$_['text_rating']       = 'Értékelés';
$_['text_reviews']      = '%s vélemény alapján.';
$_['text_summary']      = 'Összesítés';
$_['text_weight']       = 'Súly';
$_['text_dimension']    = 'Méretek ( H x Sz x M )';
$_['text_compare']      = 'Termékek összehasonlítása (%s)';
$_['text_success']      = 'Sikeresen hozzáadta a(z) <a href="%s">%s</a> az Ön <a href="%s">termék összehasonlításához</a>';
$_['text_remove']       = 'Sikeresen eltávolította a terméket az összehasonlításból.';
$_['text_empty']        = 'Kérem, válasszon termékeket az összehasonlításhoz.';