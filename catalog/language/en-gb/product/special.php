<?php
// Heading
$_['heading_title']     = 'Akciós termék(ek)';

// Text
$_['text_empty']        = 'Jelenleg nincs akciós termék(ek).';
$_['text_quantity']     = 'Mennyiség:';
$_['text_manufacturer'] = 'Gyártó:';
$_['text_model']        = 'Cikkszám:';
$_['text_points']       = 'Hűségpontok:';
$_['text_price']        = 'Ár:';
$_['text_tax']          = 'Nettó:';
$_['text_compare']      = 'Termékek összehasonlítása (%s)';
$_['text_sort']         = 'Rendezés:';
$_['text_default']      = 'Alapértelmezett';
$_['text_name_asc']     = 'Név (A - Z)';
$_['text_name_desc']    = 'Név (Z - A)';
$_['text_price_asc']    = 'Ár (alacsony > magas)';
$_['text_price_desc']   = 'Ár (magas > alcsony)';
$_['text_rating_asc']   = 'Értékelés (legalacsonyabb)';
$_['text_rating_desc']  = 'Értékelés (legmagasabb)';
$_['text_model_asc']    = 'Cikkszám (A - Z)';
$_['text_model_desc']   = 'Cikkszám (Z - A)';
$_['text_limit']        = 'Listázás:';