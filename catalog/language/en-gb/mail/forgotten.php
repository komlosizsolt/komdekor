<?php
// Text
$_['text_subject']  = 'Új jelszava a(z) %s áruházban';
$_['text_greeting'] = 'Új jelszó lett igényelve %s áruházhoz.';
$_['text_change']   = 'To reset your password click on the link below:';
$_['text_ip']       = 'The IP used to make this request was: %s';
