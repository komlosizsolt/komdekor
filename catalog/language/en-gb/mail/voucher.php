<?php
// Text
$_['text_subject']  = '%s küldött Önnek egy ajándékutalványt';
$_['text_greeting'] = 'Gratulálunk, Ön kapott egy ajándék utalványt %s értékben';
$_['text_from']     = 'Ezt az ajándék utalványt a(z) %s küldte';
$_['text_message']  = 'Utalvány üzenete';
$_['text_redeem']   = 'Az ajándék utalvány beváltásához írja le a <b>%s</b> kódot, azután kattintson a lenti linkre és vásárolja meg a terméket amihez fel kívánja használni az ajándék utalványt. Az ajándék utalvány kódját a kosár oldalán írhatja be, mielőtt véglegesíti azt.';
$_['text_footer']   = 'Kérjük, ha bármi kérdése van válaszoljon erre az e-mailre.';
