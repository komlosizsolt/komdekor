<?php
// Text
$_['text_subject']		        = 'Köszöntjük a(z) %s  Affiliate programjában';
$_['text_welcome']		        = 'Köszönjük, hogy csatlakozott a(z) %s partnerprogramjához!';
$_['text_login']                = 'Your account has now been created and you can log in by using your e-mail address and password by visiting our website or at the following URL:';
$_['text_approval']		        = 'Fiókjának engedélyezettnek kell lennie a belépéshez. Engedélyezés után beléphet az e-mail címével és jelszavával a web áruházunkban vagy a következő hivatkozáson:';
$_['text_services']		        = 'Belépés után készíthet követő kódokat, nyomon követheti a jutalék kifizetéseket és szerkesztheti fiókjának adatait.';
$_['text_thanks']		        = 'Köszönjük a regisztrációját.';
$_['text_new_affiliate']        = 'Új Affiliate';
$_['text_signup']		        = 'Egy új partner jelentkezett:';
$_['text_store']		        = 'Áruház:';
$_['text_firstname']	        = 'Keresztnév:';
$_['text_lastname']		        = 'Vezetéknév:';
$_['text_company']		        = 'Cégnév:';
$_['text_email']		        = 'E-Mail:';
$_['text_telephone']	        = 'Telefon:';
$_['text_website']		        = 'Weboldal:';
$_['text_order_id']             = 'Rendelés azonosító:';
$_['text_transaction_subject']  = '%s - Affiliate jutalék';
$_['text_transaction_received'] = '%s jutalék jóváírva!';
$_['text_transaction_total']    = 'Your total amount of commission is now %s.';
