<?php
// Text
$_['text_subject']	= '%s - Termék értékelés';
$_['text_waiting']	= 'Önnek egy új termék értékelése vár feldolgozásra.';
$_['text_product']	= 'Termék: %s';
$_['text_reviewer']	= 'Értékelő neve: %s';
$_['text_rating']	= 'Értékelés: %s';
$_['text_review']	= 'Értékelés szövege:';