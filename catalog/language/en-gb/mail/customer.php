<?php
// Text
$_['text_subject']        = 'Köszönjük regisztrációját a(z) %s áruházban';
$_['text_welcome']        = 'Üdvözöljük és köszönjük regisztrációját a(z) %s!';
$_['text_login']          = 'Fiókja most már elkészült és beléphet e-mail címével és jelszavával web áruházunkban vagy a következő hivatkozás használatával:';
$_['text_approval']       = 'Fiókjának engedélyezettnek kell lennie a belépéshez. Engedélyezés után beléphet az e-mail címével és jelszavával a web áruházunkban vagy a következő hivatkozáson:';
$_['text_services']       = 'Belépés után elérhet más szolgáltatásokat mint előző rendeléseinek megtekintése, számlák nyomtatása és fiók adatainak szerkesztése.';
$_['text_thanks']         = 'Köszönjük a regisztrációját.';
$_['text_new_customer']   = 'Új Vásárló';
$_['text_signup']         = 'Belépve:';
$_['text_website']        = 'Weboldal:';
$_['text_customer_group'] = 'Vásárlói csoport:';
$_['text_firstname']      = 'Keresztnév:';
$_['text_lastname']       = 'Vezetéknév:';
$_['text_email']          = 'E-mail:';
$_['text_telephone']      = 'Telefon:';