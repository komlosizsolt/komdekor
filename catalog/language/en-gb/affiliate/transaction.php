<?php
// Heading
$_['heading_title']      = 'Eddigi tranzakciók';

// Column
$_['column_date_added']  = 'Hozzáadva';
$_['column_description'] = 'Leírás';
$_['column_amount']      = 'Érték (%s)';

// Text
$_['text_account']       = 'Fiók';
$_['text_transaction']   = 'Eddigi tranzakciók';
$_['text_balance']       = 'Az aktuális egyenlege:';
$_['text_empty']         = 'Önnek még nem voltak tranzakciói!';