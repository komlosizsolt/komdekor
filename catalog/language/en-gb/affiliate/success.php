<?php
// Heading
$_['heading_title'] = 'Partner fiókja elkészült!';

// Text
$_['text_message']  = '<p>Congratulations! Your new account has been successfully created!</p> <p>You are now a member of %s affiliates.</p> <p>If you have ANY questions about the operation of this affiliate system, please e-mail the store owner.</p> <p>A confirmation has been sent to the provided e-mail address. If you have not received it within the hour, please <a href="%s">contact us</a>.</p>';
$_['text_approval'] = '<p>Köszönjük, hogy a(z) %s partner programjába regisztrált!</p><p>A fiókja jóváhagyásáról e-mail-ben értesíti a web áruház üzemeltetője.</p><p>Ha bármilyen kérdése lenne a partner program mûködésével kapcsolatban, lépjen kapcsolatba <a href="%s">velünk</a>.</p>';
$_['text_account']  = 'Fiók';
$_['text_success']  = 'Sikeres regisztráció';
