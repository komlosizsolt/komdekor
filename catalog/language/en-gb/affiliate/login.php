<?php
// Heading
$_['heading_title']                 = 'Affiliate Program';

// Text
$_['text_account']                  = 'Fiók';
$_['text_login']                    = 'Belépés';
$_['text_description']              = '<p>A(z) %s partnerprogram ingyenes, és lehetővé teszi a tagoknak, hogy bevételekhez jusson azáltal, hogy elhelyez egy vagy több linket a weboldalán, amely hirdeti a(z) %s-t, vagy egy speciális termékét. Ha egy vevő ezekre a linkekre kattintva vásárol, jutalékot kap érte a vásárlás után. Az alapjutalék jelenleg: %s.</p><p>További információért kérem látokassa meg a GYIK-et, vagy tekintse meg a Partnerprogram Általános Szerződési Feltételeit.</p>';
$_['text_new_affiliate']            = 'Új regisztráció';
$_['text_register_account']         = '<p>Még nem vagyok tag.</p><p>Új Affiliate partner fiók létrehozásához kattintson a tovább gombra. Kérjük, vegye figyelembe, hogy ez nem kapcsolódik semmilyen módon a ügyfél fiókhoz.</p>';
$_['text_returning_affiliate']      = 'Affiliate belépés';
$_['text_i_am_returning_affiliate'] = 'Már rendelkezem Affiliate fiókkal.';
$_['text_forgotten']                = 'Elfelejtett jelszó';

// Entry
$_['entry_email']                   = 'E-mail cím:';
$_['entry_password']                = 'Jelszó';

// Error
$_['error_login']                   = 'Figyelem: érvénytelen e-mail cím és/vagy jelszó.';
$_['error_attempts']                = 'Figyelem: többször kísérelt meg belépni sikertelenül, ezért fiókját korlátoztuk. Kérjük, próbálja újra egy órával később.';
$_['error_approved']                = 'A fiókjába belépni az engedélyezés után tud.';