<?php
// Heading
$_['heading_title']   = 'Elfelejtette a jelszavát?';

// Text
$_['text_account']    = 'Fiók';
$_['text_forgotten']  = 'Elfelejtett jelszó';
$_['text_your_email'] = 'Az Ön e-mail címe';
$_['text_email']      = 'Adja meg a fiókjához tartozó e-mail címet. Majd kattintson a Tovább gombra, és küldjük Önnek e-mailben az új jelszavát.';
$_['text_success']    = 'Az új jelszót elküldtük az e-mail címére.';

// Entry
$_['entry_email']     = 'E-mail cím';

// Error
$_['error_email']     = 'Ilyen e-mail cím nem található, kérjük, hogy próbálja meg újra!';
$_['error_approved']  = 'Warning: Your account requires approval before you can login.';
