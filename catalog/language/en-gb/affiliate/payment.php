<?php
// Heading
$_['heading_title']             = 'Fizetési mód';

// Text
$_['text_account']              = 'Fiók';
$_['text_payment']              = 'Fizetés';
$_['text_your_payment']         = 'Fizetési információk';
$_['text_your_password']        = 'Jelszó';
$_['text_cheque']               = 'Csekk';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Banki ';
$_['text_success']              = 'Sikeresen módosította fiókja adatait.';

// Entry
$_['entry_tax']                 = 'Adóazonosító';
$_['entry_payment']             = 'Fizetési mód';
$_['entry_cheque']              = 'Ellenőrizze a kedvezményezett nevét';
$_['entry_paypal']              = 'PayPal e-mail cím';
$_['entry_bank_name']           = 'Bank neve';
$_['entry_bank_branch_number']  = 'ABA/BSB szám';
$_['entry_bank_swift_code']     = 'SWIFT kód';
$_['entry_bank_account_name']   = 'Bankszámla';
$_['entry_bank_account_number'] = 'Banksz';