<?php
// Heading
$_['heading_title']             = 'Affiliate Program';

// Text
$_['text_account']              = 'Fiók';
$_['text_register']             = 'Partner regisztráció';
$_['text_account_already']      = 'Ha már regisztrált tagunk, akkor kérjük, hogy jelentkezzen be a <a href="%s">belépési</a> oldalon.';
$_['text_signup']               = 'A regisztrációhoz kérjük töltse ki az alábbi mezőket:';
$_['text_your_details']         = 'Személyes adatok';
$_['text_your_address']         = 'Lakcím adatok';
$_['text_payment']              = 'Fizetési információk';
$_['text_your_password']        = 'Jelszó';
$_['text_cheque']               = 'Csekk';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Banki átutalás';
$_['text_agree']                = 'Elolvastam és elfogadom a(z) <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>.';

// Entry
$_['entry_firstname']           = 'Keresztnév';
$_['entry_lastname']            = 'Vezetéknév';
$_['entry_email']               = 'E-mail';
$_['entry_telephone']           = 'Telefon';
$_['entry_fax']                 = 'Fax';
$_['entry_company']             = 'Cégnév';
$_['entry_website']             = 'Weboldal';
$_['entry_address_1']           = 'Cím';
$_['entry_address_2']           = 'További cím';
$_['entry_postcode']            = 'Irányítószám';
$_['entry_city']                = 'Település';
$_['entry_country']             = 'Ország';
$_['entry_zone']                = 'Megye';
$_['entry_tax']                 = 'Adóazonosító';
$_['entry_payment']             = 'Fizetési mód';
$_['entry_cheque']              = 'Ellenőrizze a kedvezményezett nevét';
$_['entry_paypal']              = 'PayPal e-mail cím';
$_['entry_bank_name']           = 'Bank neve';
$_['entry_bank_branch_number']  = 'ABA/BSB szám';
$_['entry_bank_swift_code']     = 'SWIFT kód';
$_['entry_bank_account_name']   = 'Bankszámla';
$_['entry_bank_account_number'] = 'Bankszámlaszám:';
$_['entry_password']            = 'Jelszó';
$_['entry_confirm']             = 'Jelszó mégegyszer';

// Error
$_['error_exists']              = 'Ez az e-mail cím már regisztrálva van!';
$_['error_firstname']           = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']            = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']               = 'A megadott e-mail cím nem valós!';
$_['error_telephone']           = 'A telefonszám legalább 1 és legfeljebb 32 karakterből állhat!';
$_['error_password']            = 'A jelszó minimum 4 maximum 20 karakter lehet!';
$_['error_confirm']             = 'A jelszó megerősítése sikertelen!';
$_['error_address_1']           = 'A cím legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_city']                = 'A település neve legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_country']             = 'Kérjük, válasszon egy országot!';
$_['error_zone']                = 'Válasszon ki megyét!';
$_['error_postcode']            = 'Az irányítószámnak 2 és 10 karakter között kell lennie!';
$_['error_agree']               = 'El kell fogadnia a következőt: %s!';