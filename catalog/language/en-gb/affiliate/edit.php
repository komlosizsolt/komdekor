<?php
// Heading
$_['heading_title']     = 'Fiókom';

// Text
$_['text_account']      = 'Fiók';
$_['text_edit']         = 'Fiók információk';
$_['text_your_details'] = 'Az Ön személyes adatai';
$_['text_your_address'] = 'Az Ön címe';
$_['text_success']      = 'Sikeresen módosította fiókja adatait.';

// Entry
$_['entry_firstname']   = 'Keresztnév';
$_['entry_lastname']    = 'Vezetéknév';
$_['entry_email']       = 'E-mail';
$_['entry_telephone']   = 'Telefon';
$_['entry_fax']         = 'Fax';
$_['entry_company']     = 'Cégnév';
$_['entry_website']     = 'Weboldal';
$_['entry_address_1']   = 'Cím';
$_['entry_address_2']   = 'További cím';
$_['entry_postcode']    = 'Irányítószám';
$_['entry_city']        = 'Település';
$_['entry_country']     = 'Ország';
$_['entry_zone']        = 'Megye';

// Error
$_['error_exists']      = 'Ez az e-mail cím már regisztrálva van!';
$_['error_firstname']   = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']    = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']       = 'A megadott e-mail cím nem valós!';
$_['error_telephone']   = 'A telefonszám legalább 1 és legfeljebb 32 karakterből állhat!';
$_['error_address_1']   = 'A cím legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_city']        = 'A település neve legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_country']     = 'Kérjük, válasszon egy országot!';
$_['error_zone']        = 'Válasszon ki megyét!';
$_['error_postcode']    = 'Az irányítószámnak 2 és 10 karakter között kell lennie!';