<?php
// Heading
$_['heading_title']  = 'Kapcsolat';

// Text
$_['text_location']  = 'Címünk';
$_['text_store']     = 'Üzleteink';
$_['text_contact']   = 'Kapcsolati űrlap';
$_['text_address']   = 'Cím';
$_['text_telephone'] = 'Telefon';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Nyitva tartás';
$_['text_comment']   = 'Hozzászólás';
$_['text_success']   = '<p>Üzenetét sikeresen továbbítottuk!</p>';

// Entry
$_['entry_name']     = 'Név';
$_['entry_email']    = 'E-mail cím';
$_['entry_enquiry']  = 'Üzenet:';

// Email
$_['email_subject']  = 'Érdeklődés érkezett a(z) %s áruházból';

// Errors
$_['error_name']     = 'A név legalább 3, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']    = 'A megadott e-mail cím nem érvényes!';
$_['error_enquiry']  = 'Az üzenet legalább 10, legfeljebb 3000 karakter hosszúságú lehet!';
