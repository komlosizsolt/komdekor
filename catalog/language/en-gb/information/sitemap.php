<?php
// Heading
$_['heading_title']    = 'Honlaptérkép';

// Text
$_['text_special']     = 'Akciós termék(ek)';
$_['text_account']     = 'Fiókom';
$_['text_edit']        = 'Fiók információk';
$_['text_password']    = 'Jelszó';
$_['text_address']     = 'Címjegyzékem';
$_['text_history']     = 'Eddigi rendeléseim';
$_['text_download']    = 'Letöltéseim';
$_['text_cart']        = 'Kosár';
$_['text_checkout']    = 'Fizetés';
$_['text_search']      = 'Keresés';
$_['text_information'] = 'Információk';
$_['text_contact']     = 'Kapcsolat';