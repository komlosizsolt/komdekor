<?php
// Heading
$_['heading_title']        = 'Estimate Shipping &amp; Taxes';

// Text
$_['text_success']         = 'Success: Your shipping estimate has been applied!';
$_['text_shipping']        = 'Enter your destination to get a shipping estimate.';
$_['text_shipping_method'] = 'Kérjük válassza ki a kívánt szállítási módot.';

// Entry
$_['entry_country']        = 'Ország';
$_['entry_zone']           = 'Megye';
$_['entry_postcode']       = 'Irányítószám';

// Error
$_['error_postcode']       = 'Az irányítószámnak 2 és 10 karakter között kell lennie!';
$_['error_country']        = 'Kérjük, válasszon egy országot!';
$_['error_zone']           = 'Válasszon ki megyét!';
$_['error_shipping']       = 'Szállítási mód nincs megadva!';
$_['error_no_shipping']    = 'Hiba: nem áll rendelkezésre szállítási mód. Kérjük, lépjen velünk <a href="%s">kapcsolatba</a>';