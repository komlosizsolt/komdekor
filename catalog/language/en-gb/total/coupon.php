<?php
// Heading
$_['heading_title'] = 'Kuponkód';

// Text
$_['text_coupon']   = 'Kupon (%s)';
$_['text_success']  = 'A kupon sikeresen felhasználva!';

// Entry
$_['entry_coupon']  = 'Adja meg a kuponkódot';

// Error
$_['error_coupon']  = 'A kupon érvénytelen!';
$_['error_empty']   = 'Kérjük, írja be a kupon kódját!';
