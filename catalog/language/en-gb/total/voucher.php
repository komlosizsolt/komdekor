<?php
// Heading
$_['heading_title'] = 'Use Gift Voucher';

// Text
$_['text_voucher']  = 'Ajándékutalvány (%s)';
$_['text_success']  = 'Success: Your gift voucher discount has been applied!';

// Entry
$_['entry_voucher'] = 'Adja meg a ajándékutalvány kódját';

// Error
$_['error_voucher'] = 'Ez az ajándékutalvány érvénytelen!';
$_['error_empty']   = 'Kérjük, adja meg a kupon kódját';
