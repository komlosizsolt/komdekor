<?php
// Heading
$_['heading_title']                = 'Belépés';

// Text
$_['text_account']                 = 'Fiók';
$_['text_login']                   = 'Belépés';
$_['text_new_customer']            = 'Új Vásárló';
$_['text_register']                = 'Új vásárlói fiók létrehozása';
$_['text_register_account']        = 'Regisztráció után kényelmesebben és gyorsabban tud vásárolni, naprakész lehet a rendelései állapotával és nyomon követheti azokat, valamint megtekintheti régebbi megrendeléseit.';
$_['text_returning_customer']      = 'Regisztrált vásárló';
$_['text_i_am_returning_customer'] = 'Kérem, lépjen be:';
$_['text_forgotten']               = 'Elfelejtett jelszó';

// Entry
$_['entry_email']                  = 'E-mail cím';
$_['entry_password']               = 'Jelszó';

// Error
$_['error_login']                  = 'Figyelem: érvénytelen e-mail cím és/vagy jelszó.';
$_['error_attempts']               = 'Figyelem: többször kísérelt meg belépni sikertelenül, ezért fiókját korlátoztuk. Kérjük, próbálja újra egy órával később.';
$_['error_approved']               = 'A fiókjába csak jóváhagyás után tud belépni.';