<?php
// Heading
$_['heading_title'] = 'Kívánságlistám';

// Text
$_['text_account']  = 'Fiók';
$_['text_instock']  = 'Készleten';
$_['text_wishlist'] = 'Kívánságlista (%s)';
$_['text_login']    = '<a href="%s">Jelentkezzen be</a> vagy <a href="%s">regisztráljon</a> a(z) <a href="%s">%s</a>, ha a terméket hozzá szeretné adni a <a href="%s">kívánságlistájához</a>!';
$_['text_success']  = 'A(z) <a href="%s">%s</a> terméket sikeresen hozzáadta a <a href="%s">kívánságlistájához</a>!';
$_['text_remove']   = 'Sikeresen módosította a kívánságlistáját!';
$_['text_empty']    = 'A kívánságlistája üres.';

// Column
$_['column_image']  = 'Kép';
$_['column_name']   = 'Terméknév';
$_['column_model']  = 'Cikkszám';
$_['column_stock']  = 'Készlet';
$_['column_price']  = 'Egységár';
$_['column_action'] = 'Művelet';