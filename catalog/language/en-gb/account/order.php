<?php
// Heading
$_['heading_title']         = 'Eddigi rendeléseim';

// Text
$_['text_account']          = 'Fiók';
$_['text_order']            = 'Rendelés adatai';
$_['text_order_detail']     = 'Rendelés részletei';
$_['text_invoice_no']       = 'Számlaszám:';
$_['text_order_id']         = 'Rendelés azonosító:';
$_['text_date_added']       = 'Rendelés dátuma:';
$_['text_shipping_address'] = 'Szállítási cím';
$_['text_shipping_method']  = 'Szállítási mód:';
$_['text_payment_address']  = 'Számlázási cím';
$_['text_payment_method']   = 'Fizetési mód:';
$_['text_comment']          = 'Megjegyzés';
$_['text_history']          = 'Eddigi rendelés(ek)';
$_['text_success']          = 'A(z) <a href="%s">%s</a> terméket sikeresen a <a href="%s">kosarába</a> helyezte!';
$_['text_empty']            = 'Önnek még nem volt megrendelése!';
$_['text_error']            = 'A keresett rendelés nem található!';

// Column
$_['column_order_id']       = 'Rendelési azonosító';
$_['column_customer']       = 'Vásárló';
$_['column_product']        = 'Termékek száma';
$_['column_name']           = 'Terméknév';
$_['column_model']          = 'Cikkszám';
$_['column_quantity']       = 'Mennyiség';
$_['column_price']          = 'Ár';
$_['column_total']          = 'Összesen';
$_['column_action']         = 'Művelet';
$_['column_date_added']     = 'Rendelés dátuma';
$_['column_status']         = 'Rendelés státusza';
$_['column_comment']        = 'Megjegyzés';

// Error
$_['error_reorder']         = '%s jelenleg nem áll rendelkezésre.';