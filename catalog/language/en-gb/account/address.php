<?php
// Heading
$_['heading_title']        = 'Címjegyzékem';

// Text
$_['text_account']         = 'Fiók';
$_['text_address_book']    = 'Címjegyzékem';
$_['text_edit_address']    = 'Cím módosítása';
$_['text_add']             = 'Cím sikeresen hozzáadva a címjegyzékhez!';
$_['text_edit']            = 'A cím sikeresen módosítva!';
$_['text_delete']          = 'A cím sikeresen törölve a címjegyzékből!';
$_['text_empty']           = 'Az Ön fiókjához nem tartoznak címek.';

// Entry
$_['entry_firstname']      = 'Keresztnév';
$_['entry_lastname']       = 'Vezetéknév';
$_['entry_company']        = 'Cégnév';
$_['entry_address_1']      = 'Cím';
$_['entry_address_2']      = 'További cím';
$_['entry_postcode']       = 'Irányítószám';
$_['entry_city']           = 'Település';
$_['entry_country']        = 'Ország';
$_['entry_zone']           = 'Megye';
$_['entry_default']        = 'Alapértelmezett cím:';

// Error
$_['error_delete']                = 'Legalább egy cím beállítása szükséges!';
$_['error_default']               = 'Nem törölheti az alapértelmezett címet!';
$_['error_firstname']             = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']              = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_vat']                   = 'ÁFA kulcs értékes helytelen!';
$_['error_address_1']             = 'A címnek 3 és 128 karakter között kell lennie!';
$_['error_postcode']              = 'Az irányítószámnak 2 és 10 karakter között kell lennie!';
$_['error_city']                  = 'A település neve legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_country']               = 'Kérjük, válasszon egy országot!';
$_['error_zone']                  = 'Válasszon ki megyét!';
$_['error_custom_field']          = '%s kötelező!';
$_['error_custom_field_validate'] = '%s invalid!';
