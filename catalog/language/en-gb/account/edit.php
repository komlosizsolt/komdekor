<?php
// Heading
$_['heading_title']      = 'Belépési adatok';

// Text
$_['text_account']       = 'Fiók';
$_['text_edit']          = 'Belépési adatok módosítása';
$_['text_your_details']  = 'Személyes adatok';
$_['text_success']       = 'Sikeresen módosította fiókja adatait.';

// Entry
$_['entry_firstname']    = 'Keresztnév';
$_['entry_lastname']     = 'Vezetéknév';
$_['entry_email']        = 'E-mail';
$_['entry_telephone']    = 'Telefon';
$_['entry_fax']          = 'Fax';

// Error
$_['error_exists']                = 'Ezzel az e-mail címmel már van regisztrálva fiók!';
$_['error_firstname']             = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']              = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']                 = 'Az e-mail címét helytelenül adtad meg!';
$_['error_telephone']             = 'A telefonszám legalább 3, legfeljebb 32 arakter hosszúságú lehet!';
$_['error_custom_field']          = '%s kötelező!';
$_['error_custom_field_validate'] = '%s invalid!';
