<?php
// Heading
$_['heading_title']           = 'Előfizetéseim';

// Text
$_['text_account']            = 'Fiók';
$_['text_recurring']          = 'Recurring payment';
$_['text_recurring_detail']   = 'Előfizetés beállításai';
$_['text_order_recurring_id'] = 'Recurring ID:';
$_['text_date_added']         = 'Létrehozva: ';
$_['text_status']             = 'Állapot:';
$_['text_payment_method']     = 'Fizetési mód: ';
$_['text_order_id']           = 'Order ID:';
$_['text_product']            = 'Termék: ';
$_['text_quantity']           = 'Mennyiség: ';
$_['text_description']        = 'Description';
$_['text_reference']          = 'Reference';
$_['text_transaction']        = 'Transactions';


$_['text_status_1']           = 'Active';
$_['text_status_2']           = 'Inactive';
$_['text_status_3']           = 'Cancelled';
$_['text_status_4']           = 'Suspended';
$_['text_status_5']           = 'Expired';
$_['text_status_6']           = 'Pending';

$_['text_transaction_date_added'] = 'Létrehozás dátuma';
$_['text_transaction_payment'] = 'Fizetés';
$_['text_transaction_outstanding_payment'] = 'Kiemelkedő befizetés';
$_['text_transaction_skipped'] = 'Kihagyott befizetés';
$_['text_transaction_failed'] = 'A befizetés nem sikerült';
$_['text_transaction_cancelled'] = 'Törölve';
$_['text_transaction_suspended'] = 'Felfüggesztett';
$_['text_transaction_suspended_failed'] = 'Suspended from failed payment';
$_['text_transaction_outstanding_failed'] = 'A befizetés nem sikerült';
$_['text_transaction_expired'] = 'Lejárt';




$_['text_empty']                 = 'Nincsenek előfizetések';
$_['text_error']                 = 'The recurring order you requested could not be found!';








$_['text_cancelled'] = 'Előfizetés törölve';

/*

		$data['status_types'] = array(
			1 => $this->language->get('text_status_inactive'),
			2 => $this->language->get('text_status_active'),
			3 => $this->language->get('text_status_suspended'),
			4 => $this->language->get('text_status_cancelled'),
			5 => $this->language->get('text_status_expired'),
			6 => $this->language->get('text_status_pending'),
		);

		$data['transaction_types'] = array(
			0 => $this->language->get('text_transaction_date_added'),
			1 => $this->language->get('text_transaction_payment'),
			2 => $this->language->get('text_transaction_outstanding_payment'),
			3 => $this->language->get('text_transaction_skipped'),
			4 => $this->language->get('text_transaction_failed'),
			5 => $this->language->get('text_transaction_cancelled'),
			6 => $this->language->get('text_transaction_suspended'),
			7 => $this->language->get('text_transaction_suspended_failed'),
			8 => $this->language->get('text_transaction_outstanding_failed'),
			9 => $this->language->get('text_transaction_expired'),
		);
		
			private $recurring_status = array(
		0 => 'Inactive',
		1 => 'Active',
		2 => 'Suspended',
		3 => 'Cancelled',
		4 => 'Expired / Complete'
	);

	private $transaction_type = array(
		0 => 'Created',
		1 => 'Payment',
		2 => 'Outstanding payment',
		3 => 'Payment skipped',
		4 => 'Payment failed',
		5 => 'Cancelled',
		6 => 'Suspended',
		7 => 'Suspended from failed payment',
		8 => 'Outstanding payment failed',
		9 => 'Expired'
	);

		
		
*/


// Column
$_['column_date_added']         = 'Létrehozás dátuma';
$_['column_type']               = 'Típus';
$_['column_amount']             = 'Összeg';
$_['column_status']             = 'Állapot';
$_['column_product']            = 'Termék';
$_['column_order_recurring_id'] = 'Recurring ID';

// Error
$_['error_not_cancelled'] = 'Hiba: %s';
$_['error_not_found']     = 'Nem sikerült törölni az előfizetést';

// Button
$_['button_return']       = 'Visszaküldés';