<?php
// Heading
$_['heading_title']      = 'Hűségpontok';

// Column
$_['column_date_added']  = 'Hozzáadva';
$_['column_description'] = 'Leírás';
$_['column_points']      = 'Pontok';

// Text
$_['text_account']       = 'Fiók';
$_['text_reward']        = 'Hűségpontjaim';
$_['text_total']         = 'Hűségpontjainak száma:';
$_['text_empty']         = 'Önnek nincs jelenleg hűségpontja!';