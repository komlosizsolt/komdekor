<?php
// Heading
$_['heading_title']    = 'Ajándékutalvány vásárlása';

// Text
$_['text_account']     = 'Fiók';
$_['text_voucher']     = 'Ajándékutalvány';
$_['text_description'] = 'Az ajándékutalványt e-mailben küldjük meg, amint ki lett fizetve.';
$_['text_agree']       = 'Tudomásul veszem, hogy ajándékutalványok ellenértéke nem téríthető vissza, és a le nem vásárolt összeget nem utaljuk vissza.';
$_['text_message']     = '<p>Köszönjük, hogy megvásárolta az ajándékutalványt! Miután befejezte a rendelést, a részleteket elküldjük a címzettnek e-mail-ben, amiben megtudja, hogyan kell beváltani az ajándékutalványát.</p>';
$_['text_for']         = '%s Ajándékutalvány %s részére';

// Entry
$_['entry_to_name']    = 'Kedvezményezett(ek) neve:';
$_['entry_to_email']   = 'Kedvezményezett(ek) e-mail címe:';
$_['entry_from_name']  = 'Név';
$_['entry_from_email'] = 'Az Ön e-mail címe';
$_['entry_theme']      = 'Ajándékutalvány témája';
$_['entry_message']    = 'Üzenet';
$_['entry_amount']     = 'Összeg';

// Help
$_['help_message']     = 'Választható';
$_['help_amount']      = 'Az összegnek %s és %s között kell lennie';

// Error
$_['error_to_name']    = 'A címzett nevének 1 és 64 karakter közé kell esnie!';
$_['error_from_name']  = 'A megadott név hosszának 1 és 64 karakter közé kell esnie!';
$_['error_email']      = 'A megadott e-mail címe érvénytelennek tűnik!';
$_['error_theme']      = 'Kérjük, válasszon témát!';
$_['error_amount']     = 'Az értéknek %s és %s közé kell esnie!';
$_['error_agree']      = 'El kell fogadnia, hogy ajándékutalványok ellenértéke nem téríthető vissza, és a le nem vásárolt összeget nem utaljuk vissza!';
