<?php
// Heading
$_['heading_title']    = 'Hírlevél feliratkozás';

// Text
$_['text_account']     = 'Fiók';
$_['text_newsletter']  = 'Hírlevél beállításaim';
$_['text_success']     = 'Sikeresen módosította a hírlevél feliratkozását!';

// Entry
$_['entry_newsletter'] = 'Feliratkozás';
