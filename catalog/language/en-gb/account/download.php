<?php
// Heading
$_['heading_title']     = 'Letöltések';

// Text
$_['text_account']      = 'Fiók';
$_['text_downloads']    = 'Letöltéseim';
$_['text_empty']        = 'Önnek jelenleg nincs megrendelt letölthető terméke!';

// Column
$_['column_order_id']   = 'Rendelési azonosító';
$_['column_name']       = 'Név';
$_['column_size']       = 'Méret';
$_['column_date_added'] = 'Hozzáadás dátuma';