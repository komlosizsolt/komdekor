<?php
// Heading
$_['heading_title']      = 'Termék visszaküldése';

// Text
$_['text_account']       = 'Fiók';
$_['text_return']        = 'Visszaküldési információk';
$_['text_return_detail'] = 'Visszaküldés oka';
$_['text_description']   = '<p>Kérjük, töltse ki az alábbi űrlapot:</p>';
$_['text_order']         = 'Megrendelés információk';
$_['text_product']       = 'Termék információ és a visszaküldés oka részletesen';
$_['text_reason']        = 'Reason for Return';
$_['text_message']       = '<p>Köszönjük, hogy elküldte visszaküldési igényét, melyet továbbítottunk az illetékes osztálynak feldolgozásra.</p><p>E-mail-ben értesítjük a kérelmének állapotáról.</p>';
$_['text_return_id']     = 'Visszaküldési azonosító:';
$_['text_order_id']      = 'Rendelés azonosító:';
$_['text_date_ordered']  = 'Megrendelés dátuma:';
$_['text_status']        = 'Állapot:';
$_['text_date_added']    = 'Hozzáadva:';
$_['text_comment']       = 'Visszaküldési megjegyzések';
$_['text_history']       = 'Visszaküldés előzmény';
$_['text_empty']         = 'Önnek még nem volt visszaküldési igénye!';
$_['text_agree']         = 'Elfogadom a(z) <a class="colorbox" href="%s" alt="%s"><b>%s</b></a> szabályzatott.';

// Column
$_['column_return_id']   = 'Visszaküldési azonosító';
$_['column_order_id']    = 'Rendelési azonosító';
$_['column_status']      = 'Állapot';
$_['column_date_added']  = 'Hozzáadva';
$_['column_customer']    = 'Vásárló';
$_['column_product']     = 'Terméknév';
$_['column_model']       = 'Cikkszám';
$_['column_quantity']    = 'Mennyiség';
$_['column_price']       = 'Ár';
$_['column_opened']      = 'Nyitott';
$_['column_comment']     = 'Megjegyzés';
$_['column_reason']      = 'Indok';
$_['column_action']      = 'Művelet';

// Entry
$_['entry_order_id']     = 'Rendelési azonosító';
$_['entry_date_ordered'] = 'Rendelés dátuma:';
$_['entry_firstname']    = 'Keresztnév';
$_['entry_lastname']     = 'Vezetéknév';
$_['entry_email']        = 'E-mail';
$_['entry_telephone']    = 'Telefon';
$_['entry_product']      = 'Terméknév:';
$_['entry_model']        = 'Cikkszám:';
$_['entry_quantity']     = 'Mennyiség:';
$_['entry_reason']       = 'Visszaküldés oka:';
$_['entry_opened']       = 'A termék nyitott:';
$_['entry_fault_detail'] = 'Hibaleírás és egyéb adatok:';

// Error
$_['text_error']         = 'A kért visszaküldési igény nem található!';
$_['error_order_id']     = 'A rendelés szám megadása kötelező!';
$_['error_firstname']    = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']     = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']        = 'A megadott e-mail cím nem valós!';
$_['error_telephone']    = 'A telefonszám legalább 3 és legfeljebb 32 karakterből állhat!';
$_['error_product']      = 'Legalább egy terméket ki kell választania!';
$_['error_model']        = 'A cikkszám legalább 3 és legfeljebb 64 karakterből állhat!';
$_['error_reason']       = 'A visszaküldés okát ki kell választani!';
$_['error_agree']        = 'Nem fogadta el a(z) %s szabályzatot!';
