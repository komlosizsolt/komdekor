<?php
// Heading
$_['heading_title']               = 'Új vásárlói fiók létrehozása';

// Text
$_['text_account']                = 'Fiók';
$_['text_register']               = 'Regisztráció';
$_['text_account_already']        = 'Ha már regisztrált tagunk, akkor kérjük, hogy jelentkezzen be a <a href="%s">bejelentkezési</a> oldalon.';
$_['text_your_details']           = 'Személyes adatok';
$_['text_your_address']           = 'Számlázási adatok';
$_['text_newsletter']             = 'Hírlevél beállításaim';
$_['text_your_password']          = 'Jelszó';
$_['text_agree']                  = 'Elfogadom a(z) <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>.';

// Entry
$_['entry_customer_group']        = 'Vásárlói csoport';
$_['entry_firstname']             = 'Keresztnév';
$_['entry_lastname']              = 'Vezetéknév';
$_['entry_email']                 = 'E-mail';
$_['entry_telephone']             = 'Telefon';
$_['entry_fax']                   = 'Fax';
$_['entry_company']               = 'Cégnév';
$_['entry_address_1']             = 'Cím';
$_['entry_address_2']             = 'További cím';
$_['entry_postcode']              = 'Irányítószám';
$_['entry_city']                  = 'Település';
$_['entry_country']               = 'Ország';
$_['entry_zone']                  = 'Megye';
$_['entry_newsletter']            = 'Feliratkozás';
$_['entry_password']              = 'Jelszó';
$_['entry_confirm']               = 'Jelszó mégegyszer';

// Error
$_['error_exists']                = 'Ezzel az e-mail címmel már regisztráltak!';
$_['error_firstname']             = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']              = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']                 = 'A megadott e-mail cím nem helyes!';
$_['error_telephone']             = 'A telefonszám legalább 3 és legfeljebb 32 karakterből állhat!';
$_['error_address_1']             = 'A cím legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_city']                  = 'A település neve legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_postcode']              = 'Az irányítószámnak 2 és 10 karakter között kell lennie!';
$_['error_country']               = 'Kérjük, válasszon egy országot!';
$_['error_zone']                  = 'Válasszon ki megyét!';
$_['error_custom_field']          = '%s kötelező!';
$_['error_custom_field_validate'] = '%s invalid!';
$_['error_password']              = 'A jelszó minimum 4 maximum 20 karakter lehet!';
$_['error_confirm']               = 'A jelszó megerősítése sikertelen!';
$_['error_agree']                 = 'El kell fogadnia a(z) %s!';