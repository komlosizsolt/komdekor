<?php
// Heading
$_['heading_title']      = 'Fiókom';

// Text
$_['text_account']       = 'Fiók';
$_['text_my_account']    = 'Fiók beállítások';
$_['text_my_orders']     = 'Rendeléseim';
$_['text_my_newsletter'] = 'Hírlevél beállításaim';
$_['text_edit']          = 'Belépési adatok módosítása';
$_['text_password']      = 'Jelszó csere';
$_['text_address']       = 'Számlázási adatok módosítása';
$_['text_credit_card']   = 'Manage Stored Credit Cards';
$_['text_wishlist']      = 'Kívánságlistám';
$_['text_order']         = 'Eddigi rendeléseim';
$_['text_download']      = 'Letöltéseim';
$_['text_reward']        = 'Hűségpontok';
$_['text_return']        = 'Visszaküldéseim';
$_['text_transaction']   = 'Tranzakcióim';
$_['text_newsletter']    = 'Feliratkozás/leiratkozás a hírlevélről';
$_['text_recurring']     = 'Előfizetéseim';
$_['text_transactions']  = 'Tranzakcióim';