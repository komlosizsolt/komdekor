<?php
// Heading
$_['heading_title']   = 'Elfelejtette a jelszavát?';

// Text
$_['text_account']    = 'Fiók';
$_['text_forgotten']  = 'Elfelejtett jelszó';
$_['text_your_email'] = 'Az Ön e-mail címe';
$_['text_email']      = 'Adja meg a fiókjához tartozó e-mail címét. Majd kattintson a Tovább gombra és küldjük Önnek e-mailben az új jelszavát.';
$_['text_success']    = 'Az új jelszavát elküldtük az e-mail címére.';

// Entry
$_['entry_email']     = 'E-mail cím';
$_['entry_password']  = 'New Password';
$_['entry_confirm']   = 'Confirm';

// Error
$_['error_email']     = 'A megadott e-mail cím nem szerepel adatbázisunkban! Kérjük, hogy próbálja meg újra!';
$_['error_approved']  = 'Warning: Your account requires approval before you can login.';
$_['error_password']  = 'Password must be between 4 and 20 characters!';
$_['error_confirm']   = 'Password and password confirmation do not match!';
