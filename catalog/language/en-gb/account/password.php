<?php
// Heading
$_['heading_title']  = 'Jelszó megváltoztatása';

// Text
$_['text_account']   = 'Fiók';
$_['text_password']  = 'Jelszó';
$_['text_success']   = 'A jelszavát sikeresen módosította.';

// Entry
$_['entry_password'] = 'Jelszó';
$_['entry_confirm']  = 'Jelszó mégegyszer';

// Error
$_['error_password'] = 'A jelszó minimum 4 maximum 20 karakter lehet!';
$_['error_confirm']  = 'A jelszó megerősítése sikertelen!';