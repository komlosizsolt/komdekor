<?php
// Text
$_['text_paid_amazon'] 			= 'Fizetve az Amazon US-en';
$_['text_total_shipping'] 		= 'Szállítás';
$_['text_total_shipping_tax'] 	= 'Szállítási adó';
$_['text_total_giftwrap'] 		= 'Ajándék csomagolás';
$_['text_total_giftwrap_tax'] 	= 'Ajándék csomagolás adója';
$_['text_total_sub'] 			= 'Részösszeg';
$_['text_tax'] 					= 'Adó';
$_['text_total'] 				= 'Összesen';