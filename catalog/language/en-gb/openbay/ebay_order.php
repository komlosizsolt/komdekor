<?php
// Text
$_['text_total_shipping']		= 'Szállítás';
$_['text_total_discount']		= 'Kedvezmény';
$_['text_total_tax']			= 'Adó';
$_['text_total_sub']			= 'Részösszeg';
$_['text_total']				= 'Összesen';
$_['text_smp_id']				= 'Selling Manager sale ID: ';
$_['text_buyer']				= 'Buyer username: ';