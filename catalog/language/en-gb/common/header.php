<?php
// Text
$_['text_home']          = 'Főoldal';
$_['text_wishlist']      = 'Kívánságlista (%s)';
$_['text_shopping_cart'] = 'Kosár';
$_['text_category']      = 'Kategória';
$_['text_account']       = 'Fiókom';
$_['text_register']      = 'Regisztráció';
$_['text_login']         = 'Belépés';
$_['text_order']         = 'Rendelés előzmény';
$_['text_transaction']   = 'Tranzakcióim';
$_['text_download']      = 'Letöltéseim';
$_['text_logout']        = 'Kilépés';
$_['text_checkout']      = 'Fizetés';
$_['text_search']        = 'Keresés';
$_['text_all']           = 'Összes';
