<?php
// Heading
$_['heading_title']    = 'Karbantartás';

// Text
$_['text_maintenance'] = 'Karbantartás';
$_['text_message']     = '<h1 style="text-align:center;">Jelenleg néhány tervezett karbantartási munkálatot végzünk.<br/>Amint lehet újból elérhetőek leszünk. Kérjük, nézzen be hamarosan újra.</h1>';