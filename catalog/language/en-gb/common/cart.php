<?php
// Text
$_['text_items']     = '%s tétel - %s';
$_['text_empty']     = 'Az Ön kosara üres!';
$_['text_cart']      = 'Kosár megtekintése';
$_['text_checkout']  = 'Fizetés';
$_['text_recurring'] = 'Fizetési profil';