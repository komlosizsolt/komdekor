<?php
// Text
$_['text_information']  = 'Információk';
$_['text_service']      = 'Vevőszolgálat';
$_['text_extra']        = 'Extrák';
$_['text_contact']      = 'Kapcsolat';
$_['text_return']       = 'Termék(ek) visszaküldése';
$_['text_sitemap']      = 'Honlaptérkép';
$_['text_manufacturer'] = 'Gyártók';
$_['text_voucher']      = 'Ajándék utalvány';
$_['text_affiliate']    = 'Partner program';
$_['text_special']      = 'Akciók';
$_['text_account']      = 'Fiókom';
$_['text_order']        = 'Eddigi megrendeléseim';
$_['text_wishlist']     = 'Kívánságlistám';
$_['text_newsletter']   = 'Hírlevél beállításaim';
$_['text_powered']      = 'Készítette: <a href="http://www.opencart.hu">OpenCart Magyarország</a><br /> %s &copy; %s';
