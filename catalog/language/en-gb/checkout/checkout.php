<?php
// Heading
$_['heading_title']                  = 'Fizetés';

// Text
$_['text_cart']                      = 'Kosár';
$_['text_checkout_option']           = 'Első lépés: Vásárlási mód kiválasztása';
$_['text_checkout_account']          = 'Második lépés: Fiók / Számlázási adatok beállítása';
$_['text_checkout_payment_address']  = 'Második lépés: Számlázási adatok beállítása';
$_['text_checkout_shipping_address'] = 'Harmadik lépés: Szállítási adatok beállítása';
$_['text_checkout_shipping_method']  = 'Negyedik lépés: Szállítási mód kiválasztása';
$_['text_checkout_payment_method']   = 'Ötödik lépés: Fizetési mód kiválasztása';
$_['text_checkout_confirm']          = 'Hatodik lépés: Rendelés véglegesítése';
$_['text_modify']                    = 'Módosítás &raquo;';
$_['text_new_customer']              = 'Új Vásárló';
$_['text_returning_customer']        = 'Regisztrált vásárló';
$_['text_checkout']                  = 'Kérem válasszon vásárlási lehetőséget:';
$_['text_i_am_returning_customer']   = 'Kérem, lépjen be:';
$_['text_register']                  = 'Új vásárlói fiók létrehozása';
$_['text_guest']                     = 'Vásárlás vendégként';
$_['text_register_account']          = 'Regisztráció után kényelmesebben és gyorsabban tud vásárolni, naprakész lehet a rendelései állapotával és nyomon követheti azokat, valamint megtekintheti régebbi megrendeléseit.';
$_['text_forgotten']                 = 'Elfelejtett jelszó';
$_['text_your_details']              = 'Az Ön személyes adatai';
$_['text_your_address']              = 'Az Ön címe';
$_['text_your_password']             = 'Jelszó';
$_['text_agree']                     = 'Elolvastam és elfogadom a(z) <a class="fancybox" href="%s" alt="%s"><b>%s</b></a>.';
$_['text_address_new']               = 'Új címet szeretnék megadni';
$_['text_address_existing']          = 'Egy meglévő címet szeretnék használni';
$_['text_shipping_method']           = 'Kérjük válassza ki a kívánt szállítási módot.';
$_['text_payment_method']            = 'Kérjük, hogy válassza ki a fizetési módot ehhez a rendeléshez.';
$_['text_comments']                  = 'Megjegyzés a megrendeléshez';
$_['text_recurring_item']            = 'előfizetés';
$_['text_payment_recurring']         = 'Fizetési profil';
$_['text_trial_description']         = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description']       = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']            = '%s minden %d %s(s) amíg törlik';
$_['text_day']                       = 'nap';
$_['text_week']                      = 'hét';
$_['text_semi_month']                = 'fél hónap';
$_['text_month']                     = 'hónap';
$_['text_year']                      = 'év';

// Column
$_['column_name']                    = 'Termék neve';
$_['column_model']                   = 'Cikkszám';
$_['column_quantity']                = 'Mennyiség';
$_['column_price']                   = 'Egységár';
$_['column_total']                   = 'Összesen';

// Entry
$_['entry_email_address']            = 'E-mail cím';
$_['entry_email']                    = 'E-mail';
$_['entry_password']                 = 'Jelszó';
$_['entry_confirm']                  = 'Jelszó mégegyszer';
$_['entry_firstname']                = 'Keresztnév';
$_['entry_lastname']                 = 'Vezetéknév';
$_['entry_telephone']                = 'Telefon';
$_['entry_fax']                      = 'Fax';
$_['entry_address']                  = 'Válasszon címet';
$_['entry_company']                  = 'Cégnév';
$_['entry_customer_group']           = 'Vásárlói csoport';
$_['entry_address_1']                = 'Cím';
$_['entry_address_2']                = 'További cím';
$_['entry_postcode']                 = 'Irányítószám';
$_['entry_city']                     = 'Település';
$_['entry_country']                  = 'Ország';
$_['entry_zone']                     = 'Megye';
$_['entry_newsletter']               = 'Szeretnék feliratkozni a(z) %s hírlevélre.';
$_['entry_shipping'] 	             = 'A szállítási és számlázási címem azonos.';

// Error
$_['error_warning']                  = 'Hiba történt a rendelés feldolgozása közben! Kérjük, próbálja meg újra vagy válasszon másik fizetési módot. Ha a probléma továbbra is fennáll, kérjük, lépjen <a href="%s">kapcsolatba</a> velünk.';
$_['error_login']                    = 'Figyelem: érvénytelen e-mail cím és/vagy jelszó.';
$_['error_attempts']                 = 'Figyelem: többször kísérelt meg belépni sikertelenül, ezért fiókját korlátoztuk. Kérjük, próbálja újra egy órával később.';
$_['error_approved']                 = 'A fiókjával belépni csak a jóváhagyás után tud.';
$_['error_exists']                   = 'A megadott e-mail cím már regisztrálva van!';
$_['error_firstname']                = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']                 = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']                    = 'A megadott e-mail cím nem megfelelő!';
$_['error_telephone']                = 'A telefonszám legalább 3 és legfeljebb 32 karakterből állhat!';
$_['error_password']                 = 'A jelszó legalább 3 és legfeljebb 20 karakterből állhat!';
$_['error_confirm']                  = 'A jelszó megerősítése sikertelen!';
$_['error_address_1']                = 'A cím legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_city']                     = 'A település neve legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_postcode']                 = 'Az irányítószámnak 2 és 10 karakter között kell lennie!';
$_['error_country']                  = 'Kérjük, válasszon egy országot!';
$_['error_zone']                     = 'Válasszon ki megyét!';
$_['error_agree']                    = 'El kell fogadnia a(z) %s!';
$_['error_address']                  = 'Ki kell választania a címet!';
$_['error_shipping']                 = 'Szállítási mód nincs megadva!';
$_['error_no_shipping']              = 'Hiba: nem áll rendelkezésre szállítási mód. Kérjük, lépjen velünk <a href="%s">kapcsolatba</a>';
$_['error_payment']                  = 'Kérjük, adja meg a fizetési módot!';
$_['error_no_payment']               = 'Jelenleg nincsenek választható fizetési módok. Kérjük, lépjen velünk <a href="%s">kapcsolatba</a> a hiba megoldásért!';
$_['error_custom_field']             = '%s kötelező!';
$_['error_custom_field_validate']    = '%s invalid!';