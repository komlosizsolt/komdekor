<?php
// Heading
$_['heading_title']            = 'Kosár tartalma';

// Text
$_['text_success']             = 'A(z) <a href="%s">%s</a> terméket sikeresen a <a href="%s">kosarába</a> helyezte!';
$_['text_remove']              = 'Sikeresen módosította a kosara tartalmát!';
$_['text_login']               = 'Az árak megtekintéséhez kérem, <a href="%s">lépjen be</a> vagy <a href="%s">regisztráljon</a>!';
$_['text_items']               = '%s termék(ek) - %s';
$_['text_points']              = 'Hűségpontok: %s';
$_['text_next']                = 'Mi legyen a következő lépés?';
$_['text_next_choice']         = 'Amennyiben rendelkezik kuponkóddal vagy ajándékutalvánnyal, esetleg hűségpontjait szeretné beváltani, azt itt megteheti. Ezen kívül megbecsülheti szállítási költségét is.';
$_['text_empty']               = 'Az Ön kosara üres!';
$_['text_day']                 = 'nap';
$_['text_week']                = 'hét';
$_['text_semi_month']          = 'fél hónap';
$_['text_month']               = 'hónap';
$_['text_year']                = 'év';
$_['text_trial']               = '%s every %s %s for %s payments then ';
$_['text_recurring']           = '%s minden %s %s';
$_['text_length']              = ' for %s payments';
$_['text_until_cancelled']     = 'visszavonásig';
$_['text_recurring_item']      = 'előfizetés';
$_['text_payment_recurring']   = 'Fizetési profil';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s minden %d %s(s) amíg törlik';

// Column
$_['column_image']             = 'Kép';
$_['column_name']              = 'Termék neve';
$_['column_model']             = 'Cikkszám';
$_['column_quantity']          = 'Mennyiség';
$_['column_price']             = 'Egységár';
$_['column_total']             = 'Összesen';

// Error
$_['error_stock']              = 'A *** -al jelölt termék(ek) nem elérhető(ek) a kívánt mennyiségben!';
$_['error_minimum']            = 'A %s minimum rendelési mennyisége %s!';
$_['error_required']           = '%s kötelező!';
$_['error_product']            = 'Nem található termék a kosárban!';
$_['error_recurring_required'] = 'Kérjük, válasszon egy előfizetést!';