<?php
// Heading
$_['heading_title'] = 'Sikertelen fizetés!';

// Text
$_['text_basket']   = 'Kosár';
$_['text_checkout'] = 'Fizetés';
$_['text_failure']  = 'Sikertelen fizetés';
$_['text_message']  = '<p>Problémába ütköztünk a fizetés során, így a megrendelését nem tudjuk teljesíteni.</p>

<p>Ennek okai lehetnek:</p>
<ul>
  <li>Fedezethiány</li>
  <li>Sikertelen ellenőrzés</li>
</ul>

<p>Kérjük, próbálja meg újra leadni rendelését és válasszon másik fizetési módot.</p>

<p>Ha a probléma továbbra is fennáll, kérjük vegye fel velünk a <a href="%s">kapcsolatot</a>.</p>
';
