<?php
// Heading
$_['heading_title']        = 'Sikeres megrendelés!';

// Text
$_['text_basket']          = 'Kosár';
$_['text_checkout']        = 'Fizetés';
$_['text_success']         = 'Sikeres megrendelés';
$_['text_customer']        = '<p>Köszönjük, hogy nálunk vásárolt!</p><p>A megrendelésének állását bármikor megtekíntheti a <a href="%s">fiókjában</a>, az <a href="%s">eddigi rendeléseim</a> menüpontra kattintva.</p>Ha a megrendelése letölthető fájlt tartalmaz, akkor a fiókjában kattintson a <a href="%s">letöltések</a> menüpontra.</p><p>Ha bármilyen kérdése lenne írjon a következő <a href="%s">címre</a>.</p><p>Köszönjük a vásárlást!</p>';
$_['text_guest']           = '<p>Köszönjük, hogy nálunk vásárolt!</p><p>Ha bármilyen kérdése lenne, lépjen velünk <a href="%s">kapcsolatba</a>.</p><p>Köszönjük a vásárlást!</p>';