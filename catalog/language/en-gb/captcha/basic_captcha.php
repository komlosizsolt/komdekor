<?php
// Text
$_['text_captcha'] = 'Captcha';

// Entry
$_['entry_captcha'] = 'Írja be a kódot az alábbi mezőbe';

// Error
$_['error_captcha'] = 'Az ellenőrzőkód nem megfelelő!';
