<?php
// Text
$_['text_subject']  = '%s - Új jelszó igénylése';
$_['text_greeting'] = 'Új jelszó lett igényelve %s részére.';
$_['text_change']   = 'Hogy új jelszót állíts be, kattints rá a linkre:';
$_['text_ip']       = 'Új jelszó igénylése erről az IP címről tötént: %s';