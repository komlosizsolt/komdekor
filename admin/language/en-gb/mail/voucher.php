<?php
// Text
$_['text_subject']  = 'Ajándék kupont kaptál - %s';
$_['text_greeting'] = 'Gratulálok, Most kaptál egy ajándék kupont melynek értéke %s';
$_['text_from']     = 'Egy ajándék kupont küldött neked %s';
$_['text_message']  = 'Melyhez a következő üzenet társul:';
$_['text_redeem']   = 'Ahoz hogy beváltsd a kupont, írd le a beváltáshoz szükséges kódot ami <b>%s</b> majd kattints a linkre alúl hogy a vásárláshoz juss. A kosárnál még a fizetés előtt tudod megadni a kódot.';
$_['text_footer']   = 'Ha kérdésed van, tedd fel bátran.';
