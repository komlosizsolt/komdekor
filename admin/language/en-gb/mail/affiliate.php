<?php
// Text
$_['text_approve_subject']      = '%s - A partner fiókod aktiválva lett!';
$_['text_approve_welcome']      = 'Üdvözöllek és köszönöm hogy regisztráltál %s!';
$_['text_approve_login']        = 'A felhasználód elkészült és be is tudsz jelentkezni az email címedet és jelszavadat használva ezen az URL-en:';
$_['text_approve_services']     = 'Amint bejelentkeztél, képes leszel követő kódot generálni, követni a fizetéseket és szerkeszteni a fiók adataidat.';
$_['text_approve_thanks']       = 'Köszi,';
$_['text_transaction_subject']  = '%s - Affiliate jutalék';
$_['text_transaction_received'] = '%s jutalék jóváírva!';
$_['text_transaction_total']    = 'Eddig összegyüjtött jutalékod: %s.';