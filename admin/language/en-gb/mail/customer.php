<?php
// Text
$_['text_approve_subject']      = '%s - A fiókja aktív!';
$_['text_approve_welcome']      = 'Üdvözöljük, és köszönjük regisztrációját a következő áruházban: %s!';
$_['text_approve_login']        = 'Fiókja elkészült. Most már bejelentkezhet e-mail címével és jelszavával a következő helyen:';
$_['text_approve_services']     = 'Bejelentkezés után hozzáférhet előző rendeléseihez és szerkesztheti adatait.';
$_['text_approve_thanks']       = 'Köszönöm,';
$_['text_transaction_subject']  = '%s - Fiók kreditek';
$_['text_transaction_received'] = '%s kreditet kaptál!';
$_['text_transaction_total']    = 'Jelenlegi kreditjeid száma %s.

A kreditek automatikusan beszámításra kerülnek a következő vásárlásnál.';
$_['text_reward_subject']       = '%s - Hűségpont';
$_['text_reward_received']      = '%s hűségpontot kaptál!';
$_['text_reward_total']         = 'Jelenlegi hűségpontjaid száma %s.';