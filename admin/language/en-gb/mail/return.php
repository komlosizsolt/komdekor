<?php
// Text
$_['text_subject']       = '%s - Visszáru frissités %s';
$_['text_return_id']     = 'Visszaküldési azonosító:';
$_['text_date_added']    = 'Visszáru dátum:';
$_['text_return_status'] = 'A visszáru státusza a következőre változott:';
$_['text_comment']       = 'Visszáru komment:';
$_['text_footer']        = 'Ha kérdésed van, tedd fel bátran.';