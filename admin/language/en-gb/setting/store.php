<?php
// Heading
$_['heading_title']                = 'Webáruház';

// Text
$_['text_settings']                = 'Beállítások';
$_['text_success']                 = 'A beállítások mentése sikeresen megtörtént!';
$_['text_list']                    = 'Store List';
$_['text_add']                     = 'Add Store';
$_['text_edit']                    = 'Edit Store';
$_['text_items']                   = 'Items';
$_['text_tax']                     = 'Taxes';
$_['text_account']                 = 'Fiók';
$_['text_checkout']                = 'Fizetés';
$_['text_stock']                   = 'Stock';
$_['text_shipping']                = 'Shipping Address';
$_['text_payment']                 = 'Payment Address';

// Column
$_['column_name']                  = 'Áruház név';
$_['column_url']	               = 'Áruház URL';
$_['column_action']                = 'Művelet';

// Entry
$_['entry_url']                    = 'Áruház URL';
$_['entry_ssl']                    = 'SSL URL';
$_['entry_meta_title']             = 'Meta Title';
$_['entry_meta_description']       = 'Meta leírás:';
$_['entry_meta_keyword']           = 'Meta kulcsszavak';
$_['entry_layout']                 = 'Alapértelmezett felület';
$_['entry_theme']                  = 'Theme';
$_['entry_name']                   = 'Áruház neve:';
$_['entry_owner']                  = 'Áruház üzemeltető:';
$_['entry_address']                = 'Cím';
$_['entry_geocode']                = 'Geocode';
$_['entry_email']                  = 'E-mail';
$_['entry_telephone']              = 'Telefon';
$_['entry_fax']                    = 'Fax';
$_['entry_image']                  = 'Kép';
$_['entry_open']                   = 'Nyitva tartás';
$_['entry_comment']                = 'Megjegyzés';
$_['entry_location']               = 'Üzletek';
$_['entry_country']                = 'Ország';
$_['entry_zone']                   = 'Megye';
$_['entry_language']               = 'Nyelv';
$_['entry_currency']               = 'Pénznem';
$_['entry_tax']                    = 'Ár megjelenítése adóval:';
$_['entry_tax_default']            = 'Use Store Tax Address';
$_['entry_tax_customer']           = 'Use Customer Tax Address';
$_['entry_customer_group']         = 'Vásárlói csoport';
$_['entry_customer_group_display'] = 'Vásárlói csoportok';
$_['entry_customer_price']         = 'Bejelentkezés esetén mutatott ár';
$_['entry_account']                = 'Általános szerződési feltételek';
$_['entry_cart_weight']            = 'Mutassa a súlyt a kosár lapon';
$_['entry_checkout_guest']         = 'Vásárlás vendégként';
$_['entry_checkout']               = 'Vásárlási, fizetési feltételek';
$_['entry_order_status']           = 'Rendelés státusza';
$_['entry_stock_display']          = 'Készlet megjelenítése';
$_['entry_stock_checkout']         = 'Készlet kijelentkezés';
$_['entry_logo']                   = 'Az üzlet logója:';
$_['entry_icon']                   = 'Ikon';
$_['entry_secure']                 = 'SSL használat';

// Help
$_['help_url']                     = 'Include the full URL to your store. Make sure to add \'/\' at the end. Example: http://www.yourdomain.com/path/<br /><br />Don\'t use directories to create a new store. You should always point another domain or sub domain to your hosting.';
$_['help_ssl']                     = 'SSL URL to your store. Make sure to add \'/\' at the end. Example: http://www.yourdomain.com/path/<br /><br />Don\'t use directories to create a new store. You should always point another domain or sub domain to your hosting.';
$_['help_geocode']                 = 'Please enter your store location geocode manually.';
$_['help_open']                    = 'Áruház nyitvatartási ideje';
$_['help_comment']                 = 'This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques.';
$_['help_location']                = 'The different store locations you have that you want displayed on the contact us form.';
$_['help_currency']                = 'Megváltoztathatja az alapértelmezett pénznemet. Törölje a böngésző cache mappáját ha látni szeretné változást, és állítsa vissza a meglévő sütit.';
$_['help_tax_default']             = 'Use the store address to calculate taxes if no one is logged in. You can choose to use the store address for the customers shipping or payment address.';
$_['help_tax_customer']            = 'Use the customers default address when they login to calculate taxes. You can choose to use the default address for the customers shipping or payment address.';
$_['help_customer_group']          = 'Default customer group.';
$_['help_customer_group_display']  = 'Display customer groups that new customers can select to use such as wholesale and business when signing up.';
$_['help_customer_price']          = 'Only show prices when a customer is logged in.';
$_['help_account']                 = 'Forces people to agree to terms before an account can be created.';
$_['help_checkout_guest']          = 'Allow customers to checkout without creating an account. This will not be available when a downloadable product is in the shopping cart.';
$_['help_checkout']                = 'Forces people to agree to terms before an a customer can checkout.';
$_['help_order_status']            = 'Set the default order status when an order is processed.';
$_['help_stock_display']           = 'Display stock quantity on the product page.';
$_['help_stock_checkout']          = 'Allow customers to still checkout if the products they are ordering are not in stock.';
$_['help_icon']                    = 'Az ikon PNG file legyen, mérete: 16px x 16px.';
$_['help_secure']                  = 'To use SSL check with your host if a SSL certificate is installed.';

// Error
$_['error_warning']                = 'Kérlek nézd át az űrlapot hibákért!';
$_['error_permission']             = 'Figyelmeztetés: A beállítások módosítása az Ön számára nem engedélyezett!';
$_['error_url']                    = 'Áruház URL kötelező!';
$_['error_meta_title']             = 'Title must be between 3 and 32 characters!';
$_['error_name']                   = 'Az üzlet neve legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_owner']                  = 'Üzlet tulajdonos legalább 3 és legfeljebb 64 karakter lehet!';
$_['error_address']                = 'Üzlet címe legalább 10 és legfeljebb 256 karakter lehet!';
$_['error_email']                  = 'Figyelmeztetés: Nem tűnik érvényesnek az e-mail cím!';
$_['error_telephone']              = 'Telefon legalább 3 és legfeljebb 32 karakter lehet!';
$_['error_customer_group_display'] = 'You must include the default customer group if you are going to use this feature!';
$_['error_default']                = 'Figyelmeztetés: Nem törölheted le az alapértelmezett üzleted!';
$_['error_store']                  = 'Figyelem! Ez az áruház jelenleg nem törölhető, mert ez a kiválasztott áruház a %s rendeléshez!';
