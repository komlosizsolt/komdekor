<?php
// header
$_['heading_title']  = 'Belépés az adminisztrációs felületre';

// Text
$_['text_heading']   = 'Belépés';
$_['text_login']     = 'Kérjük, adja meg a bejelentkezési adatait';
$_['text_forgotten'] = 'Elfeledett jelszó';

// Entry
$_['entry_username'] = 'Felhasználónév';
$_['entry_password'] = 'Jelszó';

// Button
$_['button_login']   = 'Belépés';

// Error
$_['error_login']    = 'Nincs egyező felhasználónév és/vagy jelszó.';
$_['error_token']    = 'Érvénytelen. Jelentkezzen be újra.';