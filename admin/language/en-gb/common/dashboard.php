<?php
// Heading
$_['heading_title']                = 'Műszerfal';

// Text
$_['text_order_total']             = 'Összes megrendelés';
$_['text_customer_total']          = 'Összes vásárló';
$_['text_sale_total']              = 'Összes értékesítés';
$_['text_online_total']            = 'Online felhasználók';
$_['text_map']                     = 'Térkép nézet';
$_['text_sale']                    = 'Értékesítési statisztika';
$_['text_activity']                = 'Utóbbi tevékenységek';
$_['text_recent']                  = 'Utolsó rendelések';
$_['text_order']                   = 'Rendelések';
$_['text_customer']                = 'Vásárlók';
$_['text_day']                     = 'Ma';
$_['text_week']                    = 'hét';
$_['text_month']                   = 'hónap';
$_['text_year']                    = 'év';
$_['text_view']                    = 'Részletek...';

// Error
$_['error_install']                = 'Figyelmeztetés: az install mappa még létezik. Biztonsági okokból ezt távolítsa el!';