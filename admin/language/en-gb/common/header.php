<?php
// Heading
$_['heading_title']          = 'Adminisztráció';

// Text
$_['text_order']             = 'Rendelések';
$_['text_processing_status'] = 'Processing';
$_['text_complete_status']   = 'Completed';
$_['text_customer']          = 'Vásárlók';
$_['text_online']            = 'Customers Online';
$_['text_approval']          = 'Pending approval';
$_['text_product']           = 'Termékek';
$_['text_stock']             = 'Nincs raktáron';
$_['text_review']            = 'Vélemény';
$_['text_return']            = 'Termék(ek) visszaküldése';
$_['text_affiliate']         = 'Partner program';
$_['text_store']             = 'Webáruház';
$_['text_front']             = 'Előnézet';
$_['text_help']              = 'Súgó';
$_['text_homepage']          = 'Homepage';
$_['text_support']           = 'Támogató fórum';
$_['text_documentation']     = 'Dokumentáció';
$_['text_logout']            = 'Kijelentkezés';