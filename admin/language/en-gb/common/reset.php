<?php
// header
$_['heading_title']  = 'Új jelszó kérése';

// Text
$_['text_password']  = 'Adj meg egy jelszót.';
$_['text_success']   = 'A jelszavát sikeresen módosította.';

// Entry
$_['entry_password'] = 'Jelszó';
$_['entry_confirm']  = 'Megerősít';

// Error
$_['error_password'] = 'A jelszó legalább 5 és legfeljebb 20 karakter lehet!';
$_['error_confirm']  = 'A két jelszó nem egyezik!';