<?php
// Heading
$_['heading_title']    = 'Kép kezelő';

// Text
$_['text_uploaded']    = 'A fájlt sikeresen feltöltötte.';
$_['text_directory']   = 'Success: Directory created!';
$_['text_delete']      = 'A fájl vagy könyvtár törölve lett!';

// Entry
$_['entry_search']     = 'Search..';
$_['entry_folder']     = 'Új mappa:';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az engedély megtagadva!';
$_['error_filename']   = 'Figyelmeztetés: A filenév 3 és 255 karakter hosszúságú lehet!';
$_['error_folder']     = 'Warning: Folder name must be a between 3 and 255!';
$_['error_exists']     = 'Figyelmeztetés: Ezen a néven már létezik file vagy könyvtár!';
$_['error_directory']  = 'Kérem jelöljön ki egy könyvtárat!';
$_['error_filetype']   = 'Warning: Incorrect file type!';
$_['error_upload']     = 'Warning: File could not be uploaded for an unknown reason!';
$_['error_delete']     = 'Figyelmeztetés: Nem törölheti ezt a könyvtárat!';
