<?php
// Heading
$_['heading_title']          = 'Google Base';

// Text
$_['text_feed']              = 'Hírcsatornák';
$_['text_success']           = 'A Google Base módosítása sikersen megtörtént!';
$_['text_edit']              = 'Edit Google Base';
$_['text_import']            = 'To download the latest Google category list by <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">clicking here</a> and choose taxonomy with numeric IDs in Plain Text (.txt) file. Upload via the green import button.';

// Column
$_['column_google_category'] = 'Google Category';
$_['column_category']        = 'Category';
$_['column_action']          = 'Action';

// Entry
$_['entry_google_category'] = 'Google Category';
$_['entry_category']        = 'Category';
$_['entry_data_feed']       = 'Csatorna URL cím:';
$_['entry_status']          = 'Állapot';

// Error
$_['error_permission']      = 'Nincs jogosultsága a Google Base módosításához!';
$_['error_upload']          = 'File could not be uploaded!';
$_['error_filetype']        = 'Invalid file type!';
