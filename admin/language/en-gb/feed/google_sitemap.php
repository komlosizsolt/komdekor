<?php
// Heading
$_['heading_title']    = 'Google Sitemap';

// Text
$_['text_feed']        = 'Hírcsatornák';
$_['text_success']     = 'A Google Sitemap módosítása sikeresen megtörtént!';
$_['text_edit']        = 'Edit Google Sitemap';

// Entry
$_['entry_status']     = 'Állapot';
$_['entry_data_feed']  = 'Csatorna URL cím:';

// Error
$_['error_permission'] = 'Nincs jogosultsága a Google Sitemap módosításához!';