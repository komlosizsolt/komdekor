<?php
// Heading
$_['heading_title']          = 'Tulajdonságok';

// Text
$_['text_success']           = 'A jellemzők módosítása sikeresen megtörtént!';
$_['text_list']              = 'Tulajdonságok listája';
$_['text_add']               = 'Add Attribute';
$_['text_edit']              = 'Edit Attribute';

// Column
$_['column_name']            = 'Jellemző neve';
$_['column_attribute_group'] = 'Jellemző csoport';
$_['column_sort_order']      = 'Sorrend';
$_['column_action']          = 'Művelet';

// Entry
$_['entry_name']            = 'Jellemző neve:';
$_['entry_attribute_group'] = 'Jellemző csoport:';
$_['entry_sort_order']      = 'Sorrend';

// Error
$_['error_permission']      = 'Nincs jogosultsága a jellemzők módosításához!';
$_['error_attribute_group'] = 'Attribute Group Required!';
$_['error_name']            = 'Jellemző neve legalább 3 és legfeljebb 64 karakter lehet!';
$_['error_product']         = 'Ezt a jellemzőt nem lehet törölni, mivel a(z) %s termékhez van rendelve!';
