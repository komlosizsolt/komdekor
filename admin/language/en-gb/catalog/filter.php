<?php
// Heading
$_['heading_title']     = 'Szűrők';

// Text
$_['text_success']      = 'Success: You have modified filters!';
$_['text_list']         = 'Szűrő listák';
$_['text_add']          = 'Add Filter';
$_['text_edit']         = 'Edit Filter';

// Column
$_['column_group']      = 'Szűrő csoport';
$_['column_sort_order'] = 'Sorrend';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_group']       = 'Szűrő csoport név:';
$_['entry_name']        = 'Szűrő név:';
$_['entry_sort_order']  = 'Sorrend';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify filters!';
$_['error_group']       = 'Filter Group Name must be between 1 and 64 characters!';
$_['error_name']        = 'Filter Name must be between 1 and 64 characters!';