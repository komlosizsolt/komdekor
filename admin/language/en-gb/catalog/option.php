<?php
// Heading
$_['heading_title']       = 'Opciók';

// Text
$_['text_success']        = 'Az opció(k) módosítása sikeresen megtörtént!';
$_['text_list']           = 'Option List';
$_['text_add']            = 'Add Option';
$_['text_edit']           = 'Edit Option';
$_['text_choose']         = 'Választó elemek';
$_['text_select']         = 'Lista';
$_['text_radio']          = 'Radio';
$_['text_checkbox']       = 'Checkbox';
$_['text_image']          = 'Kép';
$_['text_input']          = 'Bevitel elemek';
$_['text_text']           = 'Szöveges mező';
$_['text_textarea']       = 'Megjegyzés';
$_['text_file']           = 'Fájl';
$_['text_date']           = 'Dátum';
$_['text_datetime']       = 'Dátum és idő';
$_['text_time']           = 'Idő';

// Column
$_['column_name']         = 'Opció neve';
$_['column_sort_order']   = 'Sorrend';
$_['column_action']       = 'Művelet';

// Entry
$_['entry_name']          = 'Opció neve:';
$_['entry_type']          = 'Típus';
$_['entry_option_value']  = 'Opció értékének neve:';
$_['entry_image']         = 'Kép';
$_['entry_sort_order']    = 'Sorrend';

// Error
$_['error_permission']    = 'Nincs jogosultsága az opciók módosításához!';
$_['error_name']          = 'Opció neve legalább 1 és legfeljebb 128 karakter lehet!';
$_['error_type']          = 'Opció értéket szükséges megadni!';
$_['error_option_value']  = 'Opció értéke legalább 1 és legfeljebb 128 karakter lehet!';
$_['error_product']       = 'Ezt az opciót nem lehet törölni, mivel a(z) %s termékhez van rendelve!';