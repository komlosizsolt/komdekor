<?php
// Heading
$_['heading_title']      = 'Gyártók';

// Text
$_['text_success']       = 'A gyártó módosítása sikeresen megtörtént!';
$_['text_list']          = 'Gyártók listája';
$_['text_add']           = 'Add Manufacturer';
$_['text_edit']          = 'Edit Manufacturer';
$_['text_default']       = 'Alapértelmezett';
$_['text_percent']       = 'Százalék';
$_['text_amount']        = 'Fix összeg';

// Column
$_['column_name']        = 'Gyártó név';
$_['column_sort_order']  = 'Sorrend';
$_['column_action']      = 'Művelet';

// Entry
$_['entry_name']         = 'Gyártó név:';
$_['entry_store']        = 'Webáruház';
$_['entry_keyword']      = 'SEO link';
$_['entry_image']        = 'Kép';
$_['entry_sort_order']   = 'Sorrend';
$_['entry_type']         = 'Típus';

// Help
$_['help_keyword']       = 'Ne használjon szóközt a szavak között, inkább kötőjelet vagy aláhúzást és győződjön meg róla, hogy ez a link még nem létezik ezen az oldalon.';

// Error
$_['error_permission']   = 'Nincs jogosultsága a gyártók módosításához!';
$_['error_name']         = 'A gyártó neve legalább 3 és legfeljebb 64 karakterből álljon!';
$_['error_keyword']      = 'SEO keyword already in use!';
$_['error_product']      = 'Ezt a gyártót nem törölheti, mert jelenleg %s termékhez van hozzárendelve!';
