<?php
// Heading
$_['heading_title']     = 'Tulajdonság csoportok';

// Text
$_['text_success']      = 'A jellemző csoportok módosítása sikeresen megtörtént!';
$_['text_list']         = 'Tulajdonság csoportok listája';
$_['text_add']          = 'Add Attribute Group';
$_['text_edit']         = 'Edit Attribute Group';

// Column
$_['column_name']       = 'Jellemző csoport név';
$_['column_sort_order'] = 'Sorrend';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']        = 'Jellemző csoport név:';
$_['entry_sort_order']  = 'Sorrend';

// Error
$_['error_permission']  = 'Nincs jogosultsága a jellemző csoportok módosításához!';
$_['error_name']        = 'Jellemző csoport neve legalább 3 és legfeljebb 64 karakter lehet!';
$_['error_attribute']   = 'Ezt a jellemző csoportot nem lehet törölni, mivel a(z) %s jellemzőhöz van rendelve!';
$_['error_product']     = 'Ezt a jellemző csoportot nem lehet törölni, mivel a(z) %s termékhez van rendelve!';