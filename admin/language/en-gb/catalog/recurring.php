<?php
// Heading
$_['heading_title']			= 'Ismétlődő profilok';

// Text
$_['text_success']          = 'Success: You have modified recurring profiles!';
$_['text_list']             = 'Ismétlődő porfilok listája';
$_['text_add']              = 'Új ismétlődő profil';
$_['text_edit']             = 'Ismétlődő porfil szerkesztése';
$_['text_day']				= 'Leírás';
$_['text_week']				= 'hét';
$_['text_semi_month']		= 'Semi Month';
$_['text_month']			= 'hónap';
$_['text_year']				= 'év';
$_['text_recurring']	    = '<p><i class="fa fa-info-circle"></i> Recurring amounts are calculated by the frequency and cycles.</p><p>For example if you use a frequency of "week" and a cycle of "2", then the user will be billed every 2 weeks.</p><p>The duration is the number of times the user will make a payment, set this to 0 if you want payments until they are cancelled.</p>';
$_['text_profile']			= ' Ismétlődő profil';
$_['text_trial']			= 'Trial Profile';

// Entry
$_['entry_name']			= 'Név';
$_['entry_price']			= 'Ár';
$_['entry_duration']		= 'Duration';
$_['entry_cycle']			= 'Cycle';
$_['entry_frequency']		= 'Frequency';
$_['entry_trial_price']		= 'Trial price';
$_['entry_trial_duration']	= 'Trial duration';
$_['entry_trial_status']	= 'Trial status';
$_['entry_trial_cycle']	    = 'Trial cycle';
$_['entry_trial_frequency']	= 'Trial frequency';
$_['entry_status']			= 'Állapot';
$_['entry_sort_order']		= 'Sorrend';

// Column
$_['column_name']			= 'Név';
$_['column_sort_order']	    = 'Sorrend';
$_['column_action']         = 'Művelet';

// Error
$_['error_warning']         = 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Warning: You do not have permission to modify recurring profiles!';
$_['error_name']			= 'Profile Name must be greater than 3 and less than 255 characters!';
$_['error_product']			= 'Warning: This recurring profile cannot be deleted as it is currently assigned to %s products!';