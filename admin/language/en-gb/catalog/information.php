<?php
// Heading
$_['heading_title']          = 'Egyéb oldalak';

// Text
$_['text_success']           = 'Az oldal frissítése sikeresen megtörtént!';
$_['text_list']              = 'Information List';
$_['text_add']               = 'Add Information';
$_['text_edit']              = 'Edit Information';
$_['text_default']           = 'Alapértelmezett';

// Column
$_['column_title']           = 'Oldal neve';
$_['column_sort_order']	     = 'Sorrend';
$_['column_action']          = 'Művelet';

// Entry
$_['entry_title']            = 'Oldal neve:';
$_['entry_description']      = 'Leírás:';
$_['entry_store']            = 'Webáruház';
$_['entry_meta_title'] 	     = 'Meta Title tag (Cím)';
$_['entry_meta_keyword'] 	   = 'Meta kulcsszavak';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO link';
$_['entry_bottom']           = 'Lábléc';
$_['entry_status']           = 'Állapot';
$_['entry_sort_order']       = 'Sorrend';
$_['entry_layout']           = 'Felület beállítás:';

// Help
$_['help_keyword']           = 'Ne használjon szóközt a szavak között, inkább kötőjelet vagy aláhúzást és győződjön meg róla, hogy ez a link még nem létezik ezen az oldalon.';
$_['help_bottom']            = 'Display in the bottom footer.';

// Error
$_['error_warning']          = 'Figyelmeztetés: Kérlek ellenőrizd figyelmesen az űrlapot hibákért!';
$_['error_permission']       = 'Figyelmeztetés: Az információk módosítása az Ön számára nem engedélyezett!';
$_['error_title']            = 'Az információ címe legalább 3, és legfeljebb 64 karakterből álljon!';
$_['error_description']      = 'A leírás legalább 3 karakterből álljon!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';
$_['error_account']          = 'Figyelmeztetés: Ez az oldal nem törölhető, mer használatban van az alapértelmezett áruháznál.';
$_['error_checkout']         = 'Figyelmeztetés: Ez az oldal nem törölhető, mer használatban van az alapértelmezett áruház kijelentkezési folyamatánál.';
$_['error_affiliate']        = 'Figyelmeztetés: Ezt az információs oldalt nem lehet törölni, mivel a bolt egyik affiliate feltételéhez!';
$_['error_return']           = 'Warning: This information page cannot be deleted as it is currently assigned as the store return terms!';
$_['error_store']            = 'Figyelmeztetés: Ez az információ nem törölhető, mert használatban van a %s áruházban.';
