<?php
// Heading
$_['heading_title']          = 'Kategóriák';

// Text
$_['text_success']           = 'A kategória módosítása sikeresen megtörtént!';
$_['text_list']              = 'Kategórák';
$_['text_add']               = 'Add Category';
$_['text_edit']              = 'Edit Category';
$_['text_default']           = 'Alapértelmezett';

// Column
$_['column_name']            = 'Kategória név';
$_['column_sort_order']      = 'Sorrend';
$_['column_action']          = 'Művelet';

// Entry
$_['entry_name']             = 'Kategória neve:';
$_['entry_description']      = 'Leírás:';
$_['entry_meta_title'] 	     = 'Meta Title tag (Cím)';
$_['entry_meta_keyword']     = 'Meta kulcsszavak';
$_['entry_meta_description'] = 'Meta leírás:';
$_['entry_keyword']          = 'SEO link';
$_['entry_parent']           = 'Szülő kategória:';
$_['entry_filter']           = 'Szűrők';
$_['entry_store']            = 'Webáruház';
$_['entry_image']            = 'Kép';
$_['entry_top']              = 'Felső menüsor';
$_['entry_column']           = 'Oszlopok';
$_['entry_sort_order']       = 'Sorrend';
$_['entry_status']           = 'Állapot';
$_['entry_layout']           = 'Felület beállítás:';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Ne használjon szóközt a szavak között, inkább kötőjelet vagy aláhúzást és győződjön meg róla, hogy ez a link még nem létezik ezen az oldalon.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';

// Error
$_['error_warning']          = 'Kérjük, ellenőrizze a megadott adatokat!';
$_['error_permission']       = 'Nincs jogosultsága a kategóriák módosításához!';
$_['error_name']             = 'A kategória névnek legalább 2 és legfeljebb 32 karakterből állhat!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';
