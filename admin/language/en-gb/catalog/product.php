<?php
// Heading
$_['heading_title']          = 'Termékek';

// Text
$_['text_success']           = 'A termék módosítása sikeresen megtörtént!';
$_['text_list']              = 'Termékek';
$_['text_add']               = 'Új termék';
$_['text_edit']              = 'Termék szerkesztése';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Alapértelmezett';
$_['text_option']            = 'Választék';
$_['text_option_value']      = 'Választék értéke';
$_['text_percent']           = 'Százalék';
$_['text_amount']            = 'Fix összeg';

// Column
$_['column_name']            = 'Termék név';
$_['column_model']           = 'Cikkszám';
$_['column_image']           = 'Kép';
$_['column_price']           = 'Ár';
$_['column_quantity']        = 'Mennyiség';
$_['column_status']          = 'Állapot';
$_['column_action']          = 'Művelet';

// Entry
$_['entry_name']             = 'Termék neve:';
$_['entry_description']      = 'Leírás:';
$_['entry_meta_title'] 	     = 'Meta Title tag (Cím)';
$_['entry_meta_keyword'] 	 = 'Meta kulcsszavak';
$_['entry_meta_description'] = 'Meta leírás:';
$_['entry_keyword']          = 'SEO link';
$_['entry_model']            = 'Cikkszám:';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC:';
$_['entry_ean']              = 'EAN:';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Egyéb:';
$_['entry_shipping']         = 'Szállítandó termék:';
$_['entry_manufacturer']     = 'Gyártó:';
$_['entry_store']            = 'Webáruház';
$_['entry_date_available']   = 'Mióta kapható:';
$_['entry_quantity']         = 'Mennyiség:';
$_['entry_minimum']          = 'Minimális mennyiség';
$_['entry_stock_status']     = 'Hiánycikk állapot';
$_['entry_price']            = 'Ár';
$_['entry_tax_class']        = 'Áfa kulcs:';
$_['entry_points']           = 'Pontok';
$_['entry_option_points']    = 'Pontok';
$_['entry_subtract']         = 'Készlet kivonása:';
$_['entry_weight_class']     = 'Weight Class';
$_['entry_weight']           = 'Súly:';
$_['entry_dimension']        = 'Méretek ( H x Sz x M )';
$_['entry_length_class']     = 'Length Class';
$_['entry_length']           = 'Hosszúság osztály:';
$_['entry_width']            = 'Szélesség';
$_['entry_height']           = 'Magasság';
$_['entry_image']            = 'Kép';
$_['entry_additional_image'] = 'Additional Images';
$_['entry_customer_group']   = 'Vásárlói csoport';
$_['entry_date_start']       = 'Kezdő dátum';
$_['entry_date_end']         = 'Befejezés dátum';
$_['entry_priority']         = 'Prioritás:';
$_['entry_attribute']        = 'Tulajdonság:';
$_['entry_attribute_group']  = 'Tulajdonság csoport:';
$_['entry_text']             = 'Szöveg:';
$_['entry_option']           = 'Választék:';
$_['entry_option_value']     = 'Választék értéke:';
$_['entry_required']         = 'Szükséges:';
$_['entry_status']           = 'Állapot';
$_['entry_sort_order']       = 'Sorrend';
$_['entry_category']         = 'Kategóriák:';
$_['entry_filter']           = 'Szűrők';
$_['entry_download']         = 'Letöltéseim';
$_['entry_related']          = 'Kapcsolódó termékek';
$_['entry_tag']          	 = 'Termék címkék';
$_['entry_reward']           = 'Hűségpontjaim';
$_['entry_layout']           = 'Felület felülbírálás:';
$_['entry_recurring']        = ' Ismétlődő profil';

// Help
$_['help_keyword']           = 'Ne használjon szóközt a szavak között, inkább kötőjelet vagy aláhúzást és győződjön meg róla, hogy ez a link még nem létezik ezen az oldalon.';
$_['help_sku']               = 'Stock Keeping Unit';
$_['help_upc']               = 'Universal Product Code';
$_['help_ean']               = 'Európai cikkszám';
$_['help_jan']               = 'Japanese Article Number';
$_['help_isbn']              = 'International Standard Book Number';
$_['help_mpn']               = 'Gyáriszám';
$_['help_manufacturer']      = '(Autocomplete)';
$_['help_minimum']           = 'Force a minimum ordered amount';
$_['help_stock_status']      = 'Ez az állapot jelenik meg, ha egy termék nincs raktáron.';
$_['help_points']            = 'Szükséges pontok száma. Ha azt akarod, hogy ezt a terméet ne lehessen megvásárolni pontokkal, hagyd 0-n.';
$_['help_category']          = '(Autocomplete)';
$_['help_filter']            = '(Autocomplete)';
$_['help_download']          = '(Autocomplete)';
$_['help_related']           = '(Autocomplete)';
$_['help_tag']              = 'vesszővel elválasztva';

// Error
$_['error_warning']          = 'Kérlek, nézd át az űrlapot!';
$_['error_permission']       = 'A termékek módosítása az Ön számára nem engedélyezett';
$_['error_name']             = 'A terméknév legalább 3 és legfeljebb 255 karakterből álljon!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'A termék cikkszáma legalább 3, és legfeljebb 64 karakterből álljon!';
$_['error_keyword']          = 'SEO keyword already in use!';
