<?php
// Heading
$_['heading_title']        = 'APIk';

// Text
$_['text_success']         = 'API sikeresen módosítva!';
$_['text_list']            = 'API listák';
$_['text_add']             = 'Add API';
$_['text_edit']            = 'API szerkesztése';
$_['text_ip']              = 'Below you can create a list of IP\'s allowed to access the API. Your current IP is %s';

// Column
$_['column_name']          = 'API Name';
$_['column_status']        = 'Állapot';
$_['column_date_added']    = 'Hozzáadás dátuma';
$_['column_date_modified'] = 'Módosítás dátuma';
$_['column_token']         = 'Token';
$_['column_ip']            = 'IP';
$_['column_action']        = 'Művelet';

// Entry
$_['entry_name']           = 'API Name';
$_['entry_key']            = 'API Key';
$_['entry_status']         = 'Állapot';
$_['entry_ip']             = 'IP';

// Error
$_['error_permission']     = 'Warning: You do not have permission to modify APIs!';
$_['error_name']           = 'API Name must be between 3 and 20 characters!';
$_['error_key']            = 'API Key must be between 64 and 256 characters!';
