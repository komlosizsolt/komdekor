<?php
// Heading
$_['heading_title']     = 'Felhasználó csoport';

// Text
$_['text_success']      = 'A felhasználó csoport(ok) módosítása megtörtént!';
$_['text_list']         = 'Felhasználói csoport';
$_['text_add']          = 'Felhasználói csoport létrehozása';
$_['text_edit']         = 'Felhasználói csoport szerkesztése';

// Column
$_['column_name']       = 'Felhasználó csoport neve';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']        = 'Felhasználó csoport neve:';
$_['entry_access']      = 'Hozzáférési engedély:';
$_['entry_modify']      = 'Módosítási engedély:';

// Error
$_['error_permission']  = 'A felhasználó csoportok módosítása az Ön számára nem engedélyezett!';
$_['error_name']        = 'A felhasználó csoport nevének hosszabbnak kell lennie 3 és rövidebbnek 64 karakternél!';
$_['error_user']        = 'Ezt a felhasználócsoportot nem törölheti, mivel jelenleg a következő felhasználókhoz vannak hozzárendelve: %s!';