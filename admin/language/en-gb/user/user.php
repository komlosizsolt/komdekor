<?php
// Heading
$_['heading_title']     = 'Felhasználók';

// Text
$_['text_success']      = 'A felhasználók módosítása sikeresen megtörtént!';
$_['text_list']         = 'Felhasználók listája';
$_['text_add']          = 'Felhasználó létrehozása';
$_['text_edit']         = 'Felhasználó szerkesztése';

// Column
$_['column_username']   = 'Felhasználónév';
$_['column_status']     = 'Állapot';
$_['column_date_added'] = 'Hozzáadás dátuma';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_username']   	= 'Felhasználónév';
$_['entry_user_group'] 	= 'Felhasználói csoport';
$_['entry_password']   	= 'Jelszó';
$_['entry_confirm']    	= 'Megerősít';
$_['entry_firstname']  	= 'Keresztnév';
$_['entry_lastname']   	= 'Vezetéknév';
$_['entry_email']      	= 'E-mail';
$_['entry_image']      	= 'Kép';
$_['entry_status']     	= 'Állapot';

// Error
$_['error_permission'] 	= 'A felhasználók módosítása az Ön számára nem engedélyezett!';
$_['error_account']    	= 'Nem törölheti a saját fiókját!';
$_['error_exists']     	= 'A felhasználónév már létezik!';
$_['error_username']   	= 'A felhasználónévnek hosszabbnak kell lennie 3 és rövidebbnek 20 karakternél!';
$_['error_password']   	= 'A jelszó minimum 4 maximum 20 karakter lehet!';
$_['error_confirm']    	= 'A jelszó és a jelszó megerősítése nem egyezik!';
$_['error_firstname']  	= 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']   	= 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';