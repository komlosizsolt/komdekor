<?php
$_['heading_title']    = 'Google reCAPTCHA';

// Text
$_['text_captcha']     = 'Captcha';
$_['text_success']	   = 'Sikeresen módosította a Google reCAPTCHA-t!';
$_['text_edit']        = 'Google reCAPTCHA szerkesztése';
$_['text_signup']      = 'Keresd fel a  <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank"><u>Google reCAPTCHA oldalt</u></a> és regisztráld az oldalt.';

// Entry
$_['entry_key']        = 'Site key';
$_['entry_secret']     = 'Secret key';
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'Nincs engedélye a Google Analytics módosításához!';
$_['error_key']	       = 'Kulcs nincs megadva!';
$_['error_secret']	   = 'Titkos kulcs nincs megadva!';
