<?php
$_['heading_title']    = 'Alap Captcha';

// Text
$_['text_captcha']     = 'Captcha';
$_['text_success']	   = 'Success: You have modified Basic Captcha!';
$_['text_edit']        = 'Edit Basic Captcha';

// Entry
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Basic Captcha!';
