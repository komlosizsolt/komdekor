<?php
// Heading
$_['heading_title']    = 'Üzlet kredit';

// Text
$_['text_total']       = 'Összes rendelés';
$_['text_success']     = 'Siker: Módosítottad az üzlet krediteket összesítését!';
$_['text_edit']        = 'Edit Store Credit Total';

// Entry
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Error
$_['error_permission'] = 'Figyelmeztetés: Nincs jogosultságod az üzlet kreditek összesítésének módosításához!';