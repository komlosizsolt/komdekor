<?php
// Heading
$_['heading_title']    = 'Ajándékutalvány';

// Text
$_['text_total']       = 'Összes rendelés';
$_['text_success']     = 'Siker: Módosítottad az ajándékutalvány összesítését!';
$_['text_edit']        = 'Edit Gift Voucher Total';

// Entry
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Error
$_['error_permission'] = 'Nincs jogosultsága az ajándékutalványok módosításához!';