<?php
// Heading
$_['heading_title']    = 'Szállítás';

// Text
$_['text_total']       = 'Összes rendelés';
$_['text_success']     = 'Siker: A szállítási díj módosítása megtörtént!';
$_['text_edit']        = 'Edit Shipping Total';

// Entry
$_['entry_estimator']  = 'Shipping Estimator';
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Error
$_['error_permission'] = 'Figyelmeztetés: A szállítási díj módosítása az Ön számára nem engedélyezett!';