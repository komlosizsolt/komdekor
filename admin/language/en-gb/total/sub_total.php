<?php
// Heading
$_['heading_title']    = 'Részösszeg';

// Text
$_['text_total']       = 'Összes rendelés';
$_['text_success']     = 'Siker: Az összes részösszeg módosítása megtörtént!';
$_['text_edit']        = 'Edit Sub-Total Total';

// Entry
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az összes részösszeg módosítása az Ön számára nem engedélyezett!';