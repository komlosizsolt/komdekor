<?php
// Heading
$_['heading_title']    = 'Összesen';

// Text
$_['text_total']       = 'Összes rendelés';
$_['text_success']     = 'Siker: Az összes rendelés módosítása megtörtént!';
$_['text_edit']        = 'Edit Total Total';

// Entry
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Error
$_['error_permission'] = ' Az összes rendelés módosítása az Ön számára nem engedélyezett!';