<?php
// Heading
$_['heading_title']    = 'Minimális rendelési összeg';

// Text
$_['text_total']       = 'Összes rendelés';
$_['text_success']     = 'Siker: A legkevesebb rendelési díj módosítása megtörtént!';
$_['text_edit']        = 'Edit Low Order Fee Total';

// Entry
$_['entry_total']      = 'Minimális rendelés összege:';
$_['entry_fee']        = 'Díja:';
$_['entry_tax_class']  = 'Áfa kulcs:';
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Help
$_['help_total']       = 'The checkout total the order must reach before this order total is deactivated.';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az összes legkevesebb rendelési díj módosítása az Ön számára nem engedélyezett!';
