<?php
// Heading
$_['heading_title']     = 'Adóosztály';

// Text
$_['text_success']      = 'Siker: Az adóosztály módosítása megtörtént!';
$_['text_list']         = 'Tax Class List';
$_['text_add']          = 'Add Tax Class';
$_['text_edit']         = 'Edit Tax Class';
$_['text_shipping']     = 'Shipping Address';
$_['text_payment']      = 'Payment Address';
$_['text_store']        = 'Store Address';

// Column
$_['column_title']      = 'Adóosztály';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_title']       = 'Adóosztály megnevezése:';
$_['entry_description'] = 'Leírás:';
$_['entry_rate']        = 'Áfakulcs:';
$_['entry_based']       = 'Based On';
$_['entry_geo_zone']    = 'Földrajzi zóna:';
$_['entry_priority']    = 'Prioritás:';

// Error
$_['error_permission']  = 'Figyelmeztetés: Az adóosztályok módosítása az Ön számára nem engedélyezett';
$_['error_title']       = 'Az adóosztály megnevezése legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_description'] = 'Az áfakulcs leírásának hosszabbnak kell lennie 3 és rövidebbnek 255 karakternél!';
$_['error_product']     = 'Figyelmeztetés: Ezt az adóosztályt nem törölheti, mert jelenleg %s termékhez rendelték hozzá!';