<?php
// Heading
$_['heading_title']    = 'Üzlethelyiségek';

// Text
$_['text_success']     = 'Success: You have modified store locations!';
$_['text_list']        = 'Üzlethelyiségek listája';
$_['text_add']         = 'Add Store Location';
$_['text_edit']        = 'Edit Store Location';
$_['text_default']     = 'Alapértelmezett';
$_['text_time']        = 'Nyitva tartás';
$_['text_geocode']     = 'Geocode was not successful for the following reason:';

// Column
$_['column_name']      = 'Store Name';
$_['column_address']   = 'Cím';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_name']       = 'Store Name';
$_['entry_address']    = 'Cím';
$_['entry_geocode']    = 'Geocode';
$_['entry_telephone']  = 'Telefon';
$_['entry_fax']        = 'Fax';
$_['entry_image']      = 'Kép';
$_['entry_open']       = 'Nyitva tartás';
$_['entry_comment']    = 'Megjegyzés';

// Help
$_['help_geocode']     = 'Please enter your store location geocode manually.';
$_['help_open']        = 'Áruház nyitvatartási ideje';
$_['help_comment']     = 'This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques.';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify store locations!';
$_['error_name']       = 'Store name must be at least 1 character!';
$_['error_address']    = 'Address must be between 3 and 128 characters!';
$_['error_telephone']  = 'A telefonszám minimum 3 maximum 32 karakter lehet!';