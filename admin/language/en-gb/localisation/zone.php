<?php
// Heading
$_['heading_title']          = 'Zónák';

// Text
$_['text_success']           = 'Siker: A zóna frissítése megtörtént!';
$_['text_list']              = 'Zone List';
$_['text_add']               = 'Add Zone';
$_['text_edit']              = 'Edit Zone';

// Column
$_['column_name']            = 'Zóna';
$_['column_code']            = 'Zónakód';
$_['column_country']         = 'Ország';
$_['column_action']          = 'Művelet';

// Entry
$_['entry_name']             = 'Zóna neve:';
$_['entry_code']             = 'Zónakód:';
$_['entry_country']          = 'Ország';
$_['entry_status']           = 'Állapot';

// Error
$_['error_permission']       = 'Figyelmeztetés: A zónák módosítása az Ön számára nem engedélyezett';
$_['error_name']             = 'Figyelmeztetés: A zóna neve legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_default']          = 'Figyelmeztetés: Ezt a zónát nem törölheti, mert jelenleg az üzlet alapértelmezett zónájaként hozzárendelt!';
$_['error_store']            = 'Figyelmeztetés:  Ez a zóna nem törölhető, mert ez az alapértelmezett a következő áruházban: %s!';
$_['error_address']          = 'Figyelmeztetés: Ezt a zónát nem törölheti, mert hozzá van rendelve a(z) %s címjegyzék bejegyzéshez!';
$_['error_affiliate']        = 'Figyelmeztetés: Ezt a visszaküldés műveletet nem lehet törölni mivel %s partner programhoz van rendelve!';
$_['error_zone_to_geo_zone'] = 'Figyelmeztetés: Ezt a zónát nem törölheti, mert jelenleg %s zóna hozzárendelt földrajzi zónákhoz!';