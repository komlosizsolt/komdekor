<?php
// Heading
$_['heading_title']        = 'Pénznemek';

// Text
$_['text_success']         = 'Siker: A pénznem módosítása megtörtént!';
$_['text_list']            = 'Pénznemek listája';
$_['text_add']             = 'Pénznem hozzáadása';
$_['text_edit']            = 'Pénznem szerkesztése';

// Column
$_['column_title']         = 'Pénzenm';
$_['column_code']          = 'Kód';
$_['column_value']         = 'Érték';
$_['column_date_modified'] = 'Frissítve';
$_['column_action']        = 'Művelet';

// Entry
$_['entry_title']          = 'Pénznem megnevezése:';
$_['entry_code']           = 'Kód';
$_['entry_value']          = 'Érték';
$_['entry_symbol_left']    = 'Rövidítés balra:';
$_['entry_symbol_right']   = 'Rövidítés jobbra:';
$_['entry_decimal_place']  = 'Tizedesjegyek:';
$_['entry_status']         = 'Állapot';

// Help
$_['help_code']            = 'Do not change if this is your default currency. Must be valid <a href="http://www.xe.com/iso4217.php" target="_blank">ISO code</a>.';
$_['help_value']           = 'Állítsa 1.00000 -re, ha ez az alapértelmezett valuta.';

// Error
$_['error_permission']     = 'Figyelmeztetés: A pénznemek módosítása az Ön számára nem engedélyezett';
$_['error_title']          = 'A pénznem megnevezése legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_code']           = 'A pénznem kódjának 3 karakterből kell állnia!';
$_['error_default']        = 'Figyelmeztetés: Ezt a pénznemet nem törölheti, mivel jelenleg ez az áruház alapértelmezésként hozzárendelt pénzneme!';
$_['error_store']          = 'Figyelmeztetés: Ez a valuta nem törölhető, mert a %s áruházhoz tartozik!';
$_['error_order']          = 'Figyelmeztetés: Ezt a pénznemet nem törölheti, mivel jelenleg %s rendeléshez hozzárendelt!';