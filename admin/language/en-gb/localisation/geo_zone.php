<?php
// Heading
$_['heading_title']      = 'Geo zónák';

// Text
$_['text_success']       = 'Siker: A földrajzi zóna frissítése megtörtént!';
$_['text_list']          = 'Geo Zone List';
$_['text_add']           = 'Add Geo Zone';
$_['text_edit']          = 'Edit Geo Zone';

// Column
$_['column_name']        = 'Földrajzi zóna';
$_['column_description'] = 'Leírás';
$_['column_action']      = 'Művelet';

// Entry
$_['entry_name']         = 'Földrajzi zóna neve:';
$_['entry_description']  = 'Leírás:';
$_['entry_country']      = 'Ország';
$_['entry_zone']         = 'Zóna:';

// Error
$_['error_permission']   = 'Figyelmeztetés: A földrajzi zónák módosítása az Ön számára nem engedélyezett';
$_['error_name']         = 'A földrajzi zóna neve legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_description']  = 'A leírás legalább 3, és legfeljebb 255 karakterből álljon!';
$_['error_tax_rate']     = 'Figyelmeztetés: Ezt a földrajzi zónát nem törölheti, mert jelenleg hozzárendelt egy vagy több áfakulcshoz!';