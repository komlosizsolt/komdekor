<?php
// Heading
$_['heading_title']    = 'Hosszúság osztályt';

// Text
$_['text_success']     = 'Siker: Módosította a hosszúság osztályt!';
$_['text_list']        = 'Length Class List';
$_['text_add']         = 'Add Length Class';
$_['text_edit']        = 'Edit Length Class';

// Column
$_['column_title']     = 'Hosszúság cím';
$_['column_unit']      = 'Hosszúság egység';
$_['column_value']     = 'Érték';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_title']      = 'Hosszúság cím:';
$_['entry_unit']       = 'Hosszúság egység:';
$_['entry_value']      = 'Érték';

// Help
$_['help_value']       = 'Set to 1.00000 if this is your default length.';

// Error
$_['error_permission'] = 'Figyelmeztetés: Nincs engedélye a hosszúság-osztály módosítására!';
$_['error_title']      = 'Figyelmeztetés: Hosszúság cím 3 és 32 karakter közé kell hogy essen!';
$_['error_unit']       = 'Figyelmeztetés: Hosszúság egység 1 és 4 karakter közé kell hogy essen!';
$_['error_default']    = 'Figyelmeztetés: Ez a hosszúság osztály nem törölhető, mert jelenleg ez az alapértelmezett hosszúság az áruházban!';
$_['error_product']    = 'Figyelmeztetés: Ez a hosszúságosztály nem törölhető, mert hozzá van rendelve a követhező termékhez: %s!';