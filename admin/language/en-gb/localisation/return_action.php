<?php
// Heading
$_['heading_title']    = 'Visszárú művelet';

// Text
$_['text_success']     = 'iker: Módosítottad a visszárú műveleteket!';
$_['text_list']        = 'Return Action List';
$_['text_add']         = 'Add Return Action';
$_['text_edit']        = 'Edit Return Action';

// Column
$_['column_name']      = 'Visszárú művelet neve';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_name']       = 'Visszárú művelet neve:';

// Error
$_['error_permission'] = 'Figyelmeztetés: Nincs jogosúltságod a visszárú műveletek szerkesztéséhez!';
$_['error_name']       = 'Visszárú művelet neve legalább 3 és legfeljebb 32 karakter lehet!';
$_['error_return']     = 'Figyelmeztetés: Ezt a visszárú műveletet nem lehet törölni mivel %s visszárú termékekhez van rendelve!';