<?php
// Heading
$_['heading_title']        = 'Tax Rates';

// Text
$_['text_success']         = 'Success: You have modified tax classes!';
$_['text_list']            = 'Tax Rate List';
$_['text_add']             = 'Add Tax Rate';
$_['text_edit']            = 'Edit Tax Rate';
$_['text_percent']         = 'Százalék';
$_['text_amount']          = 'Fix összeg';

// Column
$_['column_name']          = 'Tax Name';
$_['column_rate']          = 'Tax Rate';
$_['column_type']          = 'Típus';
$_['column_geo_zone']      = 'Geo Zone';
$_['column_date_added']    = 'Date Added';
$_['column_date_modified'] = 'Módosítás dátuma';
$_['column_action']        = 'Művelet';

// Entry
$_['entry_name']           = 'Tax Name:';
$_['entry_rate']           = 'Tax Rate:';
$_['entry_type']           = 'Típus';
$_['entry_customer_group'] = 'Vásárlói csoport';
$_['entry_geo_zone']       = 'Geo Zone:';

// Error
$_['error_permission']     = 'Warning: You do not have permission to modify tax classes!';
$_['error_tax_rule']       = 'Warning: This tax rate cannot be deleted as it is currently assigned to %s tax classes!';
$_['error_name']           = 'Tax Name must be between 3 and 32 characters!';
$_['error_rate']           = 'Tax Rate required!';