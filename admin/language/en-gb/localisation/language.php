<?php
// Heading
$_['heading_title']     = 'Nyelv';

// Text
$_['text_success']      = 'Siker: A nyelv frissítése megtörtént!';
$_['text_list']         = 'Nyelvek listája';
$_['text_add']          = 'Add Language';
$_['text_edit']         = 'Edit Language';

// Column
$_['column_name']       = 'Nyelv neve';
$_['column_code']       = 'Kód';
$_['column_sort_order'] = 'Sorrend';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']        = 'Nyelv neve:';
$_['entry_code']        = 'Kód';
$_['entry_status']      = 'Állapot';
$_['entry_sort_order']  = 'Sorrend';

// Help
$_['help_status']       = 'Hide/Show it in language dropdown';

// Error
$_['error_permission']  = 'Figyelmeztetés: A nyelvek módosítása az Ön számára nem engedélyezett!';
$_['error_exists']      = 'Warning: You added before the language!';
$_['error_name']        = 'A nyelv neve legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_code']        = 'A nyelvi kód 2 karakterből álljon!';
$_['error_default']     = 'Figyelmeztetés: Ezt a nyelvet nem törölheti, mert jelenleg az áruház alapértelmezett nyelvéhez hozzárendelt!';
$_['error_admin']       = 'Figyelmeztetés: Ez a nyelv nem törölhető, mert jelenleg ez az adminisztráció nyelve!';
$_['error_store']       = 'Figyelmeztetés: Ez a nyelv nem törölhető, mert jelenleg hozzá van rendelve a követhező áruházhoz: %s!';
$_['error_order']       = 'Figyelmeztetés: Ezt a nyelvet nem törölheti, mert jelenleg %s rendeléshez hozzárendelt!';