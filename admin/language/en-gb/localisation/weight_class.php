<?php
// Heading
$_['heading_title']    = 'Súlymérték';

// Text
$_['text_success']     = 'Siker: A súlymérték frissítése megtörtént!';
$_['text_list']        = 'Weight Class List';
$_['text_add']         = 'Add Weight Class';
$_['text_edit']        = 'Edit Weight Class';

// Column
$_['column_title']     = 'Súlymérték';
$_['column_unit']      = 'Rövisítés';
$_['column_value']     = 'Érték';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_title']      = 'Súlymérték megnevezése:';
$_['entry_unit']       = 'Rövidítés:';
$_['entry_value']      = 'Érték';

// Help
$_['help_value']       = 'Set to 1.00000 if this is your default weight.';

// Error
$_['error_permission'] = 'Figyelmeztetés: A súlymértékek módosítása az Ön számára nem engedélyezett';
$_['error_title']      = 'Figyelmeztetés: A súlymérték megnevezése legalább 3, és legfeljebb 32 karakterből álljon!';
$_['error_unit']       = 'Figyelmeztetés: A súly rövidítése legalább 1, és legfeljebb 4 karakterből álljon!';
$_['error_default']    = 'Figyelmeztetés: Ezt a súlymértéket nem törölheti, mert jelenleg ezt rendelte hozzá az üzleti alapértelmezett rendelési súlymértékeként!';
$_['error_product']    = 'Figyelmeztetés: Ezt a súlymértéket nem törölheti, mert jelenleg %s termékhez rendelték hozzá!';