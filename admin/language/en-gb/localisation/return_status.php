<?php
// Heading
$_['heading_title']    = 'Visszaküldések állapota';

// Text
$_['text_success']     = 'Sikeresen módosítottad a visszaküldések állapotát!';
$_['text_list']        = 'Visszaküldések listája';
$_['text_add']         = 'Visszaküldés hozzáadása';
$_['text_edit']        = 'Visszaküldés szerkesztése';

// Column
$_['column_name']      = 'Visszaküldés állapot neve';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_name']       = 'Visszaküldés állapot neve';

// Error
$_['error_permission'] = 'Figyelmeztetés: Nincs jogosultságod a Visszaküldés állapotok szerkesztéséhez!';
$_['error_name']       = 'Visszaküldés állapot neve legalább 3 és legfeljebb 32 karakter lehet!';
$_['error_default']    = 'Figyelmeztetés: Ezt a visszaküldés műveletet nem lehet törölni mivel alapértelmezett státusznak van megjelölve!';
$_['error_return']     = 'Figyelmeztetés: Ezt a visszaküldés műveletet nem lehet törölni mivel %s  visszaküldéshez van rendelve!';
