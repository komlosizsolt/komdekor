<?php
// Heading
$_['heading_title']           = 'Országok';

// Text
$_['text_success']            = 'Siker: Az ország módosítása megtörtént!';
$_['text_list']               = 'Country List';
$_['text_add']                = 'Add Country';
$_['text_edit']               = 'Ország szerkesztése';

// Column
$_['column_name']             = 'Ország';
$_['column_iso_code_2']       = 'ISO-kód (2)';
$_['column_iso_code_3']       = 'ISO-kód (3)';
$_['column_action']           = 'Művelet';

// Entry
$_['entry_name']              = 'Ország neve:';
$_['entry_iso_code_2']        = 'ISO-kód (2):';
$_['entry_iso_code_3']        = 'ISO-kód (3):';
$_['entry_address_format']    = 'Cím formátuma';
$_['entry_postcode_required'] = 'Irányítószám szükséges';
$_['entry_status']            = 'Állapot';

// Help
$_['help_address_format']     = 'Keresztnév = {firstname}<br />
Vezetéknév = {lastname}<br />
Cég = {company}<br />
Utca, házszám = {address_1}<br />
Kiegészítő címadatok = {address_2}<br />
Település = {city}<br />
Irányítószám = {postcode}<br />
Állam / Megye = {zone}<br />
Államkód = {zone_code}<br />
Ország = {country}';

// Error
$_['error_permission']        = 'Figyelmeztetés: Az országok módosítása az Ön számára nem engedélyezett!';
$_['error_name']              = 'Az országnév legalább 3, és legfeljebb 128 karakterből álljon!';
$_['error_default']           = 'Figyelmeztetés: Ezt az országot nem törölheti, mert jelenleg az áruház alapértelmezett országaként hozzárendelt!';
$_['error_store']             = 'Figyelmeztetés: Ez az ország nem törölhető, mert a %s áruházhoz tartozik!';
$_['error_address']           = 'Figyelmeztetés: Ezt az országot nem törölheti, mivel jelenleg hozzárendelt a(z)) %s címjegyzék bejegyzésekhez!';
$_['error_affiliate']         = 'Figyelmeztetés: Ezt az országot nem törölheti, mivel jelenleg %s partner programhoz van rendelve!';
$_['error_zone']              = 'Figyelmeztetés: Ezt az országot nem törölheti, mivel jelenleg %s zónához hozzárendelt!';
$_['error_zone_to_geo_zone']  = 'Figyelmeztetés: Ezt az országot nem törölheti, mivel jelenleg %s zóna földrajzi zónákba hozzárendelt!';