<?php
// Heading
$_['heading_title'] = 'Értékesítési statisztika';

// Text
$_['text_order']    = 'Rendelések';
$_['text_customer'] = 'Vásárlók';
$_['text_day']      = 'Ma';
$_['text_week']     = 'hét';
$_['text_month']    = 'hónap';
$_['text_year']     = 'év';