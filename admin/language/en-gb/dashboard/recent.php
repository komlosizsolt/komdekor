<?php
// Heading
$_['heading_title']     = 'Utolsó rendelések';

// Column
$_['column_order_id']   = 'Rendelési azonosító';
$_['column_customer']   = 'Vásárló';
$_['column_status']     = 'Állapot';
$_['column_total']      = 'Összesen';
$_['column_date_added'] = 'Hozzáadás dátuma';
$_['column_action']     = 'Művelet';