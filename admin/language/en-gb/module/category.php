<?php
// Heading
$_['heading_title']    = 'Kategória';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Siker: A kategória modul módosítása megtörtént!';
$_['text_edit']        = 'Kategória modul szerkesztése';

// Entry
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'Figyelmeztetés: A kategória modul módosítása az Ön számára nem engedélyezett!';