<?php
// Heading
$_['heading_title']     = 'HTML Content';

// Text
$_['text_module']       = 'Modulok';
$_['text_success']      = 'Success: You have modified HTML Content module!';
$_['text_edit']         = 'Edit HTML Content Module';

// Entry
$_['entry_name']        = 'Modul neve';
$_['entry_title']       = 'Heading Title';
$_['entry_description'] = 'Leírás';
$_['entry_status']      = 'Állapot';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify HTML Content module!';
$_['error_name']        = 'Module neve minimum 3 és maximum 64 karakter lehet';