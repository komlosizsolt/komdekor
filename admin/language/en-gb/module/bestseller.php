<?php
// Heading
$_['heading_title']    = 'Sikertermékek';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'A sikertermékek modul módosítása sikeresen megtörtént!';
$_['text_edit']        = 'Sikertermékek modul szerkesztése';

// Entry
$_['entry_name']       = 'Modul neve';
$_['entry_limit']      = 'Korlátozás:';
$_['entry_image']      = 'Kép (SZ x M) és átméretezés:';
$_['entry_width']      = 'Szélesség';
$_['entry_height']     = 'Magasság';
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'Nincs jogosultsága a sikertermék modul módosításához!';
$_['error_name']       = 'Module neve minimum 3 és maximum 64 karakter lehet';
$_['error_width']      = 'Szélesség kötelező!';
$_['error_height']     = 'Magasság kötelező!';