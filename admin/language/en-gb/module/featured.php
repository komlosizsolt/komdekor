<?php
// Heading
$_['heading_title']    = 'Kiemelt';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Siker: A Kiemelt modul módosítása megtörtént!';
$_['text_edit']        = 'Edit Featured Module';

// Entry
$_['entry_name']       = 'Modul neve';
$_['entry_product']    = 'Termékek:';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Szélesség';
$_['entry_height']     = 'Magasság';
$_['entry_status']     = 'Állapot';

// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Figyelmeztetés: Nincs jogosúltságod a kiemelt termékek modul szerkesztéséhez!';
$_['error_name']       = 'Module neve minimum 3 és maximum 64 karakter lehet';
$_['error_width']      = 'Szélesség kötelező!';
$_['error_height']     = 'Magasság kötelező!';