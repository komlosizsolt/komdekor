<?php
// Heading
$_['heading_title']    = 'Affiliate';

$_['text_module']      = 'Modulok';
$_['text_success']     = 'Siker: A Partner program modul módosítása megtörtént!';
$_['text_edit']        = 'Edit Affiliate Module';

// Entry
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'Figyelmeztetés: Nincs jogosúltságod a partner program modul szerkesztéséhez!';