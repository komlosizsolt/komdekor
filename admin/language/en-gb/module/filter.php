<?php
// Heading
$_['heading_title']    = 'Szűrők';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Success: You have modified module filter!';
$_['text_edit']        = 'Edit Filter Module';

// Entry
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module filter!';