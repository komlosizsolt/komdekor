<?php
// Heading
$_['heading_title']    = 'Legújabb termékek';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'A legújabb termékek modul módosítása sikeresen megtörtént!';
$_['text_edit']        = 'Legújabb modul szerkesztése';

// Entry
$_['entry_name']       = 'Modul neve';
$_['entry_limit']      = 'Maximális:';
$_['entry_width']      = 'Szélesség';
$_['entry_height']     = 'Magasság';
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'A legújabb termékek modul módosítása az Ön számára nem engedélyezett!';
$_['error_name']       = 'Module neve minimum 3 és maximum 64 karakter lehet';
$_['error_width']      = 'Szélesség kötelező!';
$_['error_height']     = 'Magasság kötelező!';