<?php
// Heading
$_['heading_title']    = 'Információk';

// Text
$_['text_module']      = 'Modulok';
$_['text_success']     = 'Siker: Az információk modul módosítása megtörtént!';
$_['text_edit']        = 'Edit Information Module';

// Entry
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az információk modul módosítása az Ön számára nem engedélyezett!';