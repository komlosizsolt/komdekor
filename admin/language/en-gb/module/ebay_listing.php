<?php
// Heading
$_['heading_title']     = 'eBay Listing';

// Text
$_['text_module']       = 'Modulok';
$_['text_success']      = 'Success: You have modified module eBay featured!';
$_['text_edit']        	= 'Edit eBay module';
$_['text_list']         = 'Layout List';
$_['text_register']     = 'You need to register and enable OpenBay Pro for eBay!';
$_['text_about'] 		= 'The eBay display module allows you to display products from your eBay account directly on your website.';
$_['text_latest']       = 'Legújabb';
$_['text_random']       = 'Random';

// Entry
$_['entry_name']        = 'Modul neve';
$_['entry_username']    = 'eBay Felhasználónév';
$_['entry_keywords']    = 'Search Keywords';
$_['entry_description'] = 'Include Description Search';
$_['entry_limit']       = 'Limit';
$_['entry_length']      = 'Length';
$_['entry_width']       = 'Szélesség';
$_['entry_height']      = 'Magasság';
$_['entry_site']   		= 'eBay Site';
$_['entry_sort']   		= 'Sort by';
$_['entry_status']   	= 'Állapot';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify module eBay!';
$_['error_name']        = 'Module neve minimum 3 és maximum 64 karakter lehet';
$_['error_width']       = 'Szélesség kötelező!';
$_['error_height']      = 'Magasság kötelező!';