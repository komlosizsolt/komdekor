<?php
// Heading
$_['heading_title']      = 'Bannerek';

// Text
$_['text_success']       = 'A banner(ek) módosítása sikeresen megtörtént!';
$_['text_list']          = 'Banner lista';
$_['text_add']           = 'Banner hozzáadása';
$_['text_edit']          = 'Banner szerkesztése';
$_['text_default']       = 'Alapértelmezett';

// Column
$_['column_name']        = 'Banner neve';
$_['column_status']      = 'Állapot';
$_['column_action']      = 'Művelet';

// Entry
$_['entry_name']         = 'Banner neve:';
$_['entry_title']        = 'Cím:';
$_['entry_link']         = 'Link ';
$_['entry_image']        = 'Kép';
$_['entry_status']       = 'Állapot';
$_['entry_sort_order']   = 'Sorrend';

// Error
$_['error_permission']   = 'Figyelmeztetés: Nincs jogosultságod a bannerek módosításához!';
$_['error_name']         = 'A banner neve legalább 3 és legfeljebb 64 karakter lehet!';
$_['error_title']        = 'A banner címe legalább 2 és legfeljebb 64 karakter lehet!';