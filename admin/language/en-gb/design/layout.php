<?php
// Heading
$_['heading_title']       = 'Felület';

// Text
$_['text_success']        = 'Siker: Felületek módosítása megtörtént!';
$_['text_list']           = 'Layout List';
$_['text_add']            = 'Add Layout';
$_['text_edit']           = 'Felület szerkesztése';
$_['text_default']        = 'Alapértelmezett';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Bal oszlop';
$_['text_column_right']   = 'Jobb oszlop';

// Column
$_['column_name']         = 'Felület neve';
$_['column_action']       = 'Művelet';

// Entry
$_['entry_name']          = 'Felület neve:';
$_['entry_store']         = 'Bolt:';
$_['entry_route']         = 'Útvonal:';
$_['entry_module']        = 'Modul';
$_['entry_position']      = 'Position';
$_['entry_sort_order']    = 'Sorrend';

// Error
$_['error_permission']    = 'Figyelmeztetés: Nincs jogosultságod a felületek szerkesztéséhez!';
$_['error_name']          = 'Felület név legalább 3 és legfeljebb 64 karakter lehet!';
$_['error_default']       = 'Figyelmeztetés: Ezt a felületet nem lehet törölni, mivel a weboldalhoz van rendelve mint alap felület!';
$_['error_store']         = 'Figyelmeztetés: Ezt a felületet nem lehet törölni, mivel a(z) %s bolthoz van rendelve!';
$_['error_product']       = 'Figyelmeztetés: Ezt a felületet nem lehet törölni, mivel a(z) %s termékhez van rendelve!';
$_['error_category']      = 'Figyelmeztetés: Ezt a felületet nem lehet törölni, mivel a(z) %s kategóriához van rendelve!';
$_['error_information']   = 'Figyelmeztetés: Ezt a felületet nem lehet törölni, mivel a(z) %s információs oldalhoz van rendelve!';