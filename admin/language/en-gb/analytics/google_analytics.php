<?php
$_['heading_title']    = 'Google Analytics';

// Text
$_['text_analytics']   = 'Analytics';
$_['text_success']	   = 'Google Analytics sikeresen módostva!';
$_['text_edit']        = 'Google Analytics szerkesztése';
$_['text_signup']      = 'Jelentkezzen be <a href="http://www.google.com/analytics/" target="_blank"><u>Google Analytics</u></a> fiókjába és másolja ide a beillesztőkódot.';
$_['text_default']     = 'Default';

// Entry
$_['entry_code']       = 'Google Analytics kód';
$_['entry_status']     = 'Állapot';

// Error
$_['error_permission'] = 'Nincs engedélye a Google Analytics módosításához!';
$_['error_code']	   = 'Kód nincs megadva!';
