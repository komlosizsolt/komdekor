<?php
// Heading
$_['heading_title']    = 'Biztonsági mentés / Visszaállítás';

// Text
$_['text_success']     = 'Az adatbázis importálása sikeres!';

// Entry
$_['entry_import']     = 'Import';
$_['entry_export']     = 'Export';

// Error
$_['error_permission'] = 'Figyelmeztetés: A biztonsági mentések módosítása az Ön számára nem engedélyezett';
$_['error_export']     = 'Warning: You must select at least one table to export!';
$_['error_empty']      = 'A feltöltött fájl üres!';