<?php
// Heading
$_['heading_title']	   = 'Hibanapló';

// Text
$_['text_success']	   = 'A hibanaplót sikeresen törölte!';
$_['text_list']        = 'Errors List';

// Error
$_['error_warning']	   = 'Warning: Your error log file %s is %s!';
$_['error_permission'] = 'Warning: You do not have permission to clear error log!';