<?php
// Heading
$_['heading_title']     = 'Feltöltések';

// Text
$_['text_success']      = 'Success: You have modified uploads!';
$_['text_list']         = 'Upload List';

// Column
$_['column_name']       = 'Upload Name';
$_['column_filename']   = 'Fájlnév';
$_['column_date_added'] = 'Hozzáadás dátuma';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']        = 'Upload Name';
$_['entry_filename']    = 'Fájlnév';
$_['entry_date_added'] 	= 'Hozzáadás dátuma';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify uploads!';