<?php
// Heading
$_['heading_title']       = 'Customer Activity Report';

// Text
$_['text_list']           = 'Customer Activity List';
$_['text_address_add']    = '<a href="customer_id=%d">%s</a> added a new address.';
$_['text_address_edit']   = '<a href="customer_id=%d">%s</a> updated their address.';
$_['text_address_delete'] = '<a href="customer_id=%d">%s</a> deleted one of their address.';
$_['text_edit']           = '<a href="customer_id=%d">%s</a> updated their account details.';
$_['text_forgotten']      = '<a href="customer_id=%d">%s</a> requested a new password.';
$_['text_reset']          = '<a href="customer_id=%d">%s</a> reset their account password.';
$_['text_login']          = '<a href="customer_id=%d">%s</a> logged in.';
$_['text_password']       = '<a href="customer_id=%d">%s</a> updated their account password.';
$_['text_register']       = '<a href="customer_id=%d">%s</a> registered for an account.';
$_['text_return_account'] = '<a href="customer_id=%d">%s</a> elküldött egy visszaküldési igényt.';
$_['text_return_guest']   = '%s visszaküldése volt.';
$_['text_order_account']  = '<a href="customer_id=%d">%s</a> created a <a href="order_id=%d">new order</a>.';
$_['text_order_guest']    = '%s created a <a href="order_id=%d">new order</a>.';

// Column
$_['column_customer']     = 'Vásárló';
$_['column_comment']      = 'Megjegyzés';
$_['column_ip']           = 'IP';
$_['column_date_added']   = 'Hozzáadás dátuma';

// Entry
$_['entry_customer']      = 'Vásárló';
$_['entry_ip']            = 'IP';
$_['entry_date_start']    = 'Kezdő dátum';
$_['entry_date_end']      = 'Befejezés dátum';
