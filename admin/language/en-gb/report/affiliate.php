<?php
// Heading
$_['heading_title']     = 'Affiliate Commission Report';

// Text
$_['text_list']         = 'Affiliate Commission List';

// Column
$_['column_affiliate']  = 'Affiliate Name';
$_['column_email']      = 'E-mail';
$_['column_status']     = 'Állapot';
$_['column_commission'] = 'Commission';
$_['column_orders']     = 'Rendelések száma';
$_['column_total']      = 'Összesen';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_date_start']  = 'Kezdő dátum';
$_['entry_date_end']    = 'Befejezés dátum';