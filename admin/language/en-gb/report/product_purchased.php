<?php
// Heading
$_['heading_title']     = 'Vásárolt termékek';

// Text
$_['text_list']         = 'Vásárolt termékek listája';
$_['text_all_status']   = 'Minden állapot';

// Column
$_['column_date_start'] = 'Kezdő dátum';
$_['column_date_end']   = 'Befejezés dátum';
$_['column_name']       = 'Terméknév';
$_['column_model']      = 'Cikkszám';
$_['column_quantity']   = 'Mennyiség';
$_['column_total']      = 'Összesen';

// Entry
$_['entry_date_start']  = 'Kezdő dátum';
$_['entry_date_end']    = 'Befejezés dátum';
$_['entry_status']      = 'Rendelés státusza';