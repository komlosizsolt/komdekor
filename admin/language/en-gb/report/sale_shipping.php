<?php
// Heading
$_['heading_title']     = 'Kiszállítás Report';

// Text
$_['text_list']         = 'Shipping List';
$_['text_year']         = 'Évek';
$_['text_month']        = 'Hónapok';
$_['text_week']         = 'Hetek';
$_['text_day']          = 'Napok';
$_['text_all_status']   = 'Minden állapot';

// Column
$_['column_date_start'] = 'Kezdő dátum';
$_['column_date_end']   = 'Befejezés dátum';
$_['column_title']      = 'Kiszállítás neve';
$_['column_orders']     = 'Rendelések száma';
$_['column_total']      = 'Összesen';

// Entry
$_['entry_date_start']  = 'Kezdő dátum';
$_['entry_date_end']    = 'Befejezés dátum';
$_['entry_group']       = 'Rendezés:';
$_['entry_status']      = 'Rendelés státusza';