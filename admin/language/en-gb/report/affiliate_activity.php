<?php
// Heading
$_['heading_title']     = 'Affiliate Activity Report';

// Text
$_['text_list']         = 'Affiliate Activity List';
$_['text_edit']         = '<a href="affiliate_id=%d">%s</a> updated their account details.';
$_['text_forgotten']    = '<a href="affiliate_id=%d">%s</a> requested a new password.';
$_['text_login']        = '<a href="affiliate_id=%d">%s</a> logged in.';
$_['text_password']     = '<a href="affiliate_id=%d">%s</a> updated their account password.';
$_['text_payment']      = '<a href="affiliate_id=%d">%s</a> updated their payment details.';
$_['text_register']     = '<a href="affiliate_id=%d">%s</a> registered for a new account.';

// Column
$_['column_affiliate']  = 'Affiliate';
$_['column_comment']    = 'Megjegyzés';
$_['column_ip']         = 'IP';
$_['column_date_added'] = 'Hozzáadás dátuma';

// Entry
$_['entry_affiliate']   = 'Affiliate';
$_['entry_ip']          = 'IP';
$_['entry_date_start']  = 'Kezdő dátum';
$_['entry_date_end']    = 'Befejezés dátum';