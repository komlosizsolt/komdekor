<?php
// Heading
$_['heading_title']    = 'Kupon Report';

// Text
$_['text_list']        = 'Kuponok listája';

// Column
$_['column_name']      = 'Kupon neve';
$_['column_code']      = 'Kód';
$_['column_orders']    = 'Rendelések';
$_['column_total']     = 'Összesen';
$_['column_action']    = 'Művelet';

// Entry
$_['entry_date_start'] = 'Kezdő dátum';
$_['entry_date_end']   = 'Befejezés dátum';