<?php
// Heading
$_['heading_title']    = 'Marketing Report';

// Text
$_['text_list']         = 'Marketing List';
$_['text_all_status']   = 'Minden állapot';

// Column
$_['column_campaign']  = 'Kampány neve';
$_['column_code']      = 'Kód';
$_['column_clicks']    = 'Kattintások';
$_['column_orders']    = 'Rendelések száma';
$_['column_total']     = 'Összesen';

// Entry
$_['entry_date_start'] = 'Kezdő dátum';
$_['entry_date_end']   = 'Befejezés dátum';
$_['entry_status']     = 'Rendelés státusza';