<?php
// Heading
$_['heading_title']     = 'Fizetés';

// Text
$_['text_success']      = 'Success: You have modified payments!';
$_['text_list']         = 'Payment List';

// Column
$_['column_name']       = 'Fizetési mód';
$_['column_status']     = 'Állapot';
$_['column_sort_order'] = 'Sorrend';
$_['column_action']     = 'Művelet';

// Error
$_['error_permission']  = 'A fizetés módok beállításainak módosítása az Ön számára nem engedélyezett!';