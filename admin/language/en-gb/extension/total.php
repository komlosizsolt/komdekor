<?php
// Heading
$_['heading_title']     = 'Vásárlási kiegészítők';

// Text
$_['text_success']      = 'Success: You have modified totals!';
$_['text_list']         = 'Order Total List';

// Column
$_['column_name']       = 'Kiegészítő neve';
$_['column_status']     = 'Állapot';
$_['column_sort_order'] = 'Sorrend';
$_['column_action']     = 'Művelet';

// Error
$_['error_permission']  = 'Az Összes rendelés módosítása az Ön számára nem engedélyezett!';