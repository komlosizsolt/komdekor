<?php
// Heading
$_['heading_title']    = 'Hírcsatornák';

// Text
$_['text_success']     = 'Success: You have modified feeds!';
$_['text_list']        = 'Feed List';

// Column
$_['column_name']      = 'Termékcsatorna neve';
$_['column_status']    = 'Állapot';
$_['column_action']    = 'Művelet';

// Error
$_['error_permission'] = 'Figyelmeztetés: Termékcsatornák módosítása az Ön számára nem engedélyezett!';