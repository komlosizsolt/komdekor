<?php
// Heading
$_['heading_title']       = 'Kuponok';

// Text
$_['text_success']        = 'Sikeresen módosította a kuponokat!';
$_['text_list']           = 'Kuponok listája';
$_['text_add']            = 'Kupon hozzáadása';
$_['text_edit']           = 'Kupon szerkesztése';
$_['text_percent']        = 'Százalék';
$_['text_amount']         = 'Fix összeg';

// Column
$_['column_name']         = 'Kupon neve';
$_['column_code']         = 'Kód';
$_['column_discount']     = 'Kedvezmény';
$_['column_date_start']   = 'Kezdő dátum';
$_['column_date_end']     = 'Befejezés dátum';
$_['column_status']       = 'Állapot';
$_['column_order_id']     = 'Rendelési azonosító';
$_['column_customer']     = 'Vásárló';
$_['column_amount']       = 'Összeg';
$_['column_date_added']   = 'Hozzáadás dátuma';
$_['column_action']       = 'Művelet';

// Entry
$_['entry_name']          = 'Kupon neve';
$_['entry_code']          = 'Kód';
$_['entry_type']          = 'Típus';
$_['entry_discount']      = 'Kedvezmény';
$_['entry_logged']        = 'Belépett vásárló';
$_['entry_shipping']      = 'Ingyenes szállítás';
$_['entry_total']         = 'Teljes összeg';
$_['entry_category']      = 'Kategória';
$_['entry_product']       = 'Termékek';
$_['entry_date_start']    = 'Kezdő dátum';
$_['entry_date_end']      = 'Befejezés dátum';
$_['entry_uses_total']    = 'Kupon felhasználása';
$_['entry_uses_customer'] = 'Kupon vásárlónként';
$_['entry_status']        = 'Állapot';

// Help
$_['help_code']           = 'The code the customer enters to get the discount.';
$_['help_type']           = 'Százalékos vagy fix összeg';
$_['help_logged']         = 'A kód beváltásoz belépés szükséges';
$_['help_total']          = 'The total amount that must be reached before the coupon is valid.';
$_['help_category']       = 'Összes termék kiválasztása ebben a kategóriában.';
$_['help_product']        = 'Choose specific products the coupon will apply to. Select no products to apply coupon to entire cart.';
$_['help_uses_total']     = 'The maximum number of times the coupon can be used by any customer. Leave blank for unlimited';
$_['help_uses_customer']  = 'The maximum number of times the coupon can be used by a single customer. Leave blank for unlimited';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify coupons!';
$_['error_exists']        = 'Warning: Coupon code is already in use!';
$_['error_name']          = 'Coupon Name must be between 3 and 128 characters!';
$_['error_code']          = 'Code must be between 3 and 10 characters!';