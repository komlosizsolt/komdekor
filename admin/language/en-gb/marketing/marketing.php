<?php
// Heading
$_['heading_title']     = 'Marketing követés';

// Text
$_['text_success']      = 'Success: You have modified marketing tracking!';
$_['text_list']         = 'Marketing követések listája';
$_['text_add']          = 'Marketing követés hozzáadása';
$_['text_edit']         = 'Marketing követő szerkesztése';

// Column
$_['column_name']       = 'Kampány neve';
$_['column_code']       = 'Kód';
$_['column_clicks']     = 'Kattintások';
$_['column_orders']     = 'Rendelések';
$_['column_date_added'] = 'Hozzáadás dátuma';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']        = 'Kampány neve';
$_['entry_description'] = 'Campaign Description';
$_['entry_code']        = 'Követőkód';
$_['entry_example']     = 'Examples';
$_['entry_date_added']  = 'Hozzáadás dátuma';

// Help
$_['help_code']         = 'The tracking code that will be used to marketing track campaigns.';
$_['help_example']      = 'So the system can track refferals you neeed to add the tracking code to the end of the URL linking to your site.';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify marketing tracking!';
$_['error_name']        = 'Campaign must be between 1 and 32 characters!';
$_['error_code']        = 'TrKövetőkód kötelező!';
