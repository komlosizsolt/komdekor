<?php
// Heading
$_['heading_title']             = 'Affiliates';

// Text
$_['text_success']              = 'Success: You have modified affiliates!';
$_['text_approved']             = '%s fiók jóváhagyása sikeres!';
$_['text_list']                 = 'Affiliate lista';
$_['text_add']                  = 'Add Affiliate';
$_['text_edit']                 = 'Affiliate szerkesztése';
$_['text_affiliate_detail']     = 'Affiliate Details';
$_['text_affiliate_address']    = 'Affiliate Address';
$_['text_balance']              = 'Egyenleg';
$_['text_cheque']               = 'Ellenőrzés';
$_['text_paypal']               = 'PayPal ';
$_['text_bank']                 = 'Átutalás';

// Column
$_['column_name']               = 'Affiliate Name';
$_['column_email']              = 'E-mail';
$_['column_code']               = 'Követőkód';
$_['column_balance']            = 'Egyenleg';
$_['column_status']             = 'Állapot';
$_['column_approved']           = 'Jóváhagyott';
$_['column_date_added']         = 'Hozzáadás dátuma';
$_['column_description']        = 'Leírás';
$_['column_amount']             = 'Összeg';
$_['column_action']             = 'Művelet';

// Entry
$_['entry_firstname']           = 'Keresztnév';
$_['entry_lastname']            = 'Vezetéknév';
$_['entry_email']               = 'E-mail';
$_['entry_telephone']           = 'Telefon';
$_['entry_fax']                 = 'Fax';
$_['entry_status']              = 'Állapot';
$_['entry_password']            = 'Jelszó';
$_['entry_confirm']             = 'Megerősít';
$_['entry_company']             = 'Cégnév';
$_['entry_website']             = 'Weboldal';
$_['entry_address_1']           = 'Cím';
$_['entry_address_2']           = 'További cím';
$_['entry_city']                = 'Település';
$_['entry_postcode']            = 'Postcode';
$_['entry_country']             = 'Ország';
$_['entry_zone']                = 'Megye';
$_['entry_code']                = 'Követőkód';
$_['entry_commission']          = 'Commission (%)';
$_['entry_tax']                 = 'Adóazonosító';
$_['entry_payment']             = 'Fizetési mód';
$_['entry_cheque']              = 'Ellenőrizze a kedvezményezett nevét';
$_['entry_paypal']              = 'PayPal e-mail cím';
$_['entry_bank_name']           = 'Bank neve';
$_['entry_bank_branch_number']  = 'ABA/BSB szám';
$_['entry_bank_swift_code']     = 'SWIFT kód';
$_['entry_bank_account_name']   = 'Bankszámla';
$_['entry_bank_account_number'] = 'Számlaszám';
$_['entry_amount']              = 'Összeg';
$_['entry_description']         = 'Leírás';
$_['entry_name']                = 'Affiliate Name';
$_['entry_approved']            = 'Jóváhagyott';
$_['entry_date_added']          = 'Hozzáadás dátuma';

// Help
$_['help_code']                 = 'The tracking code that will be used to track referrals.';
$_['help_commission']           = 'Percentage the affiliate receives on each order.';

// Error
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to modify affiliates!';
$_['error_exists']              = 'Ez az E-Mail cím már regisztrálva van!';
$_['error_firstname']           = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']            = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']               = 'E-Mail cím nem megfelelő!';
$_['error_cheque']              = 'Cheque Payee Name required!';
$_['error_paypal']              = 'PayPal Email Address does not appear to be valid!!';
$_['error_bank_account_name']   = 'Account Name required!';
$_['error_bank_account_number'] = 'Account Number required!';
$_['error_telephone']           = 'A telefonszám minimum 3 maximum 32 karakter lehet!';
$_['error_password']            = 'A jelszó minimum 4 maximum 20 karakter lehet!';
$_['error_confirm']             = 'Password and password confirmation do not match!';
$_['error_address_1']           = 'A cím minimum 3 maximum 128 karakter lehet!';
$_['error_city']                = 'A település neve legalább 3 és legfeljebb 128 karakterből állhat!';
$_['error_postcode']            = 'Az irányítószám hossza ebben az országban 2 és 10 karakter közé kell essen!';
$_['error_country']             = 'Kérjük, válasszon egy országot!';
$_['error_zone']                = 'Válasszon ki megyét!';
$_['error_code']                = 'TrKövetőkód kötelező!';
