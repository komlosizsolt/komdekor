<?php
// Heading
$_['heading_title']        = 'Email';

// Text
$_['text_success']         = 'Your message has been successfully sent!';
$_['text_sent']            = 'Your message has been successfully sent to %s of %s recipients!';
$_['text_list']            = 'Levelek listája';
$_['text_default']         = 'Alapértelmezett';
$_['text_newsletter']      = 'Minden hírlevélre feliratkozó';
$_['text_customer_all']    = 'Minden vásárló';
$_['text_customer_group']  = 'Vásárlói csoport';
$_['text_customer']        = 'Vásárlók';
$_['text_affiliate_all']   = 'Minden Affiliate partner';
$_['text_affiliate']       = 'Affiliates';
$_['text_product']         = 'Termékek';

// Entry
$_['entry_store']          = 'Feladó';
$_['entry_to']             = 'Címzett';
$_['entry_customer_group'] = 'Vásárlói csoport';
$_['entry_customer']       = 'Vásárló';
$_['entry_affiliate']      = 'Affiliate';
$_['entry_product']        = 'Termékek';
$_['entry_subject']        = 'Tárgy';
$_['entry_message']        = 'Üzenet';

// Help
$_['help_customer']       = 'Autocomplete';
$_['help_affiliate']      = 'Autocomplete';
$_['help_product']        = 'Send only to customers who have ordered products in the list. (Autocomplete)';

// Error
$_['error_permission']     = 'Warning: You do not have permission to send E-Mails!';
$_['error_subject']        = 'E-Mail Subject required!';
$_['error_message']        = 'E-Mail Message required!';
