<?php
// Heading
$_['heading_title']    = 'Személyes átvétel';

// Text
$_['text_shipping']    = 'Szállítás';
$_['text_success']     = 'Siker! Módosította a raktárból való felvételt.';
$_['text_edit']        = 'Edit Pickup From Store Shipping';

// Entry
$_['entry_geo_zone']   = 'Földrajzi zóna:';
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Error
$_['error_permission'] = 'Figyelem! Nincs engedélye módosítani a raktárból való felvételt.';