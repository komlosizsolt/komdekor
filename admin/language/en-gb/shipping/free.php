<?php
// Heading
$_['heading_title']    = 'Ingyenes szállítás';

// Text
$_['text_shipping']    = 'Szállítás';
$_['text_success']     = 'Siker: Az ingyenes szállítási mód módosítása megtörtént!';
$_['text_edit']        = 'Edit Free Shipping';

// Entry
$_['entry_total']      = 'Összesen';
$_['entry_geo_zone']   = 'Földrajzi zóna:';
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Help
$_['help_total']       = 'Sub-Total amount needed before the free shipping module becomes available.';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az ingyenes szállítás módosítása az Ön számára nem engedélyezett!';