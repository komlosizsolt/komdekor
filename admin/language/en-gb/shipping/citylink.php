<?php
// Heading
$_['heading_title']    = 'Településen belül';

// Text
$_['text_shipping']    = 'Szállítás';
$_['text_success']     = 'Siker: A Citylink szállítási mód módosítása megtörtént!';
$_['text_edit']        = 'Edit Citylink Shipping';

// Entry
$_['entry_rate']       = 'Citylink ár';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_geo_zone']   = 'Földrajzi zóna:';
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Help
$_['help_rate']        = 'Enter values upto 5,2 decimal places. (12345.67) Example: .1:1,.25:1.27 - Weights less than or equal to 0.1Kg would cost &pound;1.00, Weights less than or equal to 0.25g but more than 0.1Kg will cost 1.27. Do not enter KG or symbols.';

// Error
$_['error_permission'] = 'Figyelem! Nincs engedélye a Citylink szállítás módosítására.';