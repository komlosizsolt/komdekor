<?php
// Heading
$_['heading_title']     = 'Árucikkenkénti szállítási díj';

// Text
$_['text_shipping']    = 'Szállítás';
$_['text_success']     = 'Siker: Az árucikkenkénti szállítási díj módosítása megtörtént!';
$_['text_edit']        = 'Edit Per Item Shipping';

// Entry
$_['entry_cost']       = 'Költség:';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_geo_zone']   = 'Földrajzi zóna:';
$_['entry_status']     = 'Állapot';
$_['entry_sort_order'] = 'Sorrend';

// Error
$_['error_permission'] = 'Figyelmeztetés: Az árucikkenkénti szállítási díj módosítása az Ön számára nem engedélyezett!';