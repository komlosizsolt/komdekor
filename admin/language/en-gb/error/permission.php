<?php
// Heading
$_['heading_title']   = 'Hozzáférés megtagadva!';

// Text
$_['text_permission'] = 'Nincs jogosultsága ehhez az oldalhoz, kérem, vegye fel a kapcsolatot az admininsztrátorral!';