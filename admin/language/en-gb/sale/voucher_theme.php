<?php
// Heading
$_['heading_title']     = 'Ajándékutalvány kinézetek';

// Text
$_['text_success']      = 'Siker: Módosítottad az ajándékutalvány kinézeteket!';
$_['text_list']         = 'Voucher Theme List';
$_['text_add']          = 'Add Voucher Theme';
$_['text_edit']         = 'Edit Voucher Theme';

// Column
$_['column_name']       = 'Ajándékutalvány kinézet neve';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']        = 'Ajándékutalvány kinézet neve:';
$_['entry_description'] = 'Ajándékutalvány kinézet leírása:';
$_['entry_image']       = 'Kép';

// Error
$_['error_permission']  = 'Figyelmeztetés: Nincs jogosultságod az ajándékutalvány kinézetek szerkesztéséhez!';
$_['error_name']        = 'Ajándékutalvány kinézet neve legalább 3 és legfeljebb 32 karakter lehet!';
$_['error_image']       = 'Kép szükséges!';
$_['error_voucher']     = 'Figyelmeztetés: Ez az ajándékutalvány kinézet nem törölhető mert hozzá van rendelve %s ajándékutalványhoz!';