<?php
// Heading
$_['heading_title']     = 'Ajándékutalvány';

// Text
$_['text_success']      = 'Siker: Módosítottad az ajándékutalványokat!';
$_['text_list']         = 'Gift Voucher List';
$_['text_add']          = 'Add Gift Voucher';
$_['text_edit']         = 'Edit Gift Voucher';
$_['text_sent']         = 'Siker: Ajándékutalvány e-mail elküldve!';

// Column
$_['column_name']       = 'Ajándékutalvány meve';
$_['column_code']       = 'Kód';
$_['column_from']       = 'Feladó';
$_['column_to']         = 'Címzett';
$_['column_theme']      = 'Kinézet';
$_['column_amount']     = 'Összeg';
$_['column_status']     = 'Állapot';
$_['column_order_id']   = 'Rendelési azonosító';
$_['column_customer']   = 'Vásárló';
$_['column_date_added'] = 'Hozzáadás dátuma';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_code']        = 'Kód';
$_['entry_from_name']   = 'Küldő neve:';
$_['entry_from_email']  = 'Küldő e-mail címe:';
$_['entry_to_name']     = 'Fogadó neve:';
$_['entry_to_email']    = 'Fogadó e-mail címe:';
$_['entry_theme']       = 'Kinézet:';
$_['entry_message']     = 'Üzenet';
$_['entry_amount']      = 'Összeg';
$_['entry_status']      = 'Állapot';

// Help
$_['help_code']         = 'The code the customer enters to activate the voucher.';

// Error
$_['error_selection']   = 'Warning: No vouchers selected!';
$_['error_permission']  = 'Figyelmeztetés: Nincs jogosultságod az ajándékutalványok szerkesztéséhez!';
$_['error_exists']      = 'Warning: Voucher code is already in use!';
$_['error_code']        = 'A kód legalább 3 és legfeljebb 10 karakter lehet!';
$_['error_to_name']     = 'A címzett nevének 1 és 64 karakter közé kell esnie!';
$_['error_from_name']   = 'A megadott név hosszának 1 és 64 karakter közé kell esnie!';
$_['error_email']       = 'E-mail nem tűnik valósnak!';
$_['error_amount']      = 'A mennyiségnek 1 vagy annál nagyobbnak kell lennie!';
$_['error_order']       = 'Warning: This voucher cannot be deleted as it is part of an <a href="%s">order</a>!';