<?php
// Heading
$_['heading_title']       = 'Termék visszaküldése';

// Text
$_['text_success']        = 'Siker: Módosítottad a visszárukat!';
$_['text_list']           = 'Product Return List';
$_['text_add']            = 'Add Product Return';
$_['text_edit']           = 'Edit Product Return';
$_['text_opened']         = 'Kibontott';
$_['text_unopened']       = 'Bontatlan';
$_['text_order']          = 'Order Information';
$_['text_product']        = 'Termék információ és a visszaküldés oka részletesen';
$_['text_history']        = 'Add Return History';

// Column
$_['column_return_id']     = 'Visszaküldési azonosító';
$_['column_order_id']      = 'Rendelési azonosító';
$_['column_customer']      = 'Vásárló';
$_['column_product']       = 'Termék';
$_['column_model']         = 'Cikkszám';
$_['column_status']        = 'Állapot';
$_['column_date_added']    = 'Hozzáadás dátuma';
$_['column_date_modified'] = 'Módosítás dátuma';
$_['column_comment']       = 'Megjegyzés';
$_['column_notify']        = 'Ügyfél értesítve';
$_['column_action']        = 'Művelet';

// Entry
$_['entry_customer']      = 'Vásárló';
$_['entry_order_id']      = 'Rendelési azonosító';
$_['entry_date_ordered']  = 'Rendelés dátuma:';
$_['entry_firstname']     = 'Keresztnév';
$_['entry_lastname']      = 'Vezetéknév';
$_['entry_email']         = 'E-mail';
$_['entry_telephone']     = 'Telefon';
$_['entry_product']       = 'Termék:';
$_['entry_model']         = 'Cikkszám:';
$_['entry_quantity']      = 'Mennyiség:';
$_['entry_opened']        = 'Kibontott:';
$_['entry_comment']       = 'Megjegyzés';
$_['entry_return_reason'] = 'Return Reason';
$_['entry_return_action'] = 'Return Action';
$_['entry_return_status'] = 'Visszaküldési állapota';
$_['entry_notify']        = 'Ügyfél értesítése:';
$_['entry_return_id']     = 'Visszaküldési azonosító';
$_['entry_date_added']    = 'Hozzáadás dátuma';
$_['entry_date_modified'] = 'Módosítás dátuma';

// Help
$_['help_product']        = '(Autocomplete)';

// Error
$_['error_warning']       = 'Figyelmeztetés: Kérlek nézd át az űrlapot hibákért!';
$_['error_permission']    = 'Figyelmeztetés: Nincs engedélye a visszáru módosítására!';
$_['error_order_id']      = 'Order ID required!';
$_['error_firstname']     = 'A keresztnévnek legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_lastname']      = 'A vezetéknevének legalább 1, legfeljebb 32 karakter hosszúságú lehet!';
$_['error_email']         = 'E-mail nem tűnik valósnak!';
$_['error_telephone']     = 'Telefon legalább 3 legfeljebb 32 karakter lehet!';
$_['error_product']       = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']         = 'Product Model must be greater than 3 and less than 64 characters!';