<?php
// Heading
$_['heading_title']                        = 'Ismétlődő profilok';

// Text
$_['text_success']                         = 'Success: You have modified Payment Profiles!';
$_['text_list']                            = 'Ismétlődő porfilok listája';
$_['text_recurring_detail']                = 'Recurring Details';
$_['text_order_detail']                    = 'Order Details';
$_['text_product_detail']                  = 'Product Details';
$_['text_transaction']                     = 'Transactions';
$_['text_order_recurring_id']              = 'Recurring Order ID';
$_['text_reference']                       = 'Payment Reference';
$_['text_recurring_name']                  = 'Recurring Profile';
$_['text_recurring_description']           = 'Description';
$_['text_recurring_status']                = 'Recurring Status';
$_['text_payment_method']                  = 'Payment Method';
$_['text_order_id']                        = 'Order ID';
$_['text_customer']                        = 'Customer';
$_['text_email']                           = 'Email';
$_['text_date_added']                      = 'Date Added';
$_['text_order_status']                    = 'Order Status';
$_['text_type']                            = 'Type';
$_['text_action']                          = 'Action';
$_['text_product']                         = 'Product';
$_['text_quantity']                        = 'Quantity';
$_['text_amount']                          = 'Amount';
$_['text_cancel_payment']                  = 'Cancel Payment';
$_['text_status_1']                        = 'Active';
$_['text_status_2']                        = 'Inactive';
$_['text_status_3']                        = 'Cancelled';
$_['text_status_4']                        = 'Suspended';
$_['text_status_5']                        = 'Expired';
$_['text_status_6']                        = 'Pending';

$_['text_transactions']                    = 'Tranzakcióim';
$_['text_cancel_confirm']                  = 'Profile\'s cancelation cannot be undone! Are you sure want to do this?';
$_['text_transaction_date_added']          = 'Hozzáadás dátuma';
$_['text_transaction_payment'] 			   = 'Fizetés';
$_['text_transaction_outstanding_payment'] = 'Kiemelkedő befizetés';
$_['text_transaction_skipped']             = 'Kihagyott befizetés';
$_['text_transaction_failed']              = 'A befizetés nem sikerült';
$_['text_transaction_cancelled']           = 'Törölve';
$_['text_transaction_suspended']           = 'Felfüggesztett';
$_['text_transaction_suspended_failed']    = 'Suspended from failed payment';
$_['text_transaction_outstanding_failed']  = 'A befizetés nem sikerült';
$_['text_transaction_expired']             = 'Lejárt';
$_['text_cancelled']                       = 'Előfizetés törölve';

// Column
$_['column_order_recurring_id']             = 'Recurring ID';
$_['column_order_id']                       = 'Order ID';
$_['column_reference']                      = 'Payment Reference';
$_['column_customer']                       = 'Customer';
$_['column_date_added']                     = 'Date Added';
$_['column_status']                         = 'Status';
$_['column_amount']                         = 'Amount';
$_['column_type']                           = 'Type';
$_['column_action']                         = 'Action';

// Entry
$_['entry_order_recurring_id']             = 'Recurring ID';
$_['entry_order_id']                       = 'Rendelési azonosító';
$_['entry_reference']                      = 'Payment Reference';
$_['entry_customer']                       = 'Vásárló';
$_['entry_date_added']                     = 'Hozzáadás dátuma';
$_['entry_status']                         = 'Állapot';
$_['entry_type']                           = 'Típus';
$_['entry_action']                         = 'Művelet';
$_['entry_email']                          = 'Email';
$_['entry_description']                    = 'Ismétlődő profil leírása';
$_['entry_product']                        = 'Product:';
$_['entry_quantity']                       = 'Quantity:';
$_['entry_amount']                         = 'Összeg';
$_['entry_recurring']                      = ' Ismétlődő profil';
$_['entry_payment_method']                 = 'Fizetési mód';
$_['entry_cancel_payment']                 = 'Cancel Payment:';

// Error
$_['error_not_cancelled']                  = 'Error: %s';
$_['error_not_found']                      = 'Could not cancel profile';