<?php
// Heading
$_['heading_title']					= 'PayPoint';

// Text
$_['text_payment']					= 'Fizetés';
$_['text_success']					= 'Siker: A PayPoint fiók részleteinek módosítása megtörtént!';
$_['text_edit']                     = 'Edit PayPoint';
$_['text_paypoint']					= '<a onclick="window.open(\'https://www.paypoint.net/partners/opencart\');"><img src="view/image/payment/paypoint.png" alt="PayPoint" title="PayPoint" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']						= 'Termelés';
$_['text_successful']				= 'Mindig sikeres';
$_['text_fail']						= 'Mindig nem';

// Entry
$_['entry_merchant']				= 'Kereskedő ID:';
$_['entry_password']				= 'Remote Password';
$_['entry_test']					= 'Test Mode';
$_['entry_total']					= 'Összesen';
$_['entry_order_status']			= 'Rendelés státusza';
$_['entry_geo_zone']				= 'Földrajzi zóna:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';

// Help
$_['help_password']					= 'Leave empty if you do not have "Digest Key Authentication" enabled on your account.';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']				= 'Figyelmeztetés: Az PayPoint történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant']				= 'Kereskedő ID szükséges!';