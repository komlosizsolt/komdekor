<?php
// Heading
$_['heading_title']					 = 'PayPal';

// Text
$_['text_payment']					 = 'Fizetés';
$_['text_success']					 = 'Siker: A PayPal fiók részleteinek módosítása megtörtént!';
$_['text_edit']                      = 'Edit PayPal Payments Standard';
$_['text_pp_standard']				 = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Hitelesítés';
$_['text_sale']						 = 'Eladás'; 

// Entry
$_['entry_email']					 = 'E-mail';
$_['entry_test']					 = 'Teszt mód:';
$_['entry_transaction']				 = 'Tranzakciós módszer:';
$_['entry_debug']					 = 'Debug mód';
$_['entry_total']					 = 'Összesen';
$_['entry_canceled_reversal_status'] = 'Visszavonás törölve';
$_['entry_completed_status']		 = 'Befejezett állapota';
$_['entry_denied_status']			 = 'Rendelési állapot: Megtagadva';
$_['entry_expired_status']			 = 'Expired Status:';
$_['entry_failed_status']			 = 'Rendelés állapot: Sikertelen';
$_['entry_pending_status']			 = 'Rendelési állapot: Függőben';
$_['entry_processed_status']		 = 'Processed Status:';
$_['entry_refunded_status']			 = 'Rendelési állapot: Visszatérítve';
$_['entry_reversed_status']			 = 'Rendelési állapot: Visszafordítva';
$_['entry_voided_status']			 = 'Rendelési állapot: Ismeretlen hiba:';
$_['entry_geo_zone']				 = 'Földrajzi zóna:';
$_['entry_status']					 = 'Állapot';
$_['entry_sort_order']				 = 'Sorrend';

// Tab
$_['tab_general']					 = 'Általános';
$_['tab_order_status']       		 = 'Rendelés státusza';

// Help
$_['help_test']						 = 'Use the live or testing (sandbox) gateway server to process transactions?';
$_['help_debug']			    	 = 'Logs additional information to the system log';
$_['help_total']					 = 'The checkout total the order must reach before this payment method becomes active';

// Error
$_['error_permission']				 = 'Figyelmeztetés: A PayPal fiókon keresztüli fizetés módosítása az Ön számára nem engedélyezett';
$_['error_email']					 = 'Az e-mail cím megadása kötelező!';