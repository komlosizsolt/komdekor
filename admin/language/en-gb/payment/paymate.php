<?php
// Heading
$_['heading_title']					= 'Paymate';

// Text
$_['text_payment']					= 'Fizetés';
$_['text_success']					= 'Siker: A PayMate fiók részleteinek módosítása megtörtént!';
$_['text_edit']                     = 'Edit Paymate';
$_['text_paymate']					= '<img src="view/image/payment/paymate.png" alt="Paymate" title="Paymate" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_username']				= 'Paymate felhasználónév:';
$_['entry_password']				= 'Jelszó';
$_['entry_test']					= 'Test Mode';
$_['entry_total']					= 'Összesen';
$_['entry_order_status']			= 'Rendelés státusza';
$_['entry_geo_zone']				= 'Földrajzi zóna:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';

// Help
$_['help_password']					= 'Just use some random password. This will be used to make sure the payment information is not interfered with after being sent to the payment gateway.';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']				= 'Figyelmeztetés: Az Paymate történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_username']				= 'Felhasználónév szükséges!';
$_['error_password']				= 'Password required!';