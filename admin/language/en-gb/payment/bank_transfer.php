<?php
// Heading
$_['heading_title']		 = 'Banki átutalás';

// Text
$_['text_payment']		 = 'Fizetés';
$_['text_success']		 = 'A banki átutalásos fizetési modul adatainak módosítása sikeresen megtörtént!';
$_['text_edit']          = 'Edit Bank Transfer';

// Entry
$_['entry_bank']		 = 'Banki átutalás utasításai:';
$_['entry_total']		 = 'Összesen';
$_['entry_order_status'] = 'Rendelés státusza';
$_['entry_geo_zone']	 = 'Földrajzi zóna:';
$_['entry_status']		 = 'Állapot';
$_['entry_sort_order']	 = 'Sorrend';

// Help
$_['help_total']		 = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']   = 'A banki átutalásos fizetési modul módosításához nincs jogosultsága!';
$_['error_bank']         = 'Adja meg a banki utaláshoz szükséges információkat!';