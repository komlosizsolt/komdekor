<?php
// Heading
$_['heading_title']         = 'Authorize.Net (AIM)';

// Text
$_['text_payment']          = 'Fizetés';
$_['text_success']          = 'Siker: A Authorize.Net AIM fiók részleteinek módosítása megtörtént!';
$_['text_edit']             = 'Edit Authorize.Net (AIM)';
$_['text_test']             = 'Teszt';
$_['text_live']             = 'Élő';
$_['text_authorization']    = 'Meghatalmazás';
$_['text_capture']          = 'Elfog';
$_['text_authorizenet_aim'] = '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_login']           = 'Felhasználónév:';
$_['entry_key']             = 'Kulcs:';
$_['entry_hash']            = 'MD5 Hash:';
$_['entry_server']          = 'Tranzakció Szerver:';
$_['entry_mode']            = 'Tranzakció Mód:';
$_['entry_method']          = 'Tranzakció Módszer:';
$_['entry_total']           = 'Összesen';
$_['entry_order_status']    = 'Rendelés státusza';
$_['entry_geo_zone']        = 'Földrajzi zóna:';
$_['entry_status']          = 'Állapot';
$_['entry_sort_order']      = 'Sorrend';

// Help
$_['help_total']            = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']      = 'Figyelmeztetés: Az Authorize.Net (AIM) történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_login']           = 'Felhasználónév szükséges!';
$_['error_key']             = 'Kulcs szükséges!';