<?php
// Heading
$_['heading_title']					= '2Checkout';

// Text
$_['text_payment']					= 'Fizetés';
$_['text_success']					= 'Siker: A 2Checkout fiók adatainak módosítása megtörtént!';
$_['text_edit']                     = 'Edit 2Checkout';
$_['text_twocheckout']				= '<a href="https://www.2checkout.com/2co/affiliate?affiliate=1596408" target="_blank"><img src="view/image/payment/2checkout.png" alt="2Checkout" title="2Checkout" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_account']					= '2Checkout Fiók ID:';
$_['entry_secret']					= 'Titkos szó';
$_['entry_display']					= 'Direct Checkout';
$_['entry_test']					= 'Test Mode';
$_['entry_total']					= 'Összesen';
$_['entry_order_status']			= 'Rendelés státusza';
$_['entry_geo_zone']				= 'Földrajzi zóna:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';

// Help
$_['help_secret']					= 'The secret word to confirm transactions with (must be the same as defined on the merchant account configuration page).';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']				= 'Figyelmeztetés: Az 2Checkout történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_account']					= 'A fiókszám megadása kötelező!';
$_['error_secret']					= 'Titkos szó szükséges!';