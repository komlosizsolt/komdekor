<?php
// Heading
$_['heading_title']					= 'SagePay Direct';

// Text
$_['text_payment']					= 'Fizetés';
$_['text_success']					= 'Siker: A SagePay Direct fiók részleteinek módosítása megtörtént!';
$_['text_edit']                     = 'Edit SagePay Direct';
$_['text_sagepay_direct']			= '<a href="https://support.sagepay.com/apply/default.aspx?PartnerID=E511AF91-E4A0-42DE-80B0-09C981A3FB61" target="_blank"><img src="view/image/payment/sagepay.png" alt="SagePay" title="SagePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']						= 'Szimulátor';
$_['text_test']						= 'Teszt';
$_['text_live']						= 'Élő';
$_['text_defered']					= 'Elhalasztott';
$_['text_authenticate']				= 'Hitelesít';
$_['text_release_ok']				= 'Release was successful';
$_['text_release_ok_order']			= 'Release was successful, order status updated to success - settled';
$_['text_rebate_ok']				= 'Rebate was successful';
$_['text_rebate_ok_order']			= 'Rebate was successful, order status updated to rebated';
$_['text_void_ok']					= 'Void was successful, order status updated to voided';
$_['text_payment_info']				= 'Fizetési információk';
$_['text_release_status']			= 'Payment released';
$_['text_void_status']				= 'Payment voided';
$_['text_rebate_status']			= 'Payment rebated';
$_['text_order_ref']				= 'Rendelési szám';
$_['text_order_total']				= 'Total authorised';
$_['text_total_released']			= 'Total released';
$_['text_transactions']				= 'Tranzakcióim';
$_['text_column_amount']			= 'Összeg';
$_['text_column_type']				= 'Típus';
$_['text_column_date_added']		= 'Létrehozás dátuma';
$_['text_confirm_void']				= 'Are you sure you want to void the payment?';
$_['text_confirm_release']			= 'Are you sure you want to release the payment?';
$_['text_confirm_rebate']			= 'Are you sure you want to rebate the payment?';

// Entry
$_['entry_vendor']					= 'Kereskedő ID:';
$_['entry_test']					= 'Test Mode';
$_['entry_transaction']				= 'Tranzakció Módszer:';
$_['entry_total']					= 'Összesen';
$_['entry_order_status']			= 'Rendelés státusza';
$_['entry_geo_zone']				= 'Földrajzi zóna:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';
$_['entry_debug']					= 'Debug logging';
$_['entry_card']					= 'Store Cards';
$_['entry_cron_job_token']			= 'Secret Token';
$_['entry_cron_job_url']			= 'Cron Job\'s URL';
$_['entry_last_cron_job_run']		= 'Last cron job\'s run time:';

// Help
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';
$_['help_debug']					= 'Enabling debug will write sensitive data to a log file. You should always disable unless instructed otherwise';
$_['help_transaction']				= 'Transaction method MUST be set to Payment to allow subscription payments';
$_['help_cron_job_token']			= 'Make this long and hard to guess';
$_['help_cron_job_url']				= 'Set a cron job to call this URL';

// Button
$_['button_release']				= 'Release';
$_['button_rebate']					= 'Rebate / refund';
$_['button_void']					= 'Void';

// Error
$_['error_permission']				= 'Figyelmeztetés: Az SagePay Direct történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_vendor']					= 'Kereskedő ID szükséges!';
