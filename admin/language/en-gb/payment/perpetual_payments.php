<?php
// Heading
$_['heading_title']					= 'Perpetual Payments';

// Text
$_['text_payment']					= 'Fizetés';
$_['text_success']					= 'Siker: A Perpetual Payments fiók részleteinek módosítása megtörtént!';
$_['text_edit']                     = 'Edit Perpetual Payments';

// Entry
$_['entry_auth_id']					= 'Felhasználónév:';
$_['entry_auth_pass']				= 'Jelszó:';
$_['entry_test']					= 'Test Mode';
$_['entry_total']					= 'Összesen';
$_['entry_order_status']			= 'Rendelés státusza';
$_['entry_geo_zone']				= 'Földrajzi zóna:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';

// Help
$_['help_test']						= 'Use this module in Test (YES) or Production mode (NO)?';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']				= 'Figyelmeztetés: Az Perpetual Payments történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_auth_id']					= 'Felhasználónév szükséges!';
$_['error_auth_pass']				= 'Jelszó szükséges!';