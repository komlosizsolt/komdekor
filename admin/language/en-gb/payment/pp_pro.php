<?php
// Heading
$_['heading_title']					= 'PayPal Website Payment Pro';

// Text
$_['text_success']					= 'Siker: A PayPal Website Payment Pro fiók részleteinek módosítása megtörtént!';
$_['text_edit']                     = 'Edit PayPal Pro';
$_['text_pp_pro']					= '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Engedély';
$_['text_sale']						= 'Eladás';

// Entry
$_['entry_username']				= 'API felhasználónév';
$_['entry_password']				= 'API Password';
$_['entry_signature']				= 'API aláírás';
$_['entry_test']					= 'Test Mode';
$_['entry_transaction']				= 'Transaction Method:';
$_['entry_total']					= 'Összesen';
$_['entry_order_status']			= 'Rendelés státusza';
$_['entry_geo_zone']				= 'Földrajzi zóna:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';

// Help
$_['help_test']						= 'Use the live or testing (sandbox) gateway server to process transactions?';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active';

// Error
$_['error_permission']				= 'Figyelmeztetés: Az PayPal Website Payment Pro történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_username']				= 'API Felhasználónév szükséges!';
$_['error_password']				= 'API Jelszó szükséges!';
$_['error_signature']				= 'API Aláírás szükséges!';