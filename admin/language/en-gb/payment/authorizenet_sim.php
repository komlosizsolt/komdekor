<?php
// Heading
$_['heading_title']			= 'Authorize.Net (SIM)';

// Text
$_['text_payment']			= 'Fizetés';
$_['text_success']			= 'Siker: A Authorize.Net SIM fiók részleteinek módosítása megtörtént!';
$_['text_edit']             = 'Edit Authorize.Net (SIM)';
$_['text_authorizenet_sim']	= '<a onclick="window.open(\'http://reseller.authorize.net/application/?id=5561142\');"><img src="view/image/payment/authorizenet.png" alt="Authorize.Net" title="Authorize.Net" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_merchant']		= 'Kereskedő ID:';
$_['entry_key']				= 'Tranzakció kulcs:';
$_['entry_callback']		= 'Relay Response URL';
$_['entry_md5']				= 'MD5 Hash Value';
$_['entry_test']			= 'Test Mode';
$_['entry_total']			= 'Összesen';
$_['entry_order_status']	= 'Rendelés státusza';
$_['entry_geo_zone']		= 'Földrajzi zóna:';
$_['entry_status']			= 'Állapot';
$_['entry_sort_order']		= 'Sorrend';

// Help
$_['help_callback']			= 'Please login and set this at <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.';
$_['help_md5']				= 'The MD5 Hash feature enables you to authenticate that a transaction response is securely received from Authorize.Net.Please login and set this at <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.(Optional)';
$_['help_total']			= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']		= 'Figyelmeztetés: Az Authorize.Net (SIM) történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant']		= 'Kereskedő ID szükséges!';
$_['error_key']				= 'Tranzakció kulcs szükséges!';