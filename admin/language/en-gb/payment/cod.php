<?php
// Heading
$_['heading_title']					= 'Utánvétel';

// Text
$_['text_payment']					= 'Fizetés';
$_['text_success']					= 'Siker: Az utánvétel modul frissítése megtörtént!';
$_['text_edit']                     = 'Edit Cash On Delivery';

// Entry
$_['entry_total']					= 'Összesen';
$_['entry_order_status']			= 'Rendelés státusza';
$_['entry_geo_zone']				= 'Földrajzi zóna:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';

// Help
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']				= 'Figyelmeztetés: Az utánvétellel történő fizetés módosítása az Ön számára nem engedélyezett';