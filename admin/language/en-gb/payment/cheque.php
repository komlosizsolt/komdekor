<?php
// Heading
$_['heading_title']		 = 'Postai csekk';

// Text
$_['text_payment']		 = 'Fizetés';
$_['text_success']		 = 'A postai fizetési modul adatainak módosítása sikeresen megtörtént!';
$_['text_edit']          = 'Edit Cheque / Money Order';

// Entry
$_['entry_payable']		 = 'Kedvezményezett:';
$_['entry_total']		 = 'Összesen';
$_['entry_order_status'] = 'Rendelés státusza';
$_['entry_geo_zone']	 = 'Földrajzi zóna:';
$_['entry_status']		 = 'Állapot';
$_['entry_sort_order']	 = 'Sorrend';

// Help
$_['help_total']		 = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']   = 'A postai fizetési modul módosításához nincs jogosultsága!';
$_['error_payable']	     = 'Adja meg a kedvezményezett nevét!';