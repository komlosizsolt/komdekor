<?php
// Heading
$_['heading_title']					 = 'PayPal Express';

// Text
$_['text_payment']				  	 = 'Fizetés';
$_['text_success']				 	 = 'Siker: A PayPal Express fiók részleteinek módosítása megtörtént!';
$_['text_edit']                      = 'Edit PayPal Express Checkout';
$_['text_pp_express']				 = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			 = 'Engedély';
$_['text_sale']						 = 'Eladás';
$_['text_signup']                    = 'Sign up for PayPal - save your settings first as this page will be refreshed';
$_['text_sandbox']                   = 'Sign up for PayPal Sandbox - save your settings first as this page will be refreshed';

// Entry
$_['entry_username']				 = 'API felhasználónév';
$_['entry_password']				 = 'API Password';
$_['entry_signature']				 = 'API aláírás';
$_['entry_sandbox_username']		 = 'API Sandbox Username';
$_['entry_sandbox_password']		 = 'API Sandbox Password';
$_['entry_sandbox_signature']		 = 'API Sandbox Signature';
$_['entry_ipn']						 = 'IPN URL';
$_['entry_test']					 = 'Teszt Mód:';
$_['entry_debug']					 = 'Debug logging';
$_['entry_currency']				 = 'Default currency';
$_['entry_recurring_cancel']	     = 'Allow customers to cancel recurring payments';
$_['entry_transaction']		         = 'Transaction Method';
$_['entry_total']					 = 'Összesen';
$_['entry_geo_zone']				 = 'Földrajzi zóna:';
$_['entry_status']					 = 'Állapot';
$_['entry_sort_order']				 = 'Sorrend';
$_['entry_canceled_reversal_status'] = 'Visszavonás törölve';
$_['entry_completed_status']		 = 'Befejezett állapota';
$_['entry_denied_status']			 = 'Rendelési állapot: Megtagadva';
$_['entry_expired_status']			 = 'Expired Status';
$_['entry_failed_status']			 = 'Rendelés állapot: Sikertelen';
$_['entry_pending_status']			 = 'Rendelési állapot: Függőben';
$_['entry_processed_status']		 = 'Processed Status';
$_['entry_refunded_status']			 = 'Rendelési állapot: Visszatérítve';
$_['entry_reversed_status']			 = 'Rendelési állapot: Visszafordítva';
$_['entry_voided_status']			 = 'Voided Status';
$_['entry_allow_notes']				 = 'Allow notes';
$_['entry_colour']	      			 = 'Page background colour';
$_['entry_logo']					 = 'Logo';

// Tab
$_['tab_api']				         = 'API Details';
$_['tab_order_status']				 = 'Rendelés státusza';
$_['tab_checkout']					 = 'Fizetés';

// Help
$_['help_ipn']						 = 'Required for subscriptions';
$_['help_total']					 = 'The checkout total the order must reach before this payment method becomes active';
$_['help_logo']						 = 'Max 750px(w) x 90px(h)<br />You should only use a logo if you have SSL set up.';
$_['help_colour']					 = '6 character HTML colour code';
$_['help_currency']					 = 'Used for transaction searches';

// Error
$_['error_permission']				 = 'Figyelmeztetés: Az PayPal Express történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_username']				 = 'API Felhasználónév szükséges!';
$_['error_password']				 = 'API Jelszó szükséges!';
$_['error_signature']				 = 'API Aláírás szükséges!';
$_['error_sandbox_username']	 	 = 'API Sandbox Username Required!';
$_['error_sandbox_password']		 = 'API Sandbox Password Required!';
$_['error_sandbox_signature']		 = 'API Sandbox Signature Required!';
$_['error_api']						 = 'Paypal Authorization Error';
$_['error_api_sandbox']				 = 'Paypal Sandbox Authorization Error';