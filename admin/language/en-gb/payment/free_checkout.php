<?php
// Heading
$_['heading_title']		 = 'Ingyenes megrendelés';

// Text
$_['text_payment']		 = 'Fizetés';
$_['text_success']		 = 'Ingyenes szállítás modul adatainak módosítása sikeresen megtörtént!';
$_['text_edit']          = 'Edit Free Checkout';

// Entry
$_['entry_order_status'] = 'Rendelés státusza';
$_['entry_status']		 = 'Állapot';
$_['entry_sort_order']	 = 'Sorrend';

// Error
$_['error_permission']	  = 'Az ingyenes szállítási modul módosításához nincs jogosultsága!';