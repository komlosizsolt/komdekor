<?php
// Heading
$_['heading_title']					= 'SagePay (US)';

// Text
$_['text_payment']					= 'Fizetés';
$_['text_success']					= 'Siker: A SagePay US fiók részleteinek módosítása megtörtént!';
$_['text_edit']                     = 'Edit Sage Payment Solutions (US)';

// Entry
$_['entry_merchant_id']				= 'Kereskedő ID:';
$_['entry_merchant_key']			= 'Kereskedő Kulcs:';
$_['entry_total']					= 'Összesen';
$_['entry_order_status']			= 'Rendelés státusza';
$_['entry_geo_zone']				= 'Földrajzi zóna:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';

// Help
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']				= 'Figyelmeztetés: Az SagePay (US) történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant_id']				= 'Kereskedő ID szükséges!';
$_['error_merchant_key']			= 'Kereskedő Kulcs szükséges!';