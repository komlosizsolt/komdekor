<?php
// Heading
$_['heading_title']		 = 'LIQPAY';

// Text
$_['text_payment']		 = 'Fizetés';
$_['text_success']		 = 'Siker: A LIQPAY fiók részleteinek módosítása megtörtént!';
$_['text_edit']          = 'Edit LIQPAY';
$_['text_pay']			 = 'LIQPAY';
$_['text_card']			 = 'Hitelkártya';
$_['text_liqpay']		 = '<img src="view/image/payment/liqpay.png" alt="LIQPAY" title="LIQPAY" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_merchant']	 = 'Kereskedő ID:';
$_['entry_signature']	 = 'Aláírás:';
$_['entry_type']		 = 'Típus';
$_['entry_total']		 = 'Összesen';
$_['entry_order_status'] = 'Rendelés státusza';
$_['entry_geo_zone']	 = 'Földrajzi zóna:';
$_['entry_status']		 = 'Állapot';
$_['entry_sort_order']	 = 'Sorrend';

// Help
$_['help_total']		 = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']	 = 'Figyelmeztetés: Az LIQPAY történő fizetés módosítása az Ön számára nem engedélyezett!';
$_['error_merchant']	 = 'Kereskedő ID szükséges!';
$_['error_signature']	 = 'Aláírás szükséges!';