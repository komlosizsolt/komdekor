<?php
// Heading
$_['heading_title']			  = 'Skrill';

// Text
$_['text_payment']			  = 'Fizetés';
$_['text_success']			  = 'Success: You have modified the Skrill details.';
$_['text_edit']               = 'Edit Skrill';
$_['text_skrill']	     	  = '<a href="https://www.moneybookers.com/partners/?p=OpenCart" target="_blank"><img src="view/image/payment/skrill.png" alt="Skrill" title="Skrill" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_email']			  = 'E-mail';
$_['entry_secret']		      = 'Secret';
$_['entry_total']			  = 'Összesen';
$_['entry_order_status']	  = 'Rendelés státusza';
$_['entry_pending_status']	  = 'Rendelési állapot: Függőben';
$_['entry_canceled_status']	  = 'Canceled Status';
$_['entry_failed_status']	  = 'Rendelés állapot: Sikertelen';
$_['entry_chargeback_status'] = 'Chargeback Status';
$_['entry_geo_zone']		  = 'Geo Zone';
$_['entry_status']			  = 'Állapot';
$_['entry_sort_order']		  = 'Sorrend';

// Help
$_['help_total']			  = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']		  = 'Warning: You do not have permission to modify Skrill!';
$_['error_email']			  = 'E-Mail Required!';