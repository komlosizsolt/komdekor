<?php
// Heading
$_['heading_title']					= 'Klarna Invoice';

// Text
$_['text_payment']					= 'Fizetés';
$_['text_success']					= 'Success: You have modified Klarna Invoice payment module!';
$_['text_edit']                     = 'Edit Klarna Invoice';
$_['text_klarna_invoice']			= '<a onclick="window.open(\'https://merchants.klarna.com/signup?locale=en&partner_id=d5c87110cebc383a826364769047042e777da5e8&utm_campaign=Platform&utm_medium=Partners&utm_source=Opencart\');"><img src="view/image/payment/klarna.png" alt="Klarna" title="Klarna" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']						= 'Live';
$_['text_beta']						= 'Beta';
$_['text_sweden']					= 'Sweden';
$_['text_norway']					= 'Norway';
$_['text_finland']					= 'Finland';
$_['text_denmark']					= 'Denmark';
$_['text_germany']					= 'Germany';
$_['text_netherlands']				= 'The Netherlands';

// Entry
$_['entry_merchant']				= 'Klarna Merchant ID';
$_['entry_secret']					= 'Klarna Secret';
$_['entry_server']					= 'Server:';
$_['entry_total']					= 'Összesen';
$_['entry_pending_status']			= 'Rendelési állapot: Függőben';
$_['entry_accepted_status']			= 'Accepted Status';
$_['entry_geo_zone']				= 'Geo Zone:';
$_['entry_status']					= 'Állapot';
$_['entry_sort_order']				= 'Sorrend';

// Help
$_['help_merchant']					= '(estore id) to use for the Klarna service (provided by Klarna).';
$_['help_secret']					= 'Shared secret to use with the Klarna service (provided by Klarna).';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify payment Klarna Invoice!';